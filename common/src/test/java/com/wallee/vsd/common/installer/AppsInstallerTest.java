package com.wallee.vsd.common.installer;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.wallee.vsd.common.DataFixtures;
import com.wallee.vsd.common.installer.data.UrlResponse;

import com.wallee.vsd.common.vsd.Updt;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.junit.jupiter.MockitoExtension;


import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class AppsInstallerTest {

    private Updt updt;

    private NetClient client;

    private Context context;

    private AppsInstaller appsInstaller;

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();


    @BeforeEach
    public void init() {
        updt = mock(Updt.class);
        context = mock(Context.class);
        client = mock(NetClient.class);
        appsInstaller = new AppsInstaller(context, client, updt);
    }


    @Test
    public void handleUrlResponseWithVerify() {
        // given
        UrlResponse urlResponse = DataFixtures.getUrlResponse();
        AppsInstaller installer = mock(AppsInstaller.class);

        //when
        installer.handleUrlResponse(urlResponse);

        // then
        verify(installer, times(1)).handleUrlResponse(urlResponse);

    }

    @Test
    public void handleUrlResponseErrorThrowIOException() {
        //given
        AppsInstaller installer = mock(AppsInstaller.class);
        IOException exception = new IOException("Error");
        doAnswer(i -> {
            IOException arg0 = i.getArgument(0);

            assertEquals(exception, arg0);
            return null;
        }).when(installer).handleUrlResponseError(exception);


       // appsInstaller.handleUrlResponseError(exception);
    }


    @Test
    public void handleUrlResponseGetArguments() throws Exception {
        //given
        UrlResponse urlResponse = DataFixtures.getUrlResponse();
        AppsInstaller installer = mock(AppsInstaller.class);

        //when
        doAnswer(invocation -> {
            UrlResponse arg0 = invocation.getArgument(0);

            assertEquals(urlResponse, arg0);
            return null;
        }).when(installer).handleUrlResponse(urlResponse);
        installer.handleUrlResponse(urlResponse);
    }

    @Test
    public void handleUrlResponseThrowNullPointerException() {
        //given
        AppsInstaller installer = mock(AppsInstaller.class);

        //when
        doThrow(NullPointerException.class)
                .when(installer)
                .handleUrlResponse(null);

        //then
        Assertions.assertThrows(NullPointerException.class, () -> {
            installer.handleUrlResponse(null);
        });
    }


    @Test
    public void handleUrlResponseSuccessfully() {
        //given
        String applicationId = "applicationId";
        PackageManager manager = mock(PackageManager.class);
        UrlResponse urlResponse = DataFixtures.getUrlResponse();

        //when
        when(context.getPackageManager()).thenReturn(manager);
        when(appsInstaller.findPackageInfo(applicationId)).thenReturn(null);
      //  appsInstaller.handleUrlResponse(urlResponse);

    }

    @Test
    public void addition_isCorrect() {
        Assert.assertEquals(4, 2 + 2);
    }


    @Test
    public void handleUrlResponseThrowsNullPointerException() {
        //given
        fail();
     /*   AppsInstaller installer = mock(AppsInstaller.class);
        UrlResponse urlResponse = null;
        //when
        Assertions.assertThrows(NullPointerException.class, () -> {
            installer.handleUrlResponse(null);
        });*/
    }



    @Test
    public void handleDownloadedApkSuccessfully()  {
        File file = mock(File.class);
        String fileId = "id";
        AppsInstaller installer = mock(AppsInstaller.class);
        doAnswer(invocation -> {
            File fileResponse = invocation.getArgument(0);
            String id = invocation.getArgument(1);

            assertEquals(fileResponse, file);
            assertEquals(id, fileId);
            return null;
        }).when(installer).handleDownloadedApk(file, fileId);
        installer.handleDownloadedApk(file, fileId);
    }

    @Test
    public void handleDownloadedApkThrowNullPointerException() {
        String fileId = "id";
        AppsInstaller installer = mock(AppsInstaller.class);

        doThrow(NullPointerException.class)
                .when(installer)
                .handleDownloadedApk(null, fileId);


        Assertions.assertThrows(NullPointerException.class, () -> {
            installer.handleDownloadedApk(null, fileId);
        });

    }


    @Test
    public void handleUrlResponseSuccessVerify() {
        //given
        File file = mock(File.class);
        String fileId = "id";
        AppsInstaller installer = mock(AppsInstaller.class);

        //when
        installer.handleDownloadedApk(file, fileId);

        // then
        verify(installer, times(1)).handleDownloadedApk(file, fileId);
    }

    @Test
    public void appInstallSuccessfully() {
        //given
        File file = mock(File.class);
        String fileId = "id";

        //when
        when(updt.updateFileToApp(any())).thenReturn(0);
        //appsInstaller.handleDownloadedApk(file, fileId);

        // then
        assertEquals(updt.uninstallApp(any()), 0);
    }

    @Test
    public void appInstallError() {
        //given
        String applicationId = "applicationId";
        File apkFile = mock(File.class);

        //when
        when(updt.updateFileToApp(any())).thenReturn(-1);
        //appsInstaller.handleDownloadedApk(apkFile, applicationId);

        // then
        assertNotEquals(updt.updateFileToApp(any()), 0);
    }

    @Test
    public void getPackageInfoSuccessfully() throws PackageManager.NameNotFoundException {
        //given
        File file = mock(File.class);
        String applicationId = "applicationId";
        PackageInfo packageInfo = mock(PackageInfo.class);
        PackageManager manager = mock(PackageManager.class);

        //when
        when(context.getPackageManager()).thenReturn(manager);
        when(manager.getPackageInfo(applicationId, PackageManager.GET_META_DATA)).thenReturn(packageInfo);
        PackageInfo result = appsInstaller.findPackageInfo(applicationId);

        //then
        assertEquals(result, packageInfo);
    }

    @Test
    public void errorGettingPackageInfo()  {
        // given
        String applicationId = "applicationId";
        PackageManager manager = mock(PackageManager.class);

        // when
        when(context.getPackageManager()).thenReturn(manager);
        when(appsInstaller.findPackageInfo(applicationId)).thenReturn(null);
        PackageInfo result = appsInstaller.findPackageInfo(applicationId);

        // then
        assertEquals(result, null);
    }


    @Test
    public void packageInfoVerify() {
        // given
        String applicationId = "applicationId";
        AppsInstaller installer = mock(AppsInstaller.class);

        // when
        installer.findPackageInfo(applicationId);

        // then
        verify(installer, times(1)).findPackageInfo(applicationId);
    }

    @Test
    public void finishCheckVerify() {
        // given
        AppsInstaller installer = mock(AppsInstaller.class);

        // when
        installer.finishCheck();

        // then
        verify(installer, times(1)).finishCheck();
    }

    @Test
    public void handleDownloadErrorVerify() {
        // given
        AppsInstaller installer = mock(AppsInstaller.class);

        // when
        installer.handleDownloadError(any(), any());

        // then
        verify(installer, times(1)).handleDownloadError(any(), any());
    }

    @Test
    public void handleDownloadErrorArguments()  {
        IOException exception = mock(IOException.class);
        String applicationId = "applicationId";
        AppsInstaller installer = mock(AppsInstaller.class);
        doAnswer(i -> {
            Exception ex = i.getArgument(0);
            String id = i.getArgument(1);

            assertEquals(exception, ex);
            assertEquals(applicationId, id);
            return null;
        }).when(installer).handleDownloadError(exception, applicationId);
       // appsInstaller.handleDownloadError(exception, applicationId);
    }

    @Test
    public void handleUrlResponseErrorVerify() {
        // given
        AppsInstaller installer = mock(AppsInstaller.class);

        // when
        installer.handleUrlResponseError(any());

        // then
        verify(installer, times(1)).handleUrlResponseError(any());
    }


    @Test
    public void handleUrlResponseErrorArguments() {
        RuntimeException exception = mock(RuntimeException.class);
        AppsInstaller installer = mock(AppsInstaller.class);
        doAnswer(i -> {
            Exception ex = i.getArgument(0);

            assertEquals(exception, ex);
            return null;
        }).when(installer).handleUrlResponseError(exception);
//        appsInstaller.handleUrlResponseError(exception);
    }

    @Test
    public void checkVerify() {
        // given
        AppsInstaller installer = mock(AppsInstaller.class);

        // when
        installer.check(any());

        // then
        verify(installer, times(1)).check(any());
    }


}



