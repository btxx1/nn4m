package com.wallee.vsd.common.integration.data;

import com.wallee.android.till.sdk.data.Receipt;
import com.wallee.android.till.sdk.data.ReceiptType;
import com.wallee.android.till.sdk.data.ResultCode;
import com.wallee.android.till.sdk.data.State;
import com.wallee.vsd.common.integration.xml.PrinterNotification;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Currency;

public class DataUtilsTest {

    @Test
    public void getCurrencyTypeTest() {
        Currency currency = Currency.getInstance("PLN");
        CurrencyType currencyType = DataUtils.getCurrencyType(currency);
        Assert.assertSame(currencyType, CurrencyType.PLN);
    }

    @Test
    public void authResultToStateTest() {
        String authResult = "0";
        State state = DataUtils.authResultToState(authResult);
        Assert.assertSame(state, State.SUCCESSFUL);

        authResult = "99";
        state = DataUtils.authResultToState(authResult);
        Assert.assertSame(state, State.SUCCESSFUL);

        authResult = "160";
        state = DataUtils.authResultToState(authResult);
        Assert.assertSame(state, State.FAILED);

        authResult = null;
        state = DataUtils.authResultToState(authResult);
        Assert.assertSame(state, State.FAILED);
    }

    @Test
    public void authResultToResultCodeTest() {
        String authResult = "0";
        ResultCode resultCode = DataUtils.authResultToResultCode(authResult);
        Assert.assertEquals("0", resultCode.getCode());
        Assert.assertEquals("Transaction approved", resultCode.getDescription());

        authResult = "99";
        resultCode = DataUtils.authResultToResultCode(authResult);
        Assert.assertEquals("99", resultCode.getCode());
        Assert.assertEquals("Transaction approved - RFU", resultCode.getDescription());

        authResult = "160";
        resultCode = DataUtils.authResultToResultCode(authResult);
        Assert.assertEquals("160", resultCode.getCode());
        Assert.assertEquals("Transaction declined - Wrong card number", resultCode.getDescription());

        authResult = null;
        resultCode = DataUtils.authResultToResultCode(authResult);
        Assert.assertEquals("error", resultCode.getCode());
        Assert.assertEquals("Failed to get authResult", resultCode.getDescription());
    }

    @Test
    public void printerNotificationToReceiptTest() {
        PrinterNotification printerNotification = new PrinterNotification();
        printerNotification.merchantReceipt = "receipt";
        Receipt receipt = DataUtils.printerNotificationToReceipt(printerNotification);
        Assert.assertEquals("receipt", receipt.getContent());
        Assert.assertEquals(ReceiptType.MERCHANT, receipt.getReceiptType());

        printerNotification = new PrinterNotification();
        printerNotification.cardholderReceipt = "receipt";
        receipt = DataUtils.printerNotificationToReceipt(printerNotification);
        Assert.assertEquals("receipt", receipt.getContent());
        Assert.assertEquals(ReceiptType.CLIENT, receipt.getReceiptType());
    }

    @Test
    public void isNegativeShouldNotTruncateMinorUnits() {
        Assert.assertTrue(DataUtils.isNegative(BigDecimal.valueOf(-0.5)));
        Assert.assertFalse(DataUtils.isNegative(BigDecimal.valueOf(0.5)));
    }
}
