package com.wallee.vsd.common;

import com.wallee.vsd.common.installer.data.UrlApplication;
import com.wallee.vsd.common.installer.data.UrlResponse;

import java.util.ArrayList;

public class DataFixtures {

    public static UrlResponse getUrlResponse(){
        UrlApplication urlApplication = new UrlApplication();
        urlApplication.version = "380";
        urlApplication.applicationId = "com.nemopay.blenny.prd";
        urlApplication.downloadUrl = "https://dev.customweb.com/hunziker/android-download/upload/blenny/blenny380_sign.apk";
        UrlResponse urlResponse = new UrlResponse();
        urlResponse.applications = new ArrayList<>();
        urlResponse.applications.add(urlApplication);
        urlResponse.lastChanged = "2021-08-05T09:14:12+00:00";
        urlResponse.reportUrl = "https://dev.customweb.com/hunziker/android-download/report.php";
        return urlResponse;

    }
}
