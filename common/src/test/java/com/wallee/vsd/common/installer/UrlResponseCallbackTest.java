package com.wallee.vsd.common.installer;


import com.wallee.vsd.common.DataFixtures;
import com.wallee.vsd.common.installer.data.UrlResponse;

import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.function.Consumer;

import okhttp3.Call;
import okhttp3.Headers;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

//@RunWith(PowerMockRunner.class)
//@PrepareForTest({Response.class, ResponseBody.class, Headers.class})
public class UrlResponseCallbackTest {


    UrlResponseCallback urlResponseCallback;

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();




    @Before
    public void init() {
        /*UrlResponse responseConsumer =  mock(UrlResponse.class);
        Consumer<Exception> errorConsumer = mock(Consumer.class);
        urlResponseCallback = new UrlResponseCallback( responseConsumer, (androidx.core.util.Consumer<Exception>) errorConsumer);*/

    }


    @org.junit.jupiter.api.Test
    public void onFailureVerify() {
        // given
        UrlResponseCallback urlResponseCallback = mock(UrlResponseCallback.class);
        // when
        urlResponseCallback.onFailure(any(), any());

        // then
        verify(urlResponseCallback, times(1)).onFailure(any(), any());
    }

    @org.junit.jupiter.api.Test
    public void onFailureArguments() {
        UrlResponseCallback urlResponseCallback = mock(UrlResponseCallback.class);
        IOException exception = mock(IOException.class);
        Call call = mock(Call.class);
        doAnswer(i -> {
            Call c = i.getArgument(0);
            IOException ex = i.getArgument(1);
            assertEquals(call, c);
            assertEquals(exception, ex);
            return null;
        }).when(urlResponseCallback).onFailure(call,exception);

        urlResponseCallback.onFailure(call, exception);
    }

    @org.junit.jupiter.api.Test
    public void onResponseVerify() throws IOException {
        // given
        UrlResponseCallback urlResponseCallback = mock(UrlResponseCallback.class);
        // when
        urlResponseCallback.onResponse(any(), any());

        // then
        verify(urlResponseCallback, times(1)).onResponse(any(), any());
    }

    @org.junit.jupiter.api.Test
    public void onResponseArguments() throws IOException {
        UrlResponseCallback urlResponseCallback = mock(UrlResponseCallback.class);
        Call call = mock(Call.class);
        Response response  = mock(Response.Builder.class).build();
        doAnswer(i -> {
            Call c = i.getArgument(0);
            Consumer<Response> rsp = i.getArgument(1);
            assertEquals(call, c);
            assertEquals(response, rsp);
            return null;
        }).when(urlResponseCallback).onResponse(call,  response);

        urlResponseCallback.onResponse(call, response);
    }





}