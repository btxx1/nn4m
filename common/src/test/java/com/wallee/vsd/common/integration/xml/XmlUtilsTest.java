package com.wallee.vsd.common.integration.xml;

import org.junit.Assert;
import org.junit.Test;

public class XmlUtilsTest {

    @Test
    public void shouldConfirmTest() {
        String response = "<?xml version=\"1.0\" encoding=\"utf-8\"?><vcs-device:confirmationRequest displayType=\"ATTENDANT\" xmlns:vcs-pos=\"http://www.vibbek.com/pos\" xmlns:vcs-device=\"http://www.vibbek.com/device\"><display><line>Double transaction</line><line>ok?</line></display><timeout>60</timeout><timeoutReminder>60</timeoutReminder></vcs-device:confirmationRequest>";
        Assert.assertTrue(XmlUtils.shouldConfirm(response));
    }

    @Test
    public void isErrorNotificationTest() throws Exception {
        String response = "<?xml version=\"1.0\" encoding=\"utf-8\"?><vcs-pos:errorNotification xmlns:vcs-pos=\"http://www.vibbek.com/pos\" xmlns:vcs-device=\"http://www.vibbek.com/device\"><errorText>Abort</errorText></vcs-pos:errorNotification>";
        Assert.assertTrue(XmlUtils.isErrorNotification(response));
        ErrorNotification parsed = XmlUtils.toErrorNotification(response);
        Assert.assertNotNull(parsed);
    }

    @Test
    public void isPrinterNotificationTest() throws Exception {
        String response = "<?xml version=\"1.0\" encoding=\"utf-8\"?><vcs-device:printerNotification xmlns:vcs-pos=\"http://www.vibbek.com/pos\" xmlns:vcs-device=\"http://www.vibbek.com/device\"><merchantReceipt>\n" +
                "           Vibbek AG        \n" +
                "       In der Luberzen 25   \n" +
                "         CH-8902 Urdorf     \n" +
                "    \n" +
                "    Purchase\n" +
                "    QVSDC Base 01\n" +
                "    contactless\n" +
                "    XXXXXXXXXXXX0010\n" +
                "    \n" +
                "    11.11.2020      14:12:40\n" +
                "    Trm-Id:         31900797\n" +
                "    Act-Id:                2\n" +
                "    AID:      A0000000031010\n" +
                "    Trx. Seq-Cnt:   12154995\n" +
                "    Auth. Code:       099875\n" +
                "    8B7D6483BCC0EE38C233DEA0\n" +
                "    BF8E22DE              07\n" +
                "    \n" +
                "    Total-EFT CHF:     10.00\n" +
                "    \n" +
                "    \n" +
                "    \n" +
                "    \n" +
                "    ________________________\n" +
                "    Signature\n" +
                "    \n" +
                "    </merchantReceipt><cardholderReceipt/><admin>false</admin></vcs-device:printerNotification>";
        Assert.assertTrue(XmlUtils.isPrinterNotification(response));
        PrinterNotification parsed = XmlUtils.toPrinterNotification(response);
        Assert.assertNotNull(parsed);
    }

    @Test
    public void isFinancialTrxResponseTest() throws Exception {
        String response = "<?xml version=\"1.0\" encoding=\"utf-8\"?><vcs-pos:financialTrxResponse xmlns:vcs-pos=\"http://www.vibbek.com/pos\" xmlns:vcs-device=\"http://www.vibbek.com/device\"><ep2AuthResponseCode>00</ep2AuthResponseCode><ep2AuthResult>0</ep2AuthResult><ep2AuthCode>099875</ep2AuthCode><ep2TrmId>31900797</ep2TrmId><ep2TrxSeqCnt>12154995</ep2TrxSeqCnt><trxResult>0</trxResult><amountAuth>1000</amountAuth><amountAuthCurr>756</amountAuthCurr><amountTip>0</amountTip><transactionRefNumber/><cardNumber/><cardSeqNumber>1</cardSeqNumber><cardExpDate>221231</cardExpDate><cardAppLabel>VisaCtless</cardAppLabel><cardAppId>A0000000031010</cardAppId><transactionTime>20201111141240</transactionTime><acquirerId>99999999998</acquirerId><dccTrxAmount/><dccTrxAmountCurr/><cvm>2</cvm><amountRemaining/><partialApprovalFlag>0</partialApprovalFlag><trxSyncNumber/><panToken/></vcs-pos:financialTrxResponse>";
        Assert.assertTrue(XmlUtils.isFinancialTrxResponse(response));
        FinancialTrxResponse parsed = XmlUtils.toFinancialTrxResponse(response);
        Assert.assertNotNull(parsed);
    }

    @Test
    public void isReversalResponseTest() throws Exception {
        String response = "<?xml version=\"1.0\" encoding=\"utf-8\"?><vcs-pos:reversalResponse xmlns:vcs-pos=\"http://www.vibbek.com/pos\" xmlns:vcs-device=\"http://www.vibbek.com/device\"><ep2TrmId>31900797</ep2TrmId><ep2TrxSeqCnt>12154996</ep2TrxSeqCnt><ep2TrxSeqCntOrigTrx>12154995</ep2TrxSeqCntOrigTrx><ep2TrxTypeExtOrigTrx>0</ep2TrxTypeExtOrigTrx><amountRev>1000</amountRev><amountRevCurr>756</amountRevCurr><cardNumber>XXXXXXXXXXXX0010</cardNumber><cardSeqNumber>1</cardSeqNumber><cardExpDate>221231</cardExpDate><cardAppLabel>VisaCtless</cardAppLabel><cardAppId>A0000000031010</cardAppId><transactionTime>20201111141927</transactionTime><acquirerId>99999999998</acquirerId></vcs-pos:reversalResponse>";
        Assert.assertTrue(XmlUtils.isReversalResponse(response));
        ReversalResponse parsed = XmlUtils.toReversalResponse(response);
        Assert.assertNotNull(parsed);
    }

    @Test
    public void isCancelReservationResponseTest() throws Exception {
        String response = "<?xml version=\"1.0\" encoding=\"utf-8\"?><vcs-pos:cancelReservationResponse xmlns:vcs-pos=\"http://www.vibbek.com/pos\" xmlns:vcs-device=\"http://www.vibbek.com/device\"><acquirerId>99999999998</acquirerId><amountAuth>20000</amountAuth><amountAuthCurr>756</amountAuthCurr><ep2AuthResult>0</ep2AuthResult><ep2TrmId>31900797</ep2TrmId><ep2TrxSeqCnt>12154998</ep2TrxSeqCnt><result>0</result><transactionRefNumber>360568</transactionRefNumber><transactionTime>20201111142134</transactionTime></vcs-pos:cancelReservationResponse>";
        Assert.assertTrue(XmlUtils.isCancelReservationResponse(response));
        CancelReservationResponse parsed = XmlUtils.toCancelReservationResponse(response);
        Assert.assertNotNull(parsed);
    }

    @Test
    public void isTransmissionResponseTest() {
        String response = "<?xml version=\"1.0\" encoding=\"utf-8\"?><vcs-pos:beTransmissionResponse xmlns:vcs-pos=\"http://www.vibbek.com/pos\" xmlns:vcs-device=\"http://www.vibbek.com/device\"/>";
        Assert.assertTrue(XmlUtils.isTransmissionResponse(response));
    }

    @Test
    public void isSubmissionResponseTest() {
        String response = "<?xml version=\"1.0\" encoding=\"utf-8\"?><vcs-pos:miSubmissionResponse xmlns:vcs-pos=\"http://www.vibbek.com/pos\" xmlns:vcs-device=\"http://www.vibbek.com/device\"/>";
        Assert.assertTrue(XmlUtils.isSubmissionResponse(response));
    }

    @Test
    public void isFinalBalanceResponseTest() {
        String response = "<?xml version=\"1.0\" encoding=\"utf-8\"?><vcs-pos:beFinalBalanceResponse xmlns:vcs-pos=\"http://www.vibbek.com/pos\" xmlns:vcs-device=\"http://www.vibbek.com/device\"><trxSyncNumber/></vcs-pos:beFinalBalanceResponse>";
        Assert.assertTrue(XmlUtils.isFinalBalanceResponse(response));
    }
}
