package com.wallee.vsd.common;

import android.os.CountDownTimer;
import android.util.Log;

import com.wallee.vsd.common.models.vsd.MiscIface;
import com.wallee.vsd.common.vsd.Misc;

import static com.wallee.vsd.common.vsd.XG.TAG_TOKEN;

/**
 * A watchdog for monitoring the event queue.
 */
public class Watchdog implements JSReadyListener {

    private static final Watchdog INSTANCE = new Watchdog();

    private static final String TAG = "Watchdog";
    private static final String HEARTBEAT = "heartbeat";
    private static final String HEARTBEAT_EVENT = String.format("{\"%s\":\"%s\"}", TAG_TOKEN, HEARTBEAT);
    private static final long WATCHDOG_TIMEOUT = 100_000L;
    private static final long HEARTBEAT_TIMEOUT = WATCHDOG_TIMEOUT / 2;
    private static final long _UNUSED_INTERVAL = WATCHDOG_TIMEOUT * 2;

    private static MiscIface misc;

    private static boolean watchdogKicked = true;

    private static final CountDownTimer heartbeatTimer = new CountDownTimer(HEARTBEAT_TIMEOUT, _UNUSED_INTERVAL) {
        @Override
        public void onTick(long millisUntilFinished) {
        }

        @Override
        public void onFinish() {
            Log.d(TAG, "Kicking watchdog...");
            kickWatchdog();
            this.start();
        }
    };

    private static final CountDownTimer watchdogTimer = new CountDownTimer(WATCHDOG_TIMEOUT, _UNUSED_INTERVAL) {

        @Override
        public void onTick(long millisUntilFinished) {
        }

        @Override
        public void onFinish() {
            Log.e(TAG, "Watchdog not kicked within the time limit. Restarting application...");
            Misc.getInstance().mainAppRestart();
        }
    };

    private Watchdog() {
    }

    public static Watchdog getInstance() {
        return INSTANCE;
    }

    /**
     * Activate the watchdog.
     * <p>
     * To be able to activate the watchdog you first need to configure the misc interface with
     * {@link #setMiscInterface(MiscIface)}
     * <p>
     * The watchdog will try to send a heartbeat event to VPJ every {@value #HEARTBEAT_TIMEOUT} ms. If VPJ was able to
     * consume the event, the watchdog timer is reset. Otherwise the application will be restarted after
     * {@value #WATCHDOG_TIMEOUT} ms.
     */
    public void activate() {
        if (misc == null) {
            throw new IllegalStateException("Misc interface missing. You cannot activate the watchdog without " +
                    "providing the misc interface");
        }
        restart(watchdogTimer);
        restart(heartbeatTimer);
        Log.d(TAG, "Watchdog is active. The application will restart automatically if the watchdog is not kicked " +
                "within the next " + WATCHDOG_TIMEOUT / 1000 + " seconds");
    }

    private static void restart(CountDownTimer timer) {
        timer.cancel();
        timer.start();
    }

    /**
     * Deactivate the watchdog
     */
    public void deactivate() {
        watchdogTimer.cancel();
        heartbeatTimer.cancel();
        Log.d(TAG, "Watchdog is inactive.");
    }

    public void setMiscInterface(MiscIface misc) {
        this.misc = misc;
        this.misc.addJSReadyListener(this);
    }

    @Override
    public void onJSReady() {
        if (!watchdogKicked) {
            kickWatchdog();
        }
    }

    private static void kickWatchdog() {
        watchdogKicked = misc.postEventToJS(HEARTBEAT_EVENT, true);
        if (watchdogKicked) {
            restart(watchdogTimer);
            Log.d(TAG, "Watchdog kicked.");
        }
    }
}
