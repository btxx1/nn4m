package com.wallee.vsd.common.ui;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class IconMenuView extends RecyclerView {
    private static final String TAG = "IconMenuView";
    public static int ICON_SIZE = 108;
    public static int ICON_MENU_HEIGHT = 625;
    private static int ICON_MENU_COLUMNS = 3;
    private static int ICON_MENU_ROWS = 4;
    public final int FWD_SCROLL_ITEMS = ICON_MENU_COLUMNS * ICON_MENU_ROWS;
    private static final int STATUS_BAR_HEIGHT = 50;

    int scrollPos;

    public IconMenuView(@NonNull Context context, int iconMenuColumns) {
        super(context);
        ICON_SIZE = Resources.getSystem().getDisplayMetrics().widthPixels * 3 / 20;
        ICON_MENU_HEIGHT = STATUS_BAR_HEIGHT + ICON_MENU_ROWS * 188;
        ICON_MENU_COLUMNS = iconMenuColumns;
        scrollPos = 0;
    }

    @Override
    protected void onMeasure(int widthSpec, int heightSpec) {
        int count = getAdapter().getItemCount();
        if (count > FWD_SCROLL_ITEMS) {
            int specSize = MeasureSpec.getSize(widthSpec);
            setMeasuredDimension(specSize, ICON_MENU_HEIGHT); // TODO: how to calculate height depending on other controls?
        } else {
            super.onMeasure(widthSpec, heightSpec);
        }
    }

    public void scrollForward() {
        int count = getAdapter().getItemCount();
        scrollPos += FWD_SCROLL_ITEMS;
        if (scrollPos >= count) {
            scrollPos = 0;
            smoothScrollToPosition(0); // scroll back so that 0 pos is visible
        } else {
            smoothScrollToPosition(scrollPos + FWD_SCROLL_ITEMS);
        }
    }
}
