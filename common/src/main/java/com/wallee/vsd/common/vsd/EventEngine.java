package com.wallee.vsd.common.vsd;

import android.webkit.JavascriptInterface;
import android.util.Log;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Semaphore;

import com.wallee.vsd.common.models.EventEngineIface;


public class EventEngine implements EventEngineIface {

    public static final String TAG = "EventEngine";

    private static ArrayBlockingQueue<Integer> jsSelectRequestsQueue = new ArrayBlockingQueue<>(20);
    private static volatile boolean pageIsReady = false;

    public static boolean isPageReady() {
        return pageIsReady;
    }

    public static void resetPageReady() {
        pageIsReady = false;
    }
    @Override
    @JavascriptInterface
    public void notifyPageReady() {
        Log.d(TAG, "+notifyPageReady: "+Thread.currentThread().getName());
        pageIsReady = true;
        EventEngine.internalNotifyJSReady();
        Log.d(TAG, "-notifyPageReady");
    }

    static public void waitForJSReady() throws InterruptedException {
        Integer eid = jsSelectRequestsQueue.take();
        Log.d(TAG, "JSSYNCH: waitForJSReady: got tag:" + eid + "queue size:" + jsSelectRequestsQueue.size());
        while (eid != null) {
            eid = jsSelectRequestsQueue.poll();
            if(eid!=null) {
                Log.d(TAG, "JSSYNCH: waitForJSReady: got tag:" + eid);
            }
        }
    }

    static public int jssequence = 0;

    @Override
    @JavascriptInterface
    public int notifyJSReady() {
        return 0;
    }

    static public int internalNotifyJSReady() {
        Log.d(TAG, "JSSYNCH: vsdNotifyJSReady tag:" + jssequence + ", queue size: " + jsSelectRequestsQueue.size());
        jsSelectRequestsQueue.add(jssequence++);
        return 0;
    }

    @Override
    @JavascriptInterface
    public int vsdNotifyJSReady() {
        return internalNotifyJSReady();
    }

}
