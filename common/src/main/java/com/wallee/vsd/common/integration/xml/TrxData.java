package com.wallee.vsd.common.integration.xml;

import org.simpleframework.xml.Element;

public class TrxData {
    @Element(required = false)
    public String amount;
    @Element(required = false)
    public String currency;
    @Element(required = false)
    public String transactionType;
    @Element(required = false)
    public String transactionRefNumber;
    @Element(required = false)
    public String additionalMerchantData;

    public TrxData(String amount, String currency, String transactionType, String transactionRefNumber) {
        this.amount = amount;
        this.currency = currency;
        this.transactionType = transactionType;
        this.transactionRefNumber = transactionRefNumber;
    }

    public TrxData(String amount, String currency, String transactionType, String transactionRefNumber, String additionalMerchantData) {
        this(amount, currency, transactionType, transactionRefNumber);
        this.additionalMerchantData = additionalMerchantData;
    }
}
