package com.wallee.vsd.common.os;

import android.content.Context;
import android.media.AudioManager;
import android.widget.SeekBar;

import com.wallee.vsd.common.vsd.Misc;

public class Volume extends com.wallee.vsd.common.os.SliderBar {

    @Override
    public void adjustParam() {
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        try {
            super.sliderBar.setMax(audioManager
                    .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
            super.sliderBar.setProgress(audioManager
                    .getStreamVolume(AudioManager.STREAM_MUSIC));

            super.sliderBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onStopTrackingTouch(SeekBar arg0) {
                }

                @Override
                public void onStartTrackingTouch(SeekBar arg0) {
                }

                @Override
                public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                            progress, 0);
                    Misc.beeps(1500, 100);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
