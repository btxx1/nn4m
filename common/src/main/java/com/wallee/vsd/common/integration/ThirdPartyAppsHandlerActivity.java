package com.wallee.vsd.common.integration;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;

import androidx.annotation.Nullable;

import com.wallee.android.till.sdk.ApiMessageType;
import com.wallee.android.till.sdk.Utils;
import com.wallee.android.till.sdk.data.Transaction;
import com.wallee.android.till.sdk.data.TransactionResponse;
import com.wallee.vsd.common.integration.data.ThirdPartyApiMessageType;
import com.wallee.vsd.common.ui.CurrentActivity;
import com.wallee.vsd.common.vsd.XG;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ThirdPartyAppsHandlerActivity extends CurrentActivity {
    private static final String TAG = "ThirdPartyAppsHandlerAc";

    private static final int REQUEST_CODE = 6699;

    private static final String INTERCEPTION_TYPE_KEY = "INTERCEPTION_TYPE_KEY";
    private static final String MESSAGE_KEY = "MESSAGE_KEY";
    private static final String FINAL_REQUEST_RESPONSE_KEY = "FINAL_REQUEST_RESPONSE_KEY";

    // start required
    private InterceptionType interceptionType;
    private Message message;
    private ThirdPartyApiMessageType apiType;

    // processing required
    private LinkedList<ActivityInfo> thirdPartyActivities;
    private Bundle lastHandledData;

    private enum InterceptionType {
        BEFORE,
        AFTER
    }

    public static void start(Message message) {
        Intent intent = new Intent(XG.getMainActivity(), ThirdPartyAppsHandlerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(INTERCEPTION_TYPE_KEY, InterceptionType.BEFORE);
        intent.putExtra(MESSAGE_KEY, message);
        XG.getMainActivity().startActivity(intent);
    }

    private static void start(Message message, Bundle finalRequestResponse) {
        Intent intent = new Intent(XG.getMainActivity(), ThirdPartyAppsHandlerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(INTERCEPTION_TYPE_KEY, InterceptionType.AFTER);
        intent.putExtra(MESSAGE_KEY, message);
        intent.putExtra(FINAL_REQUEST_RESPONSE_KEY, finalRequestResponse);
        XG.getMainActivity().startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleStartIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleStartIntent(intent);
    }

    private void handleStartIntent(Intent intent) {
        Log.d(TAG, "handleStartIntent: " + intent);
        thirdPartyActivities = null;
        lastHandledData = null;
        try {
            interceptionType = (InterceptionType) intent.getSerializableExtra(INTERCEPTION_TYPE_KEY);
            message = intent.getParcelableExtra(MESSAGE_KEY);
            apiType = ThirdPartyApiMessageType.find(ApiMessageType.values()[message.arg1]);
            handleNext(interceptionType == InterceptionType.BEFORE ? message.getData() : intent.getBundleExtra(FINAL_REQUEST_RESPONSE_KEY));
        } catch (Exception e) {
            Log.d(TAG, "Failed to process data during handleStartIntent", e);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == REQUEST_CODE) {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    handleNext(data.getExtras());
                } else {
                    handleNext(lastHandledData);
                }
            }
        } catch (Exception e) {
            Log.d(TAG, "Failed to process data during onActivityResult", e);
            finish();
        }
    }

    private void handleNext(Bundle data) {
        Log.d(TAG, "handleNext: interceptionType: " + interceptionType + " data: " + data);
        lastHandledData = validateData(data);
        if (thirdPartyActivities == null) {
            String action = interceptionType == InterceptionType.BEFORE ? apiType.actionBefore : apiType.actionAfter;
            thirdPartyActivities = new LinkedList<>(getThirdPartyActivities(action));
        }
        ActivityInfo activityInfo = thirdPartyActivities.pollFirst();
        if (activityInfo != null) {
            Intent intent = new Intent();
            intent.setClassName(activityInfo.packageName, activityInfo.name);
            intent.putExtras(data);
            startActivityForResult(intent, REQUEST_CODE);
        } else if (interceptionType == InterceptionType.BEFORE) {
            handleFinalRequest();
        } else if (interceptionType == InterceptionType.AFTER) {
            handleFinalResponse();
        } else {
            throw new IllegalStateException("Internal error");
        }
    }

    private Bundle validateData(Bundle data) {
        boolean valid = false;
        try {
            switch (apiType) {
                case AUTHORIZE_TRANSACTION:
                    Object result = interceptionType == InterceptionType.BEFORE ? Utils.getTransaction(data) : Utils.getTransactionResponse(data);
                    valid = result != null;
                    break;
                default:
                    throw new IllegalStateException(apiType + " is not supported for third party integration");
            }
        } catch (Exception e) {
            Log.d(TAG, "HandleFinalRequestThread", e);
        }
        if (!valid) {
            Log.d(TAG, "data is not valid - " + data);
            if (lastHandledData != null) {
                return lastHandledData;
            } else {
                throw new IllegalStateException("lastHandledData is null");
            }
        }
        Log.d(TAG, "data is valid - " + data);
        return data;
    }

    private void handleFinalRequest() {
        new HandleFinalRequestThread(message, apiType, lastHandledData).start();
    }

    private void handleFinalResponse() {
        Log.d(TAG, "handleFinalResponse: " + lastHandledData);
        try {
            Message response = Message.obtain();
            response.arg1 = apiType.original.ordinal();
            response.setData(lastHandledData);
            message.replyTo.send(response);
        } catch (Exception e) {
            Log.d(TAG, "Failed to handleFinalResponse", e);
        }
        finish();
    }

    private List<ActivityInfo> getThirdPartyActivities(String action) {
        List<ActivityInfo> result = new ArrayList<>();
        Intent intent = new Intent(action);
        List<ResolveInfo> resolveInfos = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo info : resolveInfos) {
            result.add(info.activityInfo);
        }
        Collections.sort(result, (o1, o2) -> o1.packageName.compareTo(o2.packageName));
        return result;
    }

    private static final class HandleFinalRequestThread extends Thread {
        private final Message originalMessage;
        private final ThirdPartyApiMessageType apiType;
        private final Bundle data;

        private HandleFinalRequestThread(Message originalMessage, ThirdPartyApiMessageType apiType, Bundle data) {
            this.originalMessage = originalMessage;
            this.apiType = apiType;
            this.data = data;
        }

        @Override
        public void run() {
            Log.d(TAG, "HandleFinalRequestThread: " + data);
            try {
                switch (apiType) {
                    case AUTHORIZE_TRANSACTION:
                        Transaction transaction = Utils.getTransaction(data);
                        TransactionResponse transactionResponse = new AuthorizeTransactionHandler().handle(transaction);
                        Bundle finalRequestResponse = Utils.toBundle(transactionResponse);
                        ThirdPartyAppsHandlerActivity.start(originalMessage, finalRequestResponse);
                        break;
                    default:
                        throw new IllegalStateException(apiType + " is not supported for third party integration");
                }
            } catch (Exception e) {
                Log.d(TAG, "HandleFinalRequestThread", e);
                ThirdPartyAppsHandlerActivity.start(originalMessage, null); // required in order to finish activity
            }
        }
    }
}
