package com.wallee.vsd.common.vsd;

import android.util.Log;
import android.webkit.JavascriptInterface;

import com.wallee.vsd.common.models.vsd.PedIface;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class Ped implements PedIface {
    private static final String TAG = "Ped";

    protected void postPinEntryReady() {
        final JSONObject res = new JSONObject();
        try {
            res.put(XG.TAG_TOKEN, XG.XG_TOKEN_PIN_BEFORE_EDIT);
            res.put(XG.TAG_VALUE, 0);
        } catch (JSONException e) {
            Log.e(TAG, "Failed to construct json response", e);
        }
        XG.postEventBlocking(res.toString());
        XG.requestFocus();
    }

    @Override
    @JavascriptInterface
    public String createEp2Cryptogram(int sessionKeyId, int compSecretId, String senderId, String publicKeyFormat, int version, String pubKeyId) {
        return createEp2Cryptogram(sessionKeyId, compSecretId, senderId, publicKeyFormat, version);
    }

    @Override
    @JavascriptInterface
    public int storePublicKey(int macRecvKeyIdx, String message,  String macHex,  String pubKeyId, String publicKey) {
        Log.d(TAG, "storePublicKey called, macRecvKeyIdx:" + macRecvKeyIdx);
        return 0;
    }

    @Override
    @JavascriptInterface
    public int storePublicKey(int macRecvKeyIdx, String message,  String macHex,  String pubKeyId, String publicKey, boolean isJSON) {
        Log.d(TAG, "storePublicKey with isJSON called,  macRecvKeyIdx:" + macRecvKeyIdx);
        return 0;
    }

}
