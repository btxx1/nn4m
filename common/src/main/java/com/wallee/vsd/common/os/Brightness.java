package com.wallee.vsd.common.os;

import android.provider.Settings;
import android.widget.SeekBar;

public class Brightness extends com.wallee.vsd.common.os.SliderBar {
    int brightness;

    @Override
    public void adjustParam() {
        try {
            brightness =
                    Settings.System.getInt(super.context.getContentResolver(),
                            Settings.System.SCREEN_BRIGHTNESS, 0);
            super.sliderBar.setProgress(brightness);
            super.sliderBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, progress);
                    resetTimeout();
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


