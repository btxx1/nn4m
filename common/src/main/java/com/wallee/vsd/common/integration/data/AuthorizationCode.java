package com.wallee.vsd.common.integration.data;

public enum AuthorizationCode {
    APPROVED_0(0, "Transaction approved"),
    APPROVED_1(1, "Transaction approved - Duplicate operation"),
    APPROVED_2(2, "Transaction approved - no liability shift"),
    APPROVED_3(3, "Transaction approved - Address is correct, Postal Code is incorrect"),
    APPROVED_4(4, "Transaction approved - Neither Address nor Postal Code is correct"),
    APPROVED_5(5, "Transaction approved - Retry, temporary unavailable"),
    APPROVED_6(6, "Transaction approved - Service not allowed, Acquirer not certified"),
    APPROVED_7(7, "Transaction approved - Information unavailable, Automated Address Verification is not supported by the issuer"),
    APPROVED_8(8, "Transaction approved - both Address and Postal Code are correct"),
    APPROVED_9(9, "Transaction approved - Postal Code is correct, Address incorrect or Address not included in request"),
    APPROVED_10(10, "Transaction approved - Original Reservation Data included"),
    APPROVED_11(11, "Transaction approved - Restricted approval"),
    APPROVED_12(12, "Transaction approved - PIN changed"),
    APPROVED_13(13, "Transaction approved - Partial approval"),
    APPROVED_14(14, "Transaction approved - Partial and restricted approval"),
    APPROVED_99(15, 99, "Transaction approved - RFU"),
    DECLINED_100(100, "Transaction declined - Generic"),
    DECLINED_101(101, "Transaction declined - Card error"),
    DECLINED_102(102, "Transaction declined - Card expired"),
    DECLINED_103(103, "Transaction declined - Card unknown"),
    DECLINED_104(104, "Transaction declined - Duplicate transaction"),
    DECLINED_105(105, "Transaction declined - Funds too low"),
    DECLINED_106(106, "Transaction declined - Amount invalid"),
    DECLINED_107(107, "Transaction declined - Transaction invalid"),
    DECLINED_108(108, "Transaction declined - PIN incorrect"),
    DECLINED_109(109, "Transaction declined - System error"),
    DECLINED_110(110, "Transaction declined - Transaction repetition"),
    DECLINED_111(111, "Transaction declined - Try later"),
    DECLINED_112(112, "Transaction declined - Withdrawal amount exceed"),
    DECLINED_113(113, "Transaction declined - Withdrawal frequency exceeded"),
    DECLINED_114(114, "Transaction declined - Allowable number of PIN tries exceeded"),
    DECLINED_115(115, "Transaction declined - Card blocked"),
    DECLINED_116(116, "Transaction declined - PIN mandatory"),
    DECLINED_117(117, "Transaction declined - Cashback amount exceeded"),
    DECLINED_118(118, "Transaction declined - Cashback not allowed"),
    DECLINED_119(119, "Transaction declined - Try another interface"),
    DECLINED_160(160, "Transaction declined - Wrong card number"),
    DECLINED_161(161, "Transaction declined - Wrong card expiry date"),
    DECLINED_162(162, "Transaction declined - Authentication failed"),
    DECLINED_165(165, "Transaction declined - Address is correct, Postal Code is incorrect"),
    DECLINED_166(166, "Transaction declined - Neither Address nor Postal Code is correct"),
    DECLINED_167(167, "Transaction declined - Retry, temporary unavailable"),
    DECLINED_168(168, "Transaction declined - Service not allowed, Acquirer not certified"),
    DECLINED_169(169, "Transaction declined - Information unavailable, Automated Address Verification is not supported by the issuer"),
    DECLINED_170(170, "Transaction declined - both Address and Postal Code are correct"),
    DECLINED_171(171, "Transaction declined - Postal Code is correct, Address incorrect or Address not included in request"),
    DECLINED_199(172, 199, "Transaction declined - RFU"),
    REFERRAL_200(200, "Transaction referral - Generic"),
    REFERRAL_201(201, "Transaction referral - Referral declined, wrong <Authorisation Code>"),
    REFERRAL_202(202, "Transaction referral - Referral declined, wrong <Amount, Authorised (numeric)>"),
    REFERRAL_203(203, "Transaction referral - Referral declined, other reasons"),
    REFERRAL_299(204, 299, "Transaction referral - RFU"),
    UNKNOWN(-1, -1, "Unknown code");

    public final int codeFrom;
    public final int codeTo;
    public final String description;

    AuthorizationCode(int codeFrom, int codeTo, String description) {
        this.codeFrom = codeFrom;
        this.codeTo = codeTo;
        this.description = description;
    }

    AuthorizationCode(int code, String description) {
        this(code, code, description);
    }

    public static AuthorizationCode find(int code) {
        for (AuthorizationCode ac : AuthorizationCode.values()) {
            if (code >= ac.codeFrom && code <= ac.codeTo) {
                return ac;
            }
        }
        return UNKNOWN;
    }
}
