package com.wallee.vsd.common.integration;

import android.util.Log;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

class NetClient {
    public static final String TAG = "NetClient";

    private static final String HOST = "127.0.0.1";
    private static final int PORT = 50000;

    private Socket socket = null;
    private DataOutputStream out = null;
    private DataInputStream in = null;

    boolean connectWithServer() {
        try {
            socket = new Socket(HOST, PORT);
            out = new DataOutputStream(socket.getOutputStream());
            in = new DataInputStream(socket.getInputStream());
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    void disConnectWithServer() {
        if (socket != null) {
            if (socket.isConnected()) {
                try {
                    in.close();
                    out.close();
                    socket.close();
                } catch (IOException e) {
                    // ignore
                }
            }
        }
    }

    void send(String data) throws IOException {
        Log.d(TAG, "send: " + data);
        byte[] bytes = data.getBytes(StandardCharsets.UTF_8);
        out.writeInt(bytes.length);
        out.write(bytes);
        out.flush();
    }

    String receive() throws IOException {
        int msgLen = in.readInt();
        byte[] res = new byte[msgLen];
        in.readFully(res);
        String data = new String(res);
        Log.d(TAG, "receive: " + data);
        return data;
    }
}
