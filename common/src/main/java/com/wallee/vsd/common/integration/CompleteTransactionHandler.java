package com.wallee.vsd.common.integration;

import com.wallee.android.till.sdk.data.Receipt;
import com.wallee.android.till.sdk.data.ResultCode;
import com.wallee.android.till.sdk.data.State;
import com.wallee.android.till.sdk.data.TransactionCompletion;
import com.wallee.android.till.sdk.data.TransactionCompletionResponse;
import com.wallee.vsd.common.integration.data.CurrencyType;
import com.wallee.vsd.common.integration.data.DataUtils;
import com.wallee.vsd.common.integration.xml.FinancialTrxRequest;
import com.wallee.vsd.common.integration.xml.FinancialTrxResponse;
import com.wallee.vsd.common.integration.xml.TrxData;
import com.wallee.vsd.common.integration.xml.XmlUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class CompleteTransactionHandler {
    private final NetClient client = new NetClient();

    TransactionCompletionResponse handle(TransactionCompletion transaction) {
        if (client.connectWithServer()) {
            try {
                FinancialTrxRequest xmlRequest = createXmlRequest(transaction);
                List<Receipt> receipts = new ArrayList<>();
                TransactionCompletionResponse response = null;

                client.send(XmlUtils.toXml(xmlRequest));
                while (response == null) {
                    String xmlResponse = client.receive();
                    if (xmlResponse.isEmpty()) {
                        response = createErrorResponse(transaction, "Internal error - empty response");
                    } else if (XmlUtils.shouldConfirm(xmlResponse)) {
                        client.send(XmlUtils.CONFIRMATION);
                    } else if (XmlUtils.isPrinterNotification(xmlResponse)) {
                        Receipt receipt = DataUtils.printerNotificationToReceipt(XmlUtils.toPrinterNotification(xmlResponse));
                        if (receipt != null) {
                            receipts.add(receipt);
                        }
                    } else if (XmlUtils.isErrorNotification(xmlResponse)) {
                        response = createErrorResponse(transaction, XmlUtils.toErrorNotification(xmlResponse).errorText);
                    } else if (XmlUtils.isFinancialTrxResponse(xmlResponse)) {
                        response = createResponse(transaction, receipts, XmlUtils.toFinancialTrxResponse(xmlResponse));
                    }
                }

                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return createErrorResponse(transaction, e.getMessage());
            } finally {
                client.disConnectWithServer();
            }
        } else {
            return createErrorResponse(transaction, "Failed to connect to VPJ");
        }
    }

    private FinancialTrxRequest createXmlRequest(TransactionCompletion transaction) {
        CurrencyType currencyType = DataUtils.getCurrencyType(transaction.getCurrency());
        return new FinancialTrxRequest(
                DataUtils.POS_ID,
                new TrxData(
                        DataUtils.toMinorUnits(transaction.getTotalAmountIncludingTax(), currencyType).toString(),
                        currencyType.code,
                        "1",
                        transaction.getReserveReference().toString()
                )
        );
    }

    private TransactionCompletionResponse createResponse(TransactionCompletion transaction, List<Receipt> receipts, FinancialTrxResponse response) {
        return new TransactionCompletionResponse(
                transaction,
                DataUtils.authResultToState(response.ep2AuthResult),
                DataUtils.authResultToResultCode(response.ep2AuthResult),
                response.ep2AuthCode,
                response.ep2TrmId,
                DataUtils.stringToLong(response.ep2TrxSeqCnt),
                response.transactionTime,
                receipts
        );
    }

    private TransactionCompletionResponse createErrorResponse(TransactionCompletion transaction, String error) {
        return new TransactionCompletionResponse(
                transaction,
                State.FAILED,
                new ResultCode(
                        "error",
                        error
                ),
                null,
                null,
                null,
                null,
                Collections.emptyList()
        );
    }
}
