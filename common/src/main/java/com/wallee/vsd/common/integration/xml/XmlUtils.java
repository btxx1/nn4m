package com.wallee.vsd.common.integration.xml;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.stream.Format;
import org.simpleframework.xml.transform.RegistryMatcher;

import java.io.ByteArrayOutputStream;

public final class XmlUtils {
    public static String CONFIRMATION = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
            "<vcs-pos:confirmationResponse xmlns:vcs-pos=\"http://www.vibbek.com/pos\">\n" +
            "<result>1</result>\n" +
            "</vcs-pos:confirmationResponse>";

    public static boolean shouldConfirm(String xmlResponse) {
        return xmlResponse.contains("confirmationRequest");
    }

    public static boolean isErrorNotification(String xmlResponse) {
        return xmlResponse.contains("errorNotification");
    }

    public static ErrorNotification toErrorNotification(String xmlResponse) throws Exception {
        return fromXml(ErrorNotification.class, xmlResponse);
    }

    public static boolean isPrinterNotification(String xmlResponse) {
        return xmlResponse.contains("printerNotification");
    }

    public static PrinterNotification toPrinterNotification(String xmlResponse) throws Exception {
        return fromXml(PrinterNotification.class, xmlResponse);
    }

    public static boolean isFinancialTrxResponse(String xmlResponse) {
        return xmlResponse.contains("financialTrxResponse");
    }

    public static FinancialTrxResponse toFinancialTrxResponse(String xmlResponse) throws Exception {
        return fromXml(FinancialTrxResponse.class, xmlResponse);
    }

    public static boolean isReversalResponse(String xmlResponse) {
        return xmlResponse.contains("reversalResponse");
    }

    public static ReversalResponse toReversalResponse(String xmlResponse) throws Exception {
        return fromXml(ReversalResponse.class, xmlResponse);
    }

    public static boolean isCancelReservationResponse(String xmlResponse) {
        return xmlResponse.contains("cancelReservationResponse");
    }

    public static CancelReservationResponse toCancelReservationResponse(String xmlResponse) throws Exception {
        return fromXml(CancelReservationResponse.class, xmlResponse);
    }

    public static boolean isFinalBalanceResponse(String xmlResponse) {
        return xmlResponse.contains("beFinalBalanceResponse");
    }

    public static BeFinalBalanceResponse toFinalBalanceResponse(String xmlResponse) throws Exception {
        return fromXml(BeFinalBalanceResponse.class, xmlResponse);
    }

    public static boolean isTransmissionResponse(String xmlResponse) {
        return xmlResponse.contains("beTransmissionResponse");
    }

    public static boolean isSubmissionResponse(String xmlResponse) {
        return xmlResponse.contains("miSubmissionResponse");
    }

    public static String toXml(Object object) throws Exception {
        Serializer serializer = new Persister(new Format("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"));

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        serializer.write(object, baos);

        return baos.toString();
    }

    public static <T> T fromXml(Class<? extends T> type, String xml) throws Exception {
        Persister serializer = new Persister(new RegistryMatcher());

        return serializer.read(type, xml);
    }
}
