package com.wallee.vsd.common.vsd;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.Network;
import android.net.RouteInfo;
import android.os.Build;
import android.os.Environment;
import android.os.Looper;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.wallee.vsd.common.JSReadyListener;
import com.wallee.vsd.common.R;
import com.wallee.vsd.common.Timers;
import com.wallee.vsd.common.Util;
import com.wallee.vsd.common.applications.ApplicationsActivity;
import com.wallee.vsd.common.models.IOReader;
import com.wallee.vsd.common.models.vsd.MiscIface;
import com.wallee.vsd.common.models.vsd.TcpSocketIface;
import com.wallee.vsd.common.ui.CurrentActivity;
import com.wallee.vsd.common.ui.LedView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.ClosedSelectorException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Stack;
import java.util.Vector;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Misc implements MiscIface {

    static public final String TAG = "Misc";
    static private TcpSocketIface sockets;
    static private Selector selector = null;
    static protected AppCompatActivity mainActivity = null;
    protected Context mContext;
    private WebView webView;
    public boolean ShowAndActivateSystemBars = false;

    public static AppCompatActivity getCurrentActivity() {
        return Misc.mainActivity;
    }

    static class MiscAudioTrack extends AudioTrack implements AudioTrack.OnPlaybackPositionUpdateListener {
        private int bufferSizeInFrames;

        public MiscAudioTrack(int streamType, int sampleRateInHz, int channelConfig, int audioFormat, int bufferSizeInBytes, int mode) throws IllegalArgumentException {
            super(streamType, sampleRateInHz, channelConfig, audioFormat, bufferSizeInBytes, mode);
            this.bufferSizeInFrames = bufferSizeInBytes/2; // TODO: is it the right size?
        }

        @Override
        public void play() {
            this.setPlaybackPositionUpdateListener(this);
            this.setNotificationMarkerPosition(this.bufferSizeInFrames);
            super.play();
        }

        @Override
        public void onPeriodicNotification(AudioTrack track) {
            // nothing to do
        }

        @Override
        public void onMarkerReached(AudioTrack track) {
            Log.d(TAG, "Sound ended");
            this.release();
        }
    }

    class BeepRequest {
        private final int hz;
        private final int timems;

        BeepRequest(int hz, int timems) {
            this.hz = hz;
            this.timems = timems;
        }

        void beep() {
            Misc.beeps(this.hz, this.timems);
        }
    }

    static BlockingQueue<BeepRequest> beepRequests = new ArrayBlockingQueue<BeepRequest>(10);

    static Thread beepThread = null;
    static {
        beepThread = new Thread(new Runnable() {
            public void run() {
                while(true) {
                    try {
                        BeepRequest beepRequest = beepRequests.take();
                        beepRequest.beep();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
        beepThread.start();
    }


    public void wakeup() {
        try {
            selector.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void postResult(String result) {
        postSelectEvent(result);
    }

    public void activateBackButton(boolean activate) {

    }

    @Override
    public void activateAllSystemButtons(boolean activate) {

    }

    private static Misc instance;

    @Override
    public void initInstance(AppCompatActivity mainActivity, TcpSocketIface tcpSocketInterface, WebView webView) {
        /*if (this.instance != null) {
            Log.e(TAG, "Can't create another instance of Misc");
            return;
        }*/
        this.mainActivity = mainActivity;
        this.mContext = mainActivity;
        sockets = tcpSocketInterface;
        ioReadersMap = new ConcurrentHashMap<Integer, IOReader>();
        ioReaders = new ArrayList<IOReader>();
        try {
            selector = Selector.open();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.instance = this;
        this.webView = webView;
    }

    public static Misc getInstance() {
        return Misc.instance;
    }

    public void addIOReaderMapping(IOReader ioReader) {
        this.ioReadersMap.put(ioReader.getDescriptor(), ioReader);
    }

    public void removeIOReaderMapping(IOReader ioReader) {
        this.ioReadersMap.remove(ioReader.getDescriptor());
    }

    @Override
    @JavascriptInterface
    public int init() {

        return 0;
    }

    @Override
    @JavascriptInterface
    public String getOsVersion() {
        return Build.VERSION.RELEASE;
    }

    @Override
    @JavascriptInterface
    public String getBootVersion() {
        return "";
    }


    @Override
    @JavascriptInterface
    public int reboot() {
        mainAppExit();
        return 0;
    }

    @Override
    @JavascriptInterface
    public int rebootIfNeeded() {
        // TODO
        throw new RuntimeException("NOT IMPLEMENTED");
    }

    @Override
    @JavascriptInterface
    public int poweroff() {
        // TODO
        throw new RuntimeException("NOT IMPLEMENTED");
    }

    @Override
    @JavascriptInterface
    public int mainAppExit() {
        mainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Misc.sockets.closeAll();
                EventEngine.resetPageReady();

                final WebView webview = (WebView) mainActivity.findViewById(R.id.webView);
                webView.reload();
            }
        });
        return 0;
    }

    @Override
    @JavascriptInterface
    public void mainAppRestart() {
        mainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final PackageManager pm = mainActivity.getPackageManager();
                final Intent intent = pm.getLaunchIntentForPackage(mainActivity.getPackageName());
                mainActivity.finishAndRemoveTask();
                mainActivity.startActivity(intent);
                System.exit(0);
            }
        });
    }

    static final private Lock lock = new ReentrantLock();
    static final private Condition eventReady = lock.newCondition();
    static private volatile boolean should_run = false;

    static int[] readFds = new int[0];
    static int[] writeFds = new int[0];
    static int[] exFds = new int[0];
    static int tv_sec;
    static int tv_usec;

    static boolean refreshRequest;
    static Semaphore refreshSemaphore = new Semaphore(1);
    private static volatile String lastSelectResult = null;

    private Map<Integer, IOReader> ioReadersMap;
    private ArrayList<IOReader> ioReaders;

    static {
        try {
            refreshSemaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    static private String selectResultToEvent(Set<SelectionKey> selectedKeys) {
        StringBuilder read = new StringBuilder();
        StringBuilder write = new StringBuilder();
        Iterator<SelectionKey> iter = selectedKeys.iterator();
        while (iter.hasNext()) {
            SelectionKey key = iter.next();
            Integer fd = (Integer) key.attachment();
            if (key.isReadable() || key.isAcceptable()) {
                key.interestOps(0); // po co to ?
                read.append(',');
                read.append(fd.toString());
            }
            if(key.isWritable()) {
                key.interestOps(0); // po co to ?
                write.append(',');
                write.append(fd.toString());
            }
        }
        String result;
        if(read.length()>0 || write.length()>0) {
            result = String.format("{\"%s\":0,\"%s\":[%s],\"%s\":[%s],\"%s\":[]}",
                    XG.TAG_RC,
                    XG.TAG_READ_DESCRIPTORS, read.length() > 0 ? read.substring(1) : "",
                    XG.TAG_WRITE_DESCRIPTORS, write.length() > 0 ? write.substring(1) : "",
                    XG.TAG_EXCEPT_DESCRIPTORS);
            Log.d(TAG, "select result: " + result);
        } else {
            result = null;
        }
        return result;
    }

    private void rawSelect() {

        try {
            int selectRes = 0;

            boolean ballIsOnTheJSCourt = true;
            while (should_run) {
                if (!selector.isOpen()) {
                    selector = Selector.open();
                }
                if (ballIsOnTheJSCourt) {
                    try {
                        EventEngine.waitForJSReady();
                        Log.d(TAG, "VPJ handler completed");
                    } catch (InterruptedException e) {
                        should_run = false;
                    }
                    ballIsOnTheJSCourt = false;
                }
                lock.lock();
                sockets.register(selector, readFds.clone(), writeFds.clone(), exFds.clone());
                lock.unlock();
                selector.selectedKeys().clear();
                selectRes = selector.select();
                if (selectRes > 0 && !selector.selectedKeys().isEmpty()) {
                    Set<SelectionKey> selectedKeys = selector.selectedKeys();
                    ballIsOnTheJSCourt = postSelectEventIfChannelActive(selectedKeys);

                }
            }
        } catch (IOException e) {
            Log.e(TAG, "IOException");
            e.printStackTrace();
        } catch (ClosedSelectorException e) {
            Log.d(TAG, "Selector closed - some other event occured");
        } finally {
            if (selector != null && selector.isOpen()) {
                try {
                    selector.selectedKeys().clear();
                    selector.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    static String getCurrentActiveDescriptors() {
        // this function is called by ui thread post event handler after null event is received
        // to read active descriptors it wakes up select thread which then runs select with 0 timeout to obtain current active descriptor list
        String lcl_lastSelectResult;
        lock.lock();
        lcl_lastSelectResult = lastSelectResult;
        lastSelectResult = null;
        lock.unlock();
        return lcl_lastSelectResult;
    }

    public void addIoReader() {

    }

    @Override
    @JavascriptInterface
    public int updateDescriptors(String readDescriptors, String writeDescriptors, String exceptDescriptors) {
        lock.lock();
//        Log.d(TAG, "read: " + readDescriptors);
//        Log.d(TAG, "write: " + writeDescriptors);
//        Log.d(TAG, "except: " + exceptDescriptors);

        readFds = Util.getIntArrayFromCsv(readDescriptors);
        writeFds = Util.getIntArrayFromCsv(writeDescriptors);
        exFds = Util.getIntArrayFromCsv(exceptDescriptors);
        ioReaders.clear();
        for (int i = 0; i < readFds.length; ++i) {
            IOReader ioReader = ioReadersMap.get(readFds[i]);
            if (ioReader != null) {
                ioReaders.add(ioReader);
                readFds[i] = -1;
            }
        }
        lastSelectResult = null;
        lock.unlock();
        selector.wakeup();
        return 0;
    }

    @Override
    @JavascriptInterface
    public int select(final String readDescriptors, final String writeDescriptors, final String exceptDescriptors, final int tv_sec, int tv_usec) {
        Log.d(TAG, String.format("Select: %s, %s, %s, %d, %d", readDescriptors, writeDescriptors, exceptDescriptors, tv_sec, tv_usec));

        updateDescriptors(readDescriptors, writeDescriptors, exceptDescriptors);

        Misc.tv_sec = tv_sec;
        Misc.tv_usec = tv_usec;

        return 0;
    }

    private static final int sampleRate = 22050;
    private static final AudioTrack pinBeepTrack = makePinBeepTrack();

    @Override
    @JavascriptInterface
    public int beep(int freq, int time) {
        return Misc.beeps(freq,time);
    }

    static public int beeps(int freq, int time) {
        Log.d(TAG, "BEEP(" + freq + "," + time + ") ...");
        try {
            makeBeepMiscTrack(freq, time).play();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @NonNull
    static private short[] makeBeepBuffer(final int freq, final int time) {
        int bufferSize = AudioTrack.getMinBufferSize(sampleRate, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);
        int numSamples = ((time / 2) * sampleRate / 1000) * 2;
        if (numSamples > bufferSize) {
            bufferSize = numSamples;
        }
        Log.d(TAG, "    beeps: bufferSize: " + bufferSize);

        //final double samples[] = new double[bufferSize];
        final short[] buffer = new short[bufferSize];
        Log.d(TAG, "    beeps: buffer len: " + buffer.length);

        for (int i = 0; i < numSamples; ++i) {
            buffer[i] = (short) (Math.sin(2 * Math.PI * i * freq / sampleRate) * Short.MAX_VALUE);  // Higher amplitude increases volume
        }
        return buffer;
    }

    @NonNull
    static private MiscAudioTrack makeBeepMiscTrack(final int freq, final int time) {
        final short[] buffer = makeBeepBuffer(freq, time);
        MiscAudioTrack audioTrack = new MiscAudioTrack(AudioManager.STREAM_MUSIC,
                sampleRate, AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT, buffer.length * 2,
                AudioTrack.MODE_STATIC);
        audioTrack.write(buffer, 0, buffer.length);
        return audioTrack;
    }

    @NonNull
    static private AudioTrack makePinBeepTrack() {
        final short[] buffer = makeBeepBuffer(1000, 20);
        AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                sampleRate, AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT, buffer.length * 2,
                AudioTrack.MODE_STATIC);
        audioTrack.write(buffer, 0, buffer.length);
        return audioTrack;
    }

    public static void pinBeep() {
        try {
            switch (pinBeepTrack.getPlayState()) {
                case AudioTrack.PLAYSTATE_PLAYING:
                    pinBeepTrack.stop();
                    /* fallthrough */
                case AudioTrack.PLAYSTATE_PAUSED:
                case AudioTrack.PLAYSTATE_STOPPED:
                    pinBeepTrack.setPlaybackHeadPosition(0);
                    pinBeepTrack.play();
                    break;
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    @Override
    @JavascriptInterface
    public int animate(final String commands) {
        final Stack<String> rpnStack = new Stack<>();
        new Thread(new Runnable() {
            public void run() {
                for(String elem : commands.split(",")) {
                    switch(elem.charAt(0)) {
                        case 'b': // beep
                        {
                            int timeMs = Integer.parseInt(rpnStack.pop());
                            int freq = Integer.parseInt(rpnStack.pop());
                            try {
                                beepRequests.put(new BeepRequest(freq, timeMs));
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            break;
                        }
                        case 'w': // wait
                        {
                            int timeMs = Integer.parseInt(rpnStack.pop());
                            try {
                                Thread.sleep(timeMs);
                            } catch (InterruptedException e) {
                                Log.w(TAG, "Interrupted animate sleep");
                            }
                            break;
                        }
                        case 'l': // led
                        {
                            int ledMask = Integer.parseInt(rpnStack.pop());
                            setLed(ledMask);
                            break;
                        }
                        default:
                            rpnStack.push(elem);
                            break;
                    }
                }
            }
        }).start();
        return 0;
    }

    private void setLed(final int mask) {
        final LedView ledview = (LedView) mainActivity.findViewById(R.id.ledView);
        ledview.post(new Runnable() {
            @Override
            public void run() {
                ledview.setStatus(mask);
            }
        });
    }

    public void onDestroy() {
        should_run = false;
        Log.d(TAG, "Signalled eventLoop");
        newEventNotify();
        sockets.closeAll();
        EventEngine.resetPageReady();
    }
    public void eventLoop() {

        should_run = true;
        boolean descriptorFound;
        Thread selectThread = new Thread(this::rawSelect);
        selectThread.start();

        lock.lock();
        try {
            while (should_run) {
                try {
                    Log.d(TAG, "Waiting for event...");
                    eventReady.await();
                    Log.d(TAG, "Event received");
                    descriptorFound = false;
                    for (IOReader ioReader : ioReaders) {
                        Log.d(TAG, "Checking FD:" + ioReader.getDescriptor());
                        if (ioReader.isEventAvailable()) {
                            String res = String.format("{\\\"%s\\\":0, \\\"%s\\\":[%s]}",
                                    XG.TAG_RC,
                                    XG.TAG_READ_DESCRIPTORS, ioReader.getDescriptor()
                            );
                            Log.d(TAG, "[EVENT_DISPATCHER] SELECT returning result: " + res);
                            postSelectEvent(res);
                            descriptorFound = true;
                        }
                    }
                    if (!descriptorFound) {
                        Log.d(TAG, "No descriptor found, looking for event...");
                        for (IOReader ioReader:ioReadersMap.values()) {
                            if (ioReader.isEventAvailable()) {
                                Log.e(TAG, "Unexpected event found for descriptor " + ioReader.getDescriptor());
                            }
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } finally {
            lock.unlock();
        }
        Log.d(TAG, "eventLoop finishing");
        selector.wakeup();

        try {
            Log.d(TAG, "eventLoop waiting select_thread to end");
            EventEngine.internalNotifyJSReady();
            selectThread.join();
            Log.d(TAG, "eventLoop select_thread ended");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        postSelectCounter = 0;
        jsReadyListeners = new Vector<>();
        Log.d(TAG, "eventLoop select_thread reset");
    }

    private volatile int postSelectCounter = 0;
    private Vector<JSReadyListener> jsReadyListeners = new Vector<>();

    @Override
    public void addJSReadyListener(JSReadyListener jsReadyListener) {
        jsReadyListeners.add(jsReadyListener);
    }

    @Override
    public synchronized boolean postEventToJS(final String event, final boolean sendEventDataDirectly) {
        Log.d(TAG, "++postEventToJS: prepare event " + event);
        //we allow event_loop events to unconditionally post to JS without waiting for other JS calls to complete
        //event_loop(f.ex. sockets) events might however cancel some pending timers, so let timers wait for them to
        // complete,
        //maybe timers will be cancelled during event_loop events callbacks
        if (postSelectCounter > 0 && sendEventDataDirectly) {
            Log.d(TAG, "--postEventToJS: is busy: " + event);
            return false;
        }
        postSelectCounter++;


        //it is safe to call JS right away if we are on main thread (to avoid delays), as webView is created on main
        // thread
        if (Looper.myLooper() == Looper.getMainLooper()) {
            executeJS(event, sendEventDataDirectly);
        } else {
            webView.post(() -> executeJS(event, sendEventDataDirectly));
        }
        return true;
    }

    private void executeJS(final String event, boolean sendEventDataDirectly) {
        String eventStr = event;
        String script = null;
        boolean notifyVSD = false;
        if(eventStr == null) {
            eventStr = this.selectNow();
            notifyVSD = true;
            sendEventDataDirectly = false;
        }
        if (eventStr != null) {
            if (!eventStr.contains("\\")) {
                eventStr = eventStr.replaceAll("\"", "\\\\\"");
            }
            script = restartOnExceptionWrapper("handleSelectEvent(\"" + eventStr + "\", " + (sendEventDataDirectly ?
                    "true" : "false") + ");");
        }
        final boolean doNotifyVSD = notifyVSD;
        final String eventString = eventStr;
        if (script == null) {
            Log.d(TAG, "No javascript evaluated because the provided script was 'null'");
            postSelectCounter--;
            EventEngine.internalNotifyJSReady();
            notifyJsReadyListenersIf(postSelectCounter == 0);
        } else {
            webView.evaluateJavascript(script, s -> {
                postSelectCounter--;
                if (doNotifyVSD) {
                    EventEngine.internalNotifyJSReady();
                }
                Log.d(TAG, "--postEventToJS: event: " + eventString + "; " + postSelectCounter);
                notifyJsReadyListenersIf(postSelectCounter == 0);
            });
        }
    }

    private void notifyJsReadyListenersIf(boolean condition) {
        if (condition) {
            Log.d(TAG, "--postEventToJS post callback notifyJSReady: " + jsReadyListeners.size());
            for (int i = 0; i < jsReadyListeners.size(); i++) {
                JSReadyListener lst = jsReadyListeners.get(i);
                lst.onJSReady();
            }
        }
    }

    private String restartOnExceptionWrapper(String script) {
        return "try {" +
                script +
                "} catch (e) {" +
                "console.log(`JSSYNCH: Handling of ${selectEvent} failed with unexpected exception: ${e}`);" +
                "console.log(\"JSSYNCH: Restarting application...\");" +
                "PAL_MISC.mainAppRestart();" +
                "}";
    }

    public void postSelectEvent(final String event) {
        Log.d(TAG,
                "postSelectEvent: " + (Looper.myLooper() == Looper.getMainLooper()) + "; " + Thread.currentThread().getName() + "; " + event);
        postEventToJS(event, false);
    }

    @Override
    @JavascriptInterface
    public String selectNow() {
        String res = getCurrentActiveDescriptors();
        Log.d(TAG, "selectNow returns: " + res);
        return res;
    }

    public void newEventNotify() {
        lock.lock();
        eventReady.signal();
        lock.unlock();
    }


    @Override
    @JavascriptInterface
    public int sleepMs(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
            return -1;
        } catch (IllegalArgumentException e) {
            return -1;
        }
        return 0;
    }

    @Override
    @JavascriptInterface
    public long getCountMs() {
        return System.currentTimeMillis();
    }



    @Override
    @JavascriptInterface
    public int sleepSec(int sec) {
        try {
            Thread.sleep(sec * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            return -1;
        } catch (IllegalArgumentException e) {
            return -1;
        }
        return 0;
    }

    private boolean postSelectEventIfChannelActive(Set<SelectionKey> selectedKeys) {
        boolean postEvent = false;
        Iterator<SelectionKey> iter = selectedKeys.iterator();
        while (iter.hasNext()) {
            SelectionKey key = iter.next();
            Integer fd = (Integer) key.attachment();
            try {
                if (!key.isValid()) {
                    continue;
                }
                if (key.isReadable() || key.isAcceptable() || key.isWritable()) {
//                    Log.d(TAG, "Posting event because channel:" + fd + " is"
//                        + ( key.isReadable() ? " READABLE" : "")
//                        + ( key.isAcceptable() ? " ACCEPTABLE" : "")
//                        + ( key.isWritable() ? " WRITABLE" : ""));
                    postEvent = true;
                }
            } catch (CancelledKeyException e) {
                e.printStackTrace();
                //TODO: shall we do something?
            }
        }
        if(postEvent) {
            lock.lock();
            lastSelectResult = selectResultToEvent(selectedKeys);
            Log.d(TAG, "JSSYNCH: postSelectEvent: " + lastSelectResult);
            lock.unlock();
            postSelectEvent(null);
        }
        return postEvent;
    }

    @Override
    @JavascriptInterface
    public String getHardwareId() {
        String res =  "0000000000000000" + Settings.Secure.getString(mainActivity.getContentResolver(), Settings.Secure.ANDROID_ID);
        return res.substring(res.length() - 16);
    }

    @Override
    @JavascriptInterface
    public String getSerialNumber() {
        return Build.SERIAL;
    }

    @Override
    @JavascriptInterface
    public String getAppResPath() {
        return Updt.getAppDirectory();
        //return mainActivity.getApplicationContext().getFilesDir() + "/";
    }

    @Override
    @JavascriptInterface
    public int createDirectory(String directory) {
        File dirfile = new File(directory);
        if(dirfile.exists()) {
            if(dirfile.isDirectory()) {
                return 0;
            }
        } else {
            dirfile.mkdir();
            if (dirfile.isDirectory()) {
                return 0;
            }
        }
        return -1;
    }

    private int saveFileWithAppendMode(String filePath, byte content[], boolean append ) {
        try {
            OutputStream out = new FileOutputStream(filePath, append);
            out.write(content, 0, content.length);
            out.close();
            return content.length;
        } catch (IOException e) {
            Log.d(TAG, "saveFile failed: " + e.toString());
            return -1;
        }
    }

    @Override
    @JavascriptInterface
    public int saveFile(String filePath, String buf) {
        return saveFileWithAppendMode(filePath, buf.getBytes(), false);
    }

    public String extractDirectoryPath(String filePath) {
        if (filePath == null || filePath.isEmpty()) {
            Log.e(TAG, "filePath may not be NULL or empty");
            return "";
        }
        File file = new File(filePath);
        if (file == null) {
            Log.e(TAG, "failed to create file object from path = " + filePath);
            return "";
        }
        return file.getParent();
    }

    public String extractFileExtension(String filePath) {
        String fileExt = "";
        if (filePath == null || filePath.isEmpty()) {
            Log.e(TAG, "filePath may not be NULL or empty");
            return "";
        }
        try {
            fileExt = filePath.substring(filePath.lastIndexOf("."));
        } catch (Exception e) {
            Log.e(TAG, "wrong format or missing file extension");
        }
        return fileExt;
    }

    @Override
    @JavascriptInterface
    public int saveBufferToFile(String filePath, String buf){
        String dirPath = this.extractDirectoryPath(filePath);
        if (dirPath == null || dirPath.isEmpty()) {
            Log.e(TAG, "extractDirectoryPath failed");
            return -1;
        }

        String fileExt = "";
        if (dirPath.equals(Updt.getPicturesDirectory())==true) {
            fileExt = this.extractFileExtension(filePath);
            if (fileExt.equals(Updt.getPictureFileExt()) != true) {
                Log.w(TAG, "picture file extention : " + fileExt + " does not match default : " + Updt.getPictureFileExt() + " . File will be saved but will not be used");
            } else {
                Log.d(TAG, "fileExt = " + fileExt);
            }
        } else {
            Log.w(TAG, "not a picture file or picture file with path different from default : " + Updt.getPicturesDirectory() + " . File will not be saved");
            return -1;
        }

        if (this.createDirectory(dirPath) == -1) {
            Log.e(TAG, "Failed to create directory: " + dirPath);
            return -1;
        }
        Log.d(TAG, "directory at " + dirPath + " existing");

        try {
            Log.w(TAG, "calling saveFileToBuffer");
            int res = saveFileWithAppendMode(filePath, buf.getBytes("windows-1252"), false);
            return res;
        } catch (IOException e) {
            Log.d(TAG, "saveFileToBuffer failed: " + e.toString());
            return -1;
        }
    }

    String bytesToHex(byte bytes[]) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();

    }

    @Override
    @JavascriptInterface
    public String getRandom(int len) {
        byte randomBytes[] = new byte[len];
        // is it random enough ?
        new Random().nextBytes(randomBytes);
        return bytesToHex(randomBytes);
    }

    @Override
    @JavascriptInterface
    public String getStoredRandom(int index) {
        return loadFile(Environment.getExternalStorageDirectory() + "/.NVPRAND" + index);
    }

    @Override
    @JavascriptInterface
    public int storeRandom(String data, int index) {
        String filePath = Environment.getExternalStorageDirectory() + "/.NVPRAND" + index;
        deleteFile(filePath);
        if (data == null) {
            return 0;
        }
        int ret =  saveFile(filePath, data);
        if (ret != data.getBytes().length) {
            return -1;
        }
        return 0;
    }

    @Override
    @JavascriptInterface
    public String loadFile(String filePath) {
        try {
            StringBuilder sb = new StringBuilder();
            byte buffer[] = new byte[512 * 1024];
            java.io.FileInputStream sourceFile = null;
            sourceFile = new java.io.FileInputStream(new File(filePath));
            int nbLecture;
            while ((nbLecture = sourceFile.read(buffer)) != -1) {
                sb.append(new String(buffer,0,nbLecture));
            }
            sourceFile.close();
            return sb.toString();
        } catch (IOException e) {
            Log.w(TAG, "Failed to read file: " + filePath + ": " + e.toString());
            return null;
        }
    }

    @Override
    @JavascriptInterface
    public int deleteFile(String filename) {
        File file = new File(filename);
        if (!file.exists()) {
            return -1;
        }
        file.delete();
        if (file.exists()) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    @JavascriptInterface
    public long fileSize(String filepath) {
        File file = new File(filepath);
        if(!file.exists() || !file.isFile()) {
            return -1;
        }
        long res = file.length();
        return res;
    }

    @Override
    @JavascriptInterface
    public String fileCalculateSha1(String filePath) {
        byte[] buffer = new byte[8192];
        int count;
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "fileCalculateSha1 - no SHA-1 algorithm: " + e.toString());
            return null;
        }
        BufferedInputStream bis = null;
        try {
            long totalSize = 0;
            bis = new BufferedInputStream(new FileInputStream(filePath));
            while ((count = bis.read(buffer)) > 0) {
                digest.update(buffer, 0, count);
                totalSize += count;
            }
            bis.close();
            byte[] hash = digest.digest();
            Log.d(TAG, "fileCalculateSha1("+filePath+") -> totalSize: " + totalSize + ", hash: " + bytesToHex(hash));
            return bytesToHex(hash);
        } catch (IOException e) {
            Log.e(TAG, "fileCalculateSha1 - invalid file: " + e.toString());
            return null;
        }
    }

    @Override
    @JavascriptInterface
    public int renameFile(String filename, String newFileName) {
        File srcFile = new File(filename);
        File dstFile = new File(newFileName);
        if (srcFile.renameTo(dstFile)) {
            return 0;
        } else {
            return -1;
        }
    }

    @Override
    @JavascriptInterface
    public String getUpdateFolderPath() {
        try {
            File dirFile = new File(Updt.getAppDirectory() + "/TmpUpdt");
            if (dirFile.isDirectory()) {
                Log.d(TAG, "Deleting old update dir");
                Updt.deleteRecursive(dirFile);
            }
        } catch (RuntimeException e) {
            Log.e(TAG, "Can't delete files", e);
        }
        return Environment.getExternalStorageDirectory() + "/" + mainActivity.getPackageName() + "/";
    }

    @Override
    @JavascriptInterface
    public boolean fileExists(String filePath) {
        boolean res = new File(filePath).exists();
        Log.d(TAG, "fileExists(" + filePath + ") res: " + res);
        return res;
    }

    private static byte[] fromHexString(final String encoded) {
        if ((encoded.length() % 2) != 0)
            throw new IllegalArgumentException("Input string must contain an even number of characters");

        final byte result[] = new byte[encoded.length()/2];
        final char enc[] = encoded.toCharArray();
        for (int i = 0; i < enc.length; i += 2) {
            StringBuilder curr = new StringBuilder(2);
            curr.append(enc[i]).append(enc[i + 1]);
            result[i/2] = (byte) Integer.parseInt(curr.toString(), 16);
        }
        return result;
    }

    @Override
    @JavascriptInterface
    public int appendFile(String filePath, String buf, String encoding) {
        int res = -1;
        byte[] decbuf ;

        if (encoding != null && encoding.equals("base64")) {
            decbuf = Base64.decode(buf, Base64.DEFAULT);
        } else {
            decbuf = fromHexString(buf);
        }
        if (decbuf != null) {
            res = saveFileWithAppendMode(filePath, decbuf, true);
        }
        return res;
    }


    @Override
    @JavascriptInterface
    public int setTimer(int timerId, long timeout) {
        return Timers.setTimer(timerId, timeout);
    }

    @Override
    @JavascriptInterface
    public int cancelTimer(int timerId) {
        return Timers.cancelTimer(timerId);
    }

    @Override
    @JavascriptInterface
    public boolean hasDisplay() {
        return true;
    }

    @Override
    @JavascriptInterface
    public boolean hasKeyboard() {
        return false;
    }

    @Override
    @JavascriptInterface
    public boolean hasTouchscreen() {
        return true;
    }

    @Override
    @JavascriptInterface
    public String hwGetDeviceModel() {
        return "ANDROID_SIMULATOR";
    }

    @Override
    @JavascriptInterface
    public String hwGetHardwarePlatform() { return "EMULATOR"; }

    @Override
    @JavascriptInterface
    public int getMaxCardholderDisplayLines() { return 6; }

    @Override
    @JavascriptInterface
    public int getMaxAttendantDisplayLines() { return 6; }

    @Override
    @JavascriptInterface
    public String getMask() {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InterfaceAddress> addrs =  intf.getInterfaceAddresses();
                for (InterfaceAddress addr : addrs) {
                    if (!addr.getAddress().isLoopbackAddress()) {
                        String sAddr = addr.getAddress().getHostAddress();
                        boolean isIPv4 = sAddr.indexOf(':')<0;
                        if (isIPv4) {
                            int mask =  (0xFFFFFFFF << (32 - addr.getNetworkPrefixLength())) & 0xFFFFFFFF;
                            return String.format(Locale.getDefault(), "%d.%d.%d.%d", (mask >> 24) & 0xFF, ((mask >> 16) & 0xFF), ((mask >> 8) & 0xFF), mask & 0xFF);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    @JavascriptInterface
    public String getGateway() {
        ConnectivityManager connectivityManager = (ConnectivityManager) this.mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        Network[] networks;
        try {
            networks = connectivityManager == null ? null : new Network[]{connectivityManager.getActiveNetwork()};
        } catch( NoSuchMethodError e) {
            networks = null;
        }
        if (networks == null) {
            return "";
        }
        for(int i = 0; i < networks.length; ++i) {
            LinkProperties linkProperties = connectivityManager.getLinkProperties(networks[i]);
            if (linkProperties != null) {
                Log.d(TAG, "Number of routes: " + linkProperties.getRoutes().size());
                for(RouteInfo route: linkProperties.getRoutes()) {
                    Log.d(TAG, "route: " + route.getGateway().getHostAddress());
                    if (route.isDefaultRoute() && route.getGateway().getHostAddress().indexOf(':') < 0) {
                        return route.getGateway().getHostAddress();
                    }
                }
            }
        }
        return "";
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    @JavascriptInterface
    public String getDNS() {
        ConnectivityManager connectivityManager = (ConnectivityManager) this.mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        Network[] networks;
        try {
            networks = connectivityManager == null ? null : new Network[]{connectivityManager.getActiveNetwork()};
        } catch( NoSuchMethodError e) {
            networks = null;
        }
        if (networks == null) {
            return "";
        }
        String result = "";
        for(int i = 0; i < networks.length; ++i) {
            Log.d(TAG, "Checking network " + i + " of " + networks.length);
            LinkProperties linkProperties = connectivityManager.getLinkProperties(networks[i]);
            if (linkProperties != null) {
                for(RouteInfo route: linkProperties.getRoutes()) {
                    try {
                        if (route.isDefaultRoute() && route.getGateway().getHostAddress().indexOf(':') < 0) {
                            List<InetAddress> dnsServers = linkProperties.getDnsServers();
                            if (dnsServers == null) {
                                continue;
                            }
                            for (InetAddress addr: dnsServers) {
                                if (addr.getHostAddress().indexOf(':') < 0) {
                                    result = addr.getHostAddress();
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        continue;
                    }
                }

            }
        }
        return result;
    }

    @Override
    @JavascriptInterface
    public String getVendorName() { return "ANDROID"; }

    @Override
    @JavascriptInterface
    public String getIPAddress() {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InterfaceAddress> addrs =  intf.getInterfaceAddresses();
                for (InterfaceAddress addr : addrs) {
                    if (!addr.getAddress().isLoopbackAddress()) {
                        String sAddr = addr.getAddress().getHostAddress();
                        boolean isIPv4 = sAddr.indexOf(':')<0;
                        if (isIPv4) {
                            return sAddr;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    @JavascriptInterface
    public String getStoredRandom(int index, int len) {
        return "0000000000000000";
    }

    @Override
    @JavascriptInterface
    public boolean printerCheck() {
        return true;
    }

    @Override
    @JavascriptInterface
    public int setTime(int currentTime, String timeZone, int timeZoneOffset) {
        return 0;
    }

    @Override
    @JavascriptInterface
    public String getFilesListFromDirectory(String path) {
        StringBuffer sb = new StringBuffer();

        sb.append('[');
        File directory = new File(path);
        File[] files = directory.listFiles();
        for (int i = 0; i < files.length; i++) {
            if (i > 0) {
                sb.append(',');
            }
            sb.append('"').append(path).append(files[i].getName()).append('"');
        }
        sb.append(']');
        return sb.toString();
    }

    @Override
    @JavascriptInterface
    public int toggleSystemBars() {
        ShowAndActivateSystemBars ^= true;
        activateAllSystemButtons(ShowAndActivateSystemBars);
        Log.d("MENU","ShowAndActivateSystemBars = " + ShowAndActivateSystemBars);
        return 0;
    }

    @Override
    public void activateSystemBars(boolean activate) {
        AppCompatActivity currentActivity = CurrentActivity.thisActivity;
        if (currentActivity != null) {
            int flags;
            if (activate) {
                flags = View.SYSTEM_UI_FLAG_VISIBLE;
            } else {
                flags = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_FULLSCREEN;

            }

            currentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    currentActivity.getWindow().getDecorView().setSystemUiVisibility(flags);

                }
            });
        }
    }

    public void restoreSystemBarsStatus() {
        if (!ShowAndActivateSystemBars) {
            activateSystemBars(false);
        }
    }

    @Override
    @JavascriptInterface
    public int selectApplicationView() {
        mainActivity.startActivity(new Intent(mainActivity, ApplicationsActivity.class));
        return 0;
    }

    @Override
    @JavascriptInterface
    public String getSimCardNumber() {
        TelephonyManager manager = (TelephonyManager) this.mainActivity.getSystemService(Context.TELEPHONY_SERVICE);
        if (manager == null || ActivityCompat.checkSelfPermission(this.mainActivity, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "TelephonyManager failed or READ_PHONE_STATE permission not granted for IMSI reading");
            return "";
        }
        return (manager.getSubscriberId()!=null) ? manager.getSubscriberId().toString() : "";
    }

    @Override
    @JavascriptInterface
    public String getSimOperatorName() {
        TelephonyManager manager = (TelephonyManager) this.mainActivity.getSystemService(Context.TELEPHONY_SERVICE);
        if (manager == null || ActivityCompat.checkSelfPermission(this.mainActivity, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "TelephonyManager failed or READ_PHONE_STATE permission not granted for IMSI reading");
            return "";
        }
        return (manager.getSimOperatorName()!=null) ? manager.getSimOperatorName().toString() : "";
    }

    public static String getSystemPlaftorm() {
        String manufacturer = Build.MANUFACTURER.toLowerCase();
        if (manufacturer.contains("pax") || Build.BRAND.toLowerCase().contains("pax")) {
            return "pax";
        } else if (manufacturer.contains("castles")) {
            return "castles";
        } else {
            return "emulator";
        }
    }

    @Override
    public int setContactReaderLights(final boolean status) {
        return 0;
    }

}
