package com.wallee.vsd.common.vsd;

import android.util.Log;
import android.webkit.JavascriptInterface;

import com.wallee.vsd.common.models.Poller;
import com.wallee.vsd.common.models.vsd.MagIface;
import com.wallee.vsd.common.models.vsd.MiscIface;

public class Mag extends Poller implements MagIface {
    private static final String TAG = "common/Mag";
    private long lastOpenTime = 0;
    private int swipeResult = 0;

    public Mag(MiscIface misc) {
        super(misc);
    }

    private synchronized int getSwipeResult() {
        return this.swipeResult;
    }

    private synchronized void setSwipeResult(int swipeResult) {
        this.swipeResult = swipeResult;
    }

    protected boolean eventDetected() {
        int res = swipeInternal();
        setSwipeResult(res);
        return (res == 0);

    }

    protected String getResultTag() {
        return XG.TAG_SWIPE_RES;
    }

    private int swipeInternal() {
        if (lastOpenTime > 0 && (System.currentTimeMillis() / 1000 - lastOpenTime) > 3) {
            Log.d(TAG, "swiped");

            return 0;
        }
        Log.d(TAG, "not swiped " + lastOpenTime);
        return -1;
    }

    @Override
    @JavascriptInterface
    public synchronized int open() {
        lastOpenTime = System.currentTimeMillis() / 1000;
        startPollingThread();
        return 0;
    }

    @Override
    @JavascriptInterface
    public synchronized int swiped() {
        return getSwipeResult();
    }

    @Override
    @JavascriptInterface
    public synchronized void close() {
        stopPollingThread();
    }

    @Override
    @JavascriptInterface
    public synchronized void reset() {
    }

    @Override
    @JavascriptInterface
    public synchronized String read() {
        Log.d(TAG, "reading");
        if (lastOpenTime != 0 && (System.currentTimeMillis() / 1000 - lastOpenTime) > 3) {
            Log.d(TAG, "reading OK");
            lastOpenTime = 0;
            return "{\"_RC\": 0, \"track1\": \"\", \"track3\": \"\", \"track2\": \"5413330056003511=2512101000000000\"}";
        }
        Log.d(TAG, "reading NOK");
        return null;
    }

}
