package com.wallee.vsd.common.applications.data;

import android.content.ComponentName;
import android.graphics.drawable.Drawable;

public class LauncherEntry {
    private final String applicationName;
    private final Drawable applicationIcon;
    private final ComponentName componentName;

    public LauncherEntry(String applicationName, Drawable applicationIcon, ComponentName componentName) {
        this.applicationName = applicationName;
        this.applicationIcon = applicationIcon;
        this.componentName = componentName;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public Drawable getApplicationIcon() {
        return applicationIcon;
    }

    public ComponentName getComponentName() {
        return componentName;
    }
}
