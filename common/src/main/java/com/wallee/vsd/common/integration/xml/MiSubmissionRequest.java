package com.wallee.vsd.common.integration.xml;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Namespace(prefix = "vcs-pos", reference = "http://www.vibbek.com/pos")
@Root(name = "miSubmissionRequest", strict = false)
public class MiSubmissionRequest {
    @Element(required = false)
    public String posId;

    public MiSubmissionRequest(String posId) {
        this.posId = posId;
    }
}
