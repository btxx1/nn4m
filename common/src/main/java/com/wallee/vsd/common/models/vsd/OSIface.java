package com.wallee.vsd.common.models.vsd;

public interface OSIface {

    int adjustOSparam(String params, int timeout);
    int getParamValue(int destination);
    int setParamValue(int destination, int value);
    boolean ethIsSupported();
    boolean wifiIsSupported();
    boolean gprsIsSupported();
    boolean isSimCardPresent();
    String getNetworkInterface();
    int setNetworkInterface(String networkInteface, boolean enabled);
    boolean isMobileDataEnabled();
    void configureWiFi();
    void configureGSM();
}
