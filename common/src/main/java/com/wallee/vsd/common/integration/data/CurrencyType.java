package com.wallee.vsd.common.integration.data;

public enum CurrencyType {
    ADP("020", 2),
    AED("784", 2),
    AFA("004", 2),
    AFN("971", 2),
    ALL("008", 2),
    AMD("051", 2),
    ANG("532", 2),
    AOA("973", 2),
    AON("024", 2),
    ARS("032", 2),
    ATS("040", 2),
    AUD("036", 2),
    AWG("533", 2),
    AZM("031", 2),
    BAM("977", 2),
    BBD("052", 2),
    BDT("050", 2),
    BEF("056", 2),
    BGL("100", 2),
    BGN("975", 2),
    BHD("048", 3),
    BIF("108", 0),
    BMD("060", 2),
    BND("096", 2),
    BOB("068", 2),
    BOV("984", 2),
    BRL("986", 2),
    BSD("044", 2),
    BTN("064", 2),
    BWP("072", 2),
    BYR("974", 2),
    BZD("084", 2),
    CAD("124", 2),
    CDF("976", 2),
    CHE("947", 2),
    CHF("756", 2),
    CHW("948", 2),
    CLF("990", 4),
    CLP("152", 0),
    CNY("156", 2),
    COP("170", 2),
    CRC("188", 2),
    CSD("891", 2),
    CSK("200", 2),
    CUP("192", 2),
    CVE("132", 2),
    CYP("196", 2),
    CZK("203", 2),
    DEM("276", 2),
    DJF("262", 0),
    DKK("208", 2),
    DOP("214", 2),
    DZD("012", 2),
    ECS("218", 2),
    EEK("233", 2),
    EGP("818", 2),
    ERN("232", 2),
    ESP("724", 2),
    ETB("230", 2),
    EUR("978", 2),
    FIM("246", 2),
    FJD("242", 2),
    FKP("238", 2),
    FRF("250", 2),
    GBP("826", 2),
    GEL("981", 2),
    GHC("288", 2),
    GHS("936", 2),
    GIP("292", 2),
    GMD("270", 2),
    GNF("324", 0),
    GRD("300", 2),
    GTQ("320", 2),
    GYD("328", 2),
    HKD("344", 2),
    HNL("340", 2),
    HRK("191", 2),
    HTG("332", 2),
    HUF("348", 2),
    IDR("360", 2),
    IEP("372", 2),
    ILS("376", 2),
    INR("356", 2),
    IQD("368", 3),
    IRR("364", 2),
    ISK("352", 0),
    ITL("380", 2),
    JMD("388", 2),
    JOD("400", 3),
    JPY("392", 0),
    KES("404", 2),
    KGS("417", 2),
    KHR("116", 2),
    KMF("174", 0),
    KPW("408", 2),
    KRW("410", 0),
    KWD("414", 3),
    KYD("136", 2),
    KZT("398", 2),
    LAK("418", 2),
    LBP("422", 2),
    LKR("144", 2),
    LRD("430", 2),
    LSL("426", 2),
    LTL("440", 2),
    LUF("442", 2),
    LVL("428", 2),
    LYD("434", 3),
    MAD("504", 2),
    MDL("498", 2),
    MGA("969", 2),
    MGF("450", 2),
    MKD("807", 2),
    MMK("104", 2),
    MNT("496", 2),
    MOP("446", 2),
    MRO("478", 2),
    MTL("470", 2),
    MUR("480", 2),
    MVR("462", 2),
    MWK("454", 2),
    MXN("484", 2),
    MXV("979", 2),
    MYR("458", 2),
    MZM("508", 2),
    MZN("943", 2),
    NAD("516", 2),
    NGN("566", 2),
    NIO("558", 2),
    NLG("528", 2),
    NOK("578", 2),
    NPR("524", 2),
    NZD("554", 2),
    OMR("512", 3),
    PAB("590", 2),
    PEN("604", 2),
    PGK("598", 2),
    PHP("608", 2),
    PKR("586", 2),
    PLN("985", 2),
    PTE("620", 2),
    PYG("600", 0),
    QAR("634", 2),
    ROL("642", 2),
    RON("946", 2),
    RSD("941", 2),
    RUB("643", 2),
    RUR("810", 2),
    RWF("646", 0),
    SAR("682", 2),
    SBD("090", 2),
    SCR("690", 2),
    SDD("736", 2),
    SDG("938", 2),
    SEK("752", 2),
    SGD("702", 2),
    SHP("654", 2),
    SIT("705", 2),
    SKK("703", 2),
    SLL("694", 2),
    SOS("706", 2),
    SRG("740", 2),
    STD("678", 2),
    SVC("222", 2),
    SYP("760", 2),
    SZL("748", 2),
    THB("764", 2),
    TJS("972", 2),
    TMM("795", 2),
    TND("788", 3),
    TOP("776", 2),
    TPE("626", 2),
    TRL("792", 2),
    TRY("949", 2),
    TTD("780", 2),
    TWD("901", 2),
    TZS("834", 2),
    UAH("980", 2),
    UGX("800", 0),
    USD("840", 2),
    USN("997", 2),
    USS("998", 2),
    UYU("858", 2),
    UZS("860", 2),
    VEB("862", 2),
    VEF("937", 2),
    VND("704", 0),
    VUV("548", 0),
    WST("882", 2),
    XAF("950", 0),
    XCD("951", 2),
    XDR("960", 0),
    XOF("952", 0),
    XPF("953", 0),
    YER("886", 2),
    YUM("891", 2),
    ZAR("710", 2),
    ZMK("894", 2),
    ZWD("716", 2);

    public final String code;
    public final int exponent;

    CurrencyType(String code, int exponent) {
        this.code = code;
        this.exponent = exponent;
    }
}
