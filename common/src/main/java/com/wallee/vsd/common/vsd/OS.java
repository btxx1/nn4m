package com.wallee.vsd.common.vsd;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.JavascriptInterface;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.wallee.vsd.common.models.vsd.MiscIface;
import com.wallee.vsd.common.models.vsd.OSIface;
import com.wallee.vsd.common.os.Brightness;
import com.wallee.vsd.common.os.Volume;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;

public class OS implements OSIface {
    public static final String TAG = "OS";
    public Context context;
    private MiscIface misc;
    private static OS instance;
    public final int BRIGHTNESS = 0;
    public final int VOLUME = 1;
    private final String TAG_ACTION_DESTINATION = "actionDest";



    private boolean ensureSystemPermissions() {
        if (OS.checkSystemWritePermission(Misc.getCurrentActivity())) {
            Log.d(TAG, "write permission granted");
            return true;
        } else {
            Log.d(TAG, "write permission denied");
            this.misc.activateBackButton(true);
            openAndroidPermissionsMenu();
            return false;
        }
    }

    private void openAndroidPermissionsMenu() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
            intent.setData(Uri.parse("package:" + Misc.getCurrentActivity().getPackageName()));
            Misc.mainActivity.startActivityForResult(intent, 1);
        }
    }

    static public class NetworkInterface {
        public static final String WIFI = "WIFI";
        public static final String GPRS = "GPRS";
    }

    public OS(Context appContext, MiscIface misc) {
        OS.instance = this;
        this.misc = misc;
        context = appContext; //appContext;
    }

    @Override
    @JavascriptInterface
    public int adjustOSparam(String params, int timeout) {
        if (!checkSystemWritePermission(context)) {
            Log.e(TAG, "request may not be completed due to lack of user consent for access to system settings");
            return -1;
        }
        AppCompatActivity mainActivity = Misc.getCurrentActivity();
        Intent defs = null;
        int actionDest = getActionDestination(params);
        if (actionDest == BRIGHTNESS)
            defs = new Intent(mainActivity, Brightness.class);
        else if (actionDest == VOLUME)
            defs = new Intent(mainActivity, Volume.class);
        else {
            Log.e(TAG, "request may not be completed due to action destination error");
            return -1;
        }
        defs.putExtra("params", params);
        defs.putExtra("timeout", timeout);
        mainActivity.startActivity(defs);
        return 0;
    }

    public int getActionDestination(String params) {
        try {
            JSONObject paramsJSON = new JSONObject(params);
            if (paramsJSON.has(TAG_ACTION_DESTINATION)) {
                int actionDest = paramsJSON.getInt(TAG_ACTION_DESTINATION);
                switch (actionDest) {
                    case BRIGHTNESS:
                        return BRIGHTNESS;
                    case VOLUME:
                        return VOLUME;
                    default:
                        Log.e(TAG, "unsupported action destination: " + actionDest + " for OS param");
                        return -1;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static OS getInstance() {
        return OS.instance;
    }

    public static boolean checkSystemWritePermission(Context appContext) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.System.canWrite(appContext)) {
                return true;
            }
        }
		Log.d(TAG, "write permissions not set");
        return false;
    }

    @Override
    @JavascriptInterface
    public int getParamValue(int destination) {
        if (!checkSystemWritePermission(context)) {
            Log.e(TAG, "param value may not be obtained due to lack of user consent for access to system settings");
            return -1;
        }
        try {
            switch (destination) {
                case (BRIGHTNESS):
                    return Settings.System.getInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, 0);
                case (VOLUME):
                    AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                    return audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                default:
                    Log.e(TAG, "unsupported control destination : " + destination);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    @JavascriptInterface
    public int setParamValue(int destination, int value){
        if (!checkSystemWritePermission(context)) {
            Log.e(TAG, "param value may not be modified due to lack of user consent for access to system settings");
            if (OS.instance != null) {
                OS.instance.ensureSystemPermissions();
            }
            return -1;
        }
        try {
            switch (destination) {
                case (BRIGHTNESS):
                    Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, value);
                    return 0;
                case (VOLUME):
                    AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, value, 0);
                    Misc.beeps(1500, 100);
                    return 0;
                default:
                    Log.e(TAG, "unsupported control destination : " + destination);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    @JavascriptInterface
    public boolean ethIsSupported() { return false; }

    @Override
    @JavascriptInterface
    public boolean wifiIsSupported() { return true; }

    @Override
    @JavascriptInterface
    public boolean gprsIsSupported() { return true; }

    @Override
    @JavascriptInterface
    public boolean isSimCardPresent() {
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (manager == null || ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "TelephonyManager failed or READ_PHONE_STATE permission not granted for IMSI reading");
            return false;
        }
        if (manager.getSimState() != TelephonyManager.SIM_STATE_ABSENT) {
            Log.i(TAG, "network type = " + networkTypeToString(manager.getNetworkType()));
            return true;
        }
        return false;
    }

    @Override
    @JavascriptInterface
    public boolean isMobileDataEnabled() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            Class cmClass = Class.forName(cm.getClass().getName());
            Method method = cmClass.getDeclaredMethod("getMobileDataEnabled");
            method.setAccessible(true); // Make the method callable
            return (Boolean)method.invoke(cm);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    @JavascriptInterface
    public String getNetworkInterface() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null || !info.isConnectedOrConnecting())
            return ""; // not connected

        return OS.networkInterfaceToString(info);
    }

    public static String networkInterfaceToString(NetworkInfo info) {
        if (info == null)
            return ""; // not connected
        if (info.getType() == ConnectivityManager.TYPE_WIFI)
            return "WIFI";
        if (info.getType() == ConnectivityManager.TYPE_MOBILE)
            return "GPRS";
        if (info.getType() == ConnectivityManager.TYPE_ETHERNET)
            return "ETHERNET";
        if (info.getType() == ConnectivityManager.TYPE_BLUETOOTH)
            return "BLUETOOTH";
        return "";//unknown interface
    }
    @Override
    @JavascriptInterface
    public int setNetworkInterface(String networkInteface, boolean enabled) {
        try {
            switch (networkInteface) {
                case (NetworkInterface.WIFI):
                    WifiManager wifi = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                    wifi.setWifiEnabled(enabled);
                    return 0;
                case (NetworkInterface.GPRS):
                    return setGPRSEnabled(enabled);
                default:
                    Log.e(TAG, "unsupported network interface: " + networkInteface);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }
    //TODO this basically does not work due to no access to MODIFY_PHONE_STATE on non-system app so needs to be replaced with pax function
    int setGPRSEnabled(boolean enable) throws Exception {
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (manager == null || ActivityCompat.checkSelfPermission(context, Manifest.permission.MODIFY_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "TelephonyManager failed or MODIFY_PHONE_STATE permission not granted for updating GPRS");
            return -1;
        }
        Method methodSet = Class.forName(manager.getClass().getName()).getDeclaredMethod( "setDataEnabled", Boolean.TYPE);
        methodSet.invoke(manager,enable);
        return 0;
    }

    public String networkTypeToString(int networkType) {
        switch (networkType) {
            case 0:
                return "NETWORK_TYPE_UNKNOWN";
            case 1:
                return "NETWORK_TYPE_GPRS";
            case 2:
                return "NETWORK_TYPE_EDGE";
            case 3:
                return "NETWORK_TYPE_UMTS";
            case 4:
               return "NETWORK_TYPE_CDMA";
            case 5:
                return "NETWORK_TYPE_EVDO_0";
            case 6:
                return "NETWORK_TYPE_EVDO_A";
            case 7:
                return "NETWORK_TYPE_1XRTT";
            case 8:
                return "NETWORK_TYPE_HSDPA";
            case 9:
                return "NETWORK_TYPE_HSUPA";
            case 10:
                return "NETWORK_TYPE_HSPA";
            case 11:
                return "NETWORK_TYPE_IDEN";
            case 12:
                return "NETWORK_TYPE_EVDO_B";
            case 13:
                return "NETWORK_TYPE_LTE";
            case 14:
                return "NETWORK_TYPE_EHRPD";
            case 15:
                return "NETWORK_TYPE_HSPAP";
            case 16:
                return "NETWORK_TYPE_GSM";
            case 17:
                return "NETWORK_TYPE_TD_SCDMA";
            case 18:
                return "NETWORK_TYPE_IWLAN";
            case 19:
                return "NETWORK_TYPE_LTE_CA";
            default:
                return "NETWORK_TYPE_UNSUPPORTED";
        }

    }

    static int getOSActivityStartFlags() {
        return Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS
                | Intent.FLAG_ACTIVITY_NO_HISTORY;
    }

    @Override
    @JavascriptInterface
    public void configureWiFi() {
        misc.activateSystemBars(true);
        Intent intent = new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK);  //Settings.ACTION_WIFI_SETTINGS
        intent.addFlags(OS.getOSActivityStartFlags());
        context.startActivity(intent);
    }

    @Override
    @JavascriptInterface
    public void configureGSM() {
        Log.d(TAG, "Configuring GSM...");
        misc.activateSystemBars(true);
        Intent intent = new Intent(Settings.ACTION_NETWORK_OPERATOR_SETTINGS);
        intent.addFlags(OS.getOSActivityStartFlags());
        context.startActivity(intent);
    }
}
