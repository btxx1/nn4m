package com.wallee.vsd.common.models.vsd;

public interface UpdtIface {

    int updateFirmware(String fileName);

    int updateFileToApp(String fileName);

    int uninstallApp(String packageName);

    int updateInstallAup(String fileName);

    int updateFileToParam(String fileName, String appName);

    int updateFileToFont(String fileName);

    int checkFileNameLen(String name);

    String getCodeTypeFromName(String fileName);

    int instalDataToAppResFolder(String sourcename, String destination);

    int instalDataToAppExeFolder(String sourcename, String destination);

    int updateInstallSo(String soFile);

    int installFromUrl(String url);

    void cleanup();
}
