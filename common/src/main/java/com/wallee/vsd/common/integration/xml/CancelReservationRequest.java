package com.wallee.vsd.common.integration.xml;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Namespace(prefix = "vcs-pos", reference = "http://www.vibbek.com/pos")
@Root(name = "cancelReservationRequest", strict = false)
public class CancelReservationRequest {
    @Element(required = false)
    public String posId;
    @Element(required = false)
    public String acquirerId;
    @Element(required = false)
    public String transactionRefNumber;

    public CancelReservationRequest(String posId, String acquirerId, String transactionRefNumber) {
        this.posId = posId;
        this.acquirerId = acquirerId;
        this.transactionRefNumber = transactionRefNumber;
    }
}
