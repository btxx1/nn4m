package com.wallee.vsd.common.integration;

import com.wallee.android.till.sdk.data.Receipt;
import com.wallee.android.till.sdk.data.ResultCode;
import com.wallee.android.till.sdk.data.State;
import com.wallee.android.till.sdk.data.SubmissionResult;
import com.wallee.vsd.common.integration.data.DataUtils;
import com.wallee.vsd.common.integration.xml.MiSubmissionRequest;
import com.wallee.vsd.common.integration.xml.XmlUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class ExecuteSubmissionHandler {
    private final NetClient client = new NetClient();

    SubmissionResult handle() {
        if (client.connectWithServer()) {
            try {
                MiSubmissionRequest xmlRequest = createXmlRequest();
                List<Receipt> receipts = new ArrayList<>();
                SubmissionResult result = null;

                client.send(XmlUtils.toXml(xmlRequest));
                while (result == null) {
                    String xmlResponse = client.receive();
                    if (xmlResponse.isEmpty()) {
                        result = createErrorResult("Internal error - empty response");
                    } else if (XmlUtils.shouldConfirm(xmlResponse)) {
                        client.send(XmlUtils.CONFIRMATION);
                    } else if (XmlUtils.isPrinterNotification(xmlResponse)) {
                        Receipt receipt = DataUtils.printerNotificationToReceipt(XmlUtils.toPrinterNotification(xmlResponse));
                        if (receipt != null) {
                            receipts.add(receipt);
                        }
                    } else if (XmlUtils.isErrorNotification(xmlResponse)) {
                        result = createErrorResult(XmlUtils.toErrorNotification(xmlResponse).errorText);
                    } else if (XmlUtils.isSubmissionResponse(xmlResponse)) {
                        result = createResult(receipts);
                    }
                }

                return result;
            } catch (Exception e) {
                e.printStackTrace();
                return createErrorResult(e.getMessage());
            } finally {
                client.disConnectWithServer();
            }
        } else {
            return createErrorResult("Failed to connect to VPJ");
        }
    }

    private MiSubmissionRequest createXmlRequest() {
        return new MiSubmissionRequest(
                DataUtils.POS_ID
        );
    }

    private SubmissionResult createResult(List<Receipt> receipts) {
        return new SubmissionResult(
                State.SUCCESSFUL,
                new ResultCode(
                        "0",
                        "Success"
                ),
                receipts
        );
    }

    private SubmissionResult createErrorResult(String error) {
        return new SubmissionResult(
                State.FAILED,
                new ResultCode(
                        "error",
                        error
                ),
                Collections.emptyList()
        );
    }
}

