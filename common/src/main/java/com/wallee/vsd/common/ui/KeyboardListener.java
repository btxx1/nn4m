package com.wallee.vsd.common.ui;

public interface KeyboardListener {
    void onCancel();
    void onClear();
    void onCommit();
    void onEnter(String chars);
}
