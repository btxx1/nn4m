package com.wallee.vsd.common;

import android.os.Build;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import novelpay.pl.npf.ctls.models.CaPublicKey;
import novelpay.pl.npf.ctls.models.CtlsAppData;
import novelpay.pl.npf.ctls.models.CtlsCombinationList;
import novelpay.pl.npf.ctls.models.CtlsInitConfig;
import novelpay.pl.npf.ctls.models.CtlsTerminalCapabilities;
import novelpay.pl.npf.ctls.models.LimitSet;
import novelpay.pl.npf.utils.Utils;

public class Config {
    private static Config instance;
    private CtlsInitConfig ctlsInitConfig;

    static public class JSAppData extends CtlsAppData implements Serializable {
        private boolean _hasSecurityCapabilities;
        private byte securityCapabilities;
        JSAppData() {
            _hasSecurityCapabilities = false;
        }
        public void setSecurityCapabilities(byte securityCapabilities) {
            this.securityCapabilities = securityCapabilities;
            this._hasSecurityCapabilities = true;
        }
        public byte getSecurityCapabilities() {
            return this.securityCapabilities;
        }
        public boolean hasSecurityCapabilities() {
            return _hasSecurityCapabilities;
        }
    }

    private Iterable<? extends CtlsTerminalCapabilities> ctlsTerminalCapabilitiesList;
    private Iterable<? extends JSAppData> ctlsAppList;
    private ArrayList<CtlsCombinationList> combinationLists = new ArrayList<>();
    private Map<String, String> currencies = new TreeMap<>();

    private static final String TAG = "Config";
    private byte[] terminalCapabilities;
    private byte[] additionalTerminalCapabilities;
    private int emvTrxType;
    private boolean emvTrxTypeSet;

    JSONObject apps = null;
    private byte securityCapabilities;

    public synchronized static Config getInstance() {
        if (instance == null) {
            instance = new Config();
        }
        return instance;
    }

    private byte[] getBytes(JSONObject json, String name) throws JSONException {
        return Base64.decode(json.getString(name), Base64.DEFAULT);
    }

    private byte[] getBcd(JSONObject json, String name) throws JSONException {
        return Utils.str2Bcd(json.getString(name));
    }

    private void setKernel3Parameters(CtlsAppData appData, JSONObject app) {
        appData.setAppName("Visa");
    }

    private void setKernel4Parameters(JSAppData appData, JSONObject app) {
        String element = null;
        appData.setAppName("Amex");
        try {
            element = "tacDefault";
            appData.setTacDefault(getBytes(app, element));
            element = "tacDenial";
            appData.setTacDenial(getBytes(app, element));
            element = "tacOnline";
            appData.setTacOnline(getBytes(app, element));
        } catch (Exception e) {
            Log.e(TAG, "Error processing element " + element + ": " + app);
            e.printStackTrace();
        }
    }

    private void setKernel7Parameters(JSAppData appData, JSONObject app) {
        appData.setAppName("Unionpay");
    }

    private void setKernel2Parameters(JSAppData appData, JSONObject app) {
        String element = null;
        byte[] aid = appData.getAid();

        boolean isMaestro = (aid.length > 5 && aid[5] == 0x30);

        appData.setAppName((isMaestro)? "Maestro" : "MasterCard");

        try {
            element = "ctlessConfigOptions";
            JSONObject ctlsOptions = app.getJSONObject("ctlessConfigOptions");

            element = "tacDefault";
            appData.setTacDefault(getBytes(app, element));
            element = "tacDenial";
            appData.setTacDenial(getBytes(app, element));
            element = "tacOnline";
            appData.setTacOnline(getBytes(app, element));

            try {
                element = "transactionCategoryCode";
                appData.setTranCategoryCode(app.getString(element));
            } catch (JSONException e) {
                // ignoring
            }
            element = "chipCvmCapCvmReq";

            byte cvmCapabilityCvmReq = getBytes(ctlsOptions, element)[0];

            element = "chipCvmCapNoCvmReq";
            appData.setChipCapabilityNoCVM(getBytes(ctlsOptions, element)[0]);

            element = "merchantCatCode";
            appData.setMerchantCategoryCode(Utils.str2Bcd(app.getString(element)));


            try {
                element = "magCvmCapCvmReq";
                appData.setMagCapabilityCVMReq(getBytes(ctlsOptions, element)[0]);
            } catch (Exception e) {
                appData.setMagCapabilityCVMReq((byte) 0);
            }

            try {
                element = "magCvmCapNoCvmReq";
                appData.setMagCapabilityNoCVM(getBytes(ctlsOptions, element)[0]);
            } catch (Exception e) {
                appData.setMagCapabilityNoCVM((byte) 0);
            }

            byte[] riskManData = new byte[]{0x0C, 0x1A, (byte)(0x80 & 0xFF), 0, 0, 0, 0, 0};

            try {
                riskManData = getBytes(ctlsOptions, "trmRiskMgmtData");
                Log.d(TAG, "Terminal risk management data available");
            } catch (JSONException e) {
                if (this.terminalCapabilities == null || this.terminalCapabilities.length < 3) {
                    riskManData[0] |= (cvmCapabilityCvmReq & 0x60); // online PIN and signature based on cvm capability
                    Log.w(TAG, "No terminal capabilities available");
                    riskManData[1] |= (cvmCapabilityCvmReq & 0x60); // online PIN and signature for contact based on cvm capability
                } else {
                    Log.d(TAG, "Terminal capabilities: " + Utils.bytesToHexString(this.terminalCapabilities));
                    cvmCapabilityCvmReq &= (this.terminalCapabilities[1] | (~0x60)); // signature and PIN only in case supported for contact interface
                    riskManData[0] = (byte) (((cvmCapabilityCvmReq & 0x60) | 0x0C) & 0xFF);  // signature & PIN online based on capabilities, no CVM and on-Device CVM always on
                    riskManData[1] = (byte) (((this.terminalCapabilities[1] & 0x78) | 0x02) & 0xFF); // contact CVM capabilities (without plaintext offline PIN) + byte 2 bit 2 (RFU???)
                }
            }

            appData.setChipCapabilityCVMReq(cvmCapabilityCvmReq);

            Log.d(TAG, "risk management data: " + Utils.bytesToHexString(riskManData));

            appData.setTerminalRiskMgmtData(riskManData);

            element = "securityCapabilities";
            try {
                appData.setSecurityCapabilities(getBytes(ctlsOptions, element)[0]);
            } catch (JSONException e) {
                Log.d(TAG, "securityCapabilities not set");
            }
            element = "trxMode";
            int trxMode = ctlsOptions.getInt(element);
            element = "onDeviceCvmFlag";
            int onDeviceCvmFlag = ctlsOptions.getInt(element);
            byte[] kernelConfig = new byte[]{0x10}; // 0x10 - Relay resistance protocol supported
            if (onDeviceCvmFlag != 0) {
                kernelConfig[0] |= 0x20;
            }
            
            switch (trxMode) {
                case 1:
                    kernelConfig[0] |= 0x40;
                    break;
                case 2:
                    kernelConfig[0] |= 0x80;
                    break;
                default:
                    break;
            }
            appData.setKernel2Configuration(kernelConfig);

        } catch (Exception e) {
            Log.e(TAG, "Error processing element " + element + ": " + app);
            e.printStackTrace();
        }
    }

    private ArrayList<JSAppData> getAppDataList(JSONArray apps, int currCode) throws JSONException {
        ArrayList<JSAppData> appDataList = new ArrayList<>();

        this.combinationLists.clear();
        Log.d(TAG, "Generating combinationLists from apps (length: " + apps.length() + ")");

        for (int i = 0; i < apps.length(); i++) {
            JSONObject app = apps.getJSONObject(i);
            if (app.getInt("readerTechnology") != 2) {
                continue;
            }
            JSAppData appData = new JSAppData();
            int kernelId = app.getInt("kernelId");
            appData.setAid(getBytes(app, "aid"));

            Log.d(TAG, "Kernel ID: " + kernelId);
            switch (kernelId) {
                case 2:
                    setKernel2Parameters(appData, app);
                    break;
                case 3:
                    setKernel3Parameters(appData, app);
                    break;
                case 4:
                    setKernel4Parameters(appData, app);
                    break;
                case 7:
                    setKernel7Parameters(appData, app);
                    break;
                default:
                    Log.e(TAG, "Unsupported kernel ID: " + kernelId);
                    continue;
            }
            appData.setAppVersionNumber(getBytes(app, "applicationVersionNumber"));

            appData.setIfdSerialNo(Build.SERIAL);
            byte[] kernelIdArray = new byte[1];
            kernelIdArray[0] = (byte) kernelId;

            appData.setKernelId(kernelIdArray);

            ArrayList<LimitSet> limitSet = getLimits(app, combinationLists, kernelIdArray, appData.getAid(), currCode);
            appData.setLimitSetList(limitSet);
            appDataList.add(appData);
        }

        Log.d(TAG, "Generated combinationLists (size: " + combinationLists.size() + ")");

        return appDataList;
    }

    private static final byte[] maxTrxLimit = new byte[]{(byte)(0x99 & 0xFF), (byte)(0x99 & 0xFF), (byte)(0x99 & 0xFF), (byte)(0x99 & 0xFF), (byte)(0x99 & 0xFF), (byte)(0x99 & 0xFF)};

    private ArrayList<LimitSet>  getLimits(JSONObject app, ArrayList<CtlsCombinationList>  combinationLists, byte[] kernelIdArray, byte[] aid, int currCode) {

        JSONArray limitConfiguration;

        try {
            limitConfiguration = app.getJSONObject("amountLimitConfiguration").getJSONArray("limitConfiguration");

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        ArrayList<LimitSet> limitSetList = new ArrayList<>();

        for (int i = 0; i < limitConfiguration.length(); i++) {
            try {

                JSONObject l = limitConfiguration.getJSONObject(i);
                String currencyCode = l.getString("currCode");
                if (Integer.parseInt(currencyCode) != currCode) {
                    continue;
                }
                LimitSet limitSet = new LimitSet();
                byte[] ctlessTrxLimit = maxTrxLimit;

                try {
                    ctlessTrxLimit = getBcd(l, "ctlessTrxLimit");
                } catch (JSONException e) {
                    Log.d(TAG, "No trx limit, setting default one");
                }

                limitSet.setReaderCtlsTranLimit(ctlessTrxLimit);

                limitSet.setCurrencyCode(currencyCode);
                limitSet.setCurrencyExponent(Utils.str2Bcd(this.currencies.get(currencyCode)));

                limitSet.setReaderCVMRequiredLimit(getBcd(l, "ctlessCvmLimit"));
                limitSet.setReaderCtlsFloorLimit(getBcd(l, "ctlessFloorLimit"));

                limitSet.setReaderCtlsTermFloorLimit(getBcd(l, "ctlessFloorLimit"));
                if (kernelIdArray[0] == 2) {
                        limitSet.setReaderCtlsTranLimitOnDev(getBcd(l, "ctlessTrxLimitOnDeviceCvm"));
                        limitSet.setReaderCtlsTranLimitNoOnDev(getBcd(l, "ctlessTrxLimitNoOnDeviceCvm"));
                }
                limitSet.setAppSelectionIndicator(0);
                limitSet.setStatusCheckAllowedFlag(0);
                limitSet.setZeroAmountAllowedFlag(0);

                try {
                    byte[] ctlessAppProgId = getBytes(l, "ctlessAppProgId");
                    limitSet.setAppProgramId(ctlessAppProgId);
                } catch (JSONException e) {
                    // ignoring
                }

                limitSetList.add(limitSet);

                byte[] ttq = null;
                int ctlessStatusCheckSupportFlag = 0;
                int ctlessZeroAmountAllowedFlag = 0;

                if (kernelIdArray[0] == 3) {
                    if (emvTrxTypeSet) {
                        try {
                            if (emvTrxType == 0x09) {
                                ttq = getBytes(app, "contactlessTrmTrxQualCash");
                            }
                            if (ttq == null) {
                                ttq = getBytes(app, "contactlessTrmTrxQual");
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, "Can't set TTQ for trx type " + emvTrxType);
                        }
                    }
                    try {
                        ctlessStatusCheckSupportFlag = l.getInt("ctlessStatusCheckSupportFlag");
                    } catch (JSONException e) {
                        Log.d(TAG, "No ctlessStatusCheckSupportFlag");
                    }

                    try {
                        ctlessZeroAmountAllowedFlag = l.getInt("ctlessZeroAmountAllowedFlag");
                    } catch (JSONException e) {
                        Log.d(TAG, "No ctlessZeroAmountAllowedFlag");
                    }
                }

                if (kernelIdArray[0] == 7) {
                    Log.e(TAG, "kernel 7 set");
                    if (emvTrxTypeSet) {
                        try {
                            if (emvTrxType == 0x09) {
                                ttq = getBytes(app, "contactlessTrmTrxQualCash");
                            }
                            if (ttq == null) {
                                ttq = getBytes(app, "contactlessTrmTrxQual");
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, "Can't set TTQ for trx type " + emvTrxType);
                        }
                    }
                }

                if (ttq == null) {
                    Log.w(TAG, "Default TTQ set in combination for kernelId = " + kernelIdArray[0]);
                    ttq = Utils.str2Bcd("32204000");
                }

                int appSelectionIndicator = 0;
                try {
                    appSelectionIndicator = app.getInt("applicationSelectionIndicator");
                } catch (JSONException e) {
                    // ignoring
                }

                CtlsCombinationList clist = new CtlsCombinationList(kernelIdArray, aid, ttq, appSelectionIndicator, ctlessZeroAmountAllowedFlag, ctlessStatusCheckSupportFlag, getBcd(l, "ctlessFloorLimit"), ctlessTrxLimit, getBcd(l, "ctlessFloorLimit"), getBcd(l, "ctlessCvmLimit"), 0, limitSet.getCurrencyCode());
                combinationLists.add(clist);
            } catch (Exception e) {
                Log.e(TAG, "Exception for limit " + i);
                e.printStackTrace();
            }
        }

        Log.d(TAG, limitSetList.size() + " limits loaded");

        return limitSetList;

    }

    private void setCurrencyConfiguration(JSONArray currencies) throws JSONException {
        this.currencies = new TreeMap<>();
        for (int i = 0; i < currencies.length(); i++) {
            JSONObject currency = currencies.getJSONObject(i);
            this.currencies.put(Integer.toString(currency.getInt("code")), Integer.toString(currency.getInt("exp")));
        }
    }

    public static String terminalCapabilitiesToString(CtlsTerminalCapabilities tcap) {
        return String.format(Locale.ENGLISH, "CtlsTerminalCapabilities{kernelId:%d,CtlsTermCapabilities:%s,CtlsAddTermCapabilities:%s}",
                tcap.getKernelId(),
                tcap.getCtlsTermCapabilities()==null ? "null" : Utils.bcd2Str(tcap.getCtlsTermCapabilities()),
                Utils.bcd2Str(tcap.getCtlsAddTermCapabilities()));
    }

    private ArrayList<CaPublicKey> getCaPublicKeys(JSONObject reader) {
        ArrayList<CaPublicKey> cpkList = new ArrayList<>();

        try {
            JSONArray keys = reader.getJSONObject("certificationAuthConfiguration").getJSONArray("certificationAuthData");
            for (int i = 0; i < keys.length(); i++) {
                JSONObject k = keys.getJSONObject(i);
                CaPublicKey cpk = new CaPublicKey();
                cpk.setCheckSum(getBytes(k, "publicKeyChecksum"));
                cpk.setKeyExponent(getBytes(k, "publicKeyExponent"));
                cpk.setKeyIndexTerminal(getBytes(k, "publicKeyIndex"));
                cpk.setKeyModulus(getBytes(k, "publicKeyModulus"));
                cpk.setRid(getBytes(k, "rid"));
                cpkList.add(cpk);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cpkList;
    }


    private int doUpdate(String emvConfig) {
        ctlsInitConfig = new CtlsInitConfig();

        try {
            if (emvConfig == null) {
                Log.e(TAG, "emvConfig is NULL!");
                return -1;
            }
            JSONObject reader = new JSONObject(emvConfig);

            apps = reader.getJSONObject("applicationConfiguration");

            terminalCapabilities = getBytes(reader, "terminalCapabilities");
            Log.d(TAG, "config terminalCapabilities: " + Utils.bcd2Str(terminalCapabilities));
            additionalTerminalCapabilities = getBytes(reader, "additionalTerminalCapabilities");
            Log.d(TAG, "config additionalTerminalCapabilities: " + Utils.bcd2Str(additionalTerminalCapabilities));

            JSONArray currencies = reader.getJSONObject("currencyConfiguration").getJSONArray("currency");
            if (currencies != null) {
                setCurrencyConfiguration(currencies);
            }

            ctlsInitConfig.setCaPublicKeys(getCaPublicKeys(reader));

            ctlsInitConfig.setCountryCode(reader.getString("countryCode"));
            ctlsInitConfig.setMerchId(reader.getString("merchantId"));
            ctlsInitConfig.setTermId(reader.getString("terminalId"));

            int tType = reader.getInt("terminalType");
            byte terminalType = (byte)((tType / 10 * 0x10)  + (tType % 10));
            ctlsInitConfig.setTerminalType(terminalType);

            ArrayList<CtlsTerminalCapabilities> tcap_list = new ArrayList<>();

            try {
                JSONObject ctlessTrmCapabilities = reader.getJSONObject("ctlessTrmCapabilities");
                JSONArray ctlessTrmCapPerKernel = ctlessTrmCapabilities.getJSONArray("ctlessTrmCapPerKernel");
                for (int i = 0; i < ctlessTrmCapPerKernel.length(); ++i) {
                    JSONObject elem = ctlessTrmCapPerKernel.getJSONObject(i);
                    CtlsTerminalCapabilities tc = new CtlsTerminalCapabilities();
                    tc.setKernelId((byte) elem.getInt("kernelId"));
                    tc.setCtlsAddTermCapabilities(getBytes(elem, "ctlessAddTrmCap"));
                    tc.setCtlsTermCapabilities(terminalCapabilities);
                    tcap_list.add(tc);
                }
            } catch (JSONException e) {
                Log.i(TAG, "ctlessTrmCapPerKernel NOT FOUND");
            }

            ctlsTerminalCapabilitiesList = tcap_list;

        } catch (JSONException e) {
            e.printStackTrace();
            return -1;
        }
        return 0;
    }

    public synchronized int update(String config) {
        this.emvTrxType = -1;
        this.emvTrxTypeSet = false;
        return doUpdate(config);
    }

    public synchronized int updateForTransaction(String config, int emvTrxType, int currCode) {
        this.emvTrxType = emvTrxType;
        this.emvTrxTypeSet = true;
        return doUpdate(config) | prepareAppListForTransaction(emvTrxType, currCode);
    }

    public synchronized int prepareAppListForTransaction(int emvTrxType, int currCode) {
        this.emvTrxType = emvTrxType;
        this.emvTrxTypeSet = true;
        try {
            ctlsAppList = getAppDataList(apps.getJSONArray("emvApplication"), currCode);
        } catch (JSONException e) {
            e.printStackTrace();
            return -1;
        }
        return 0;
    }

    private Config() {
    }

    public synchronized CtlsInitConfig getCtlsInitConfig() {
        return ctlsInitConfig;
    }

    public synchronized Iterable<? extends CtlsTerminalCapabilities> getCtlsTerminalCapabilitiesList() {
        return ctlsTerminalCapabilitiesList;
    }

    public byte[] getTerminalCapabilities() {
        return terminalCapabilities;
    }

    public byte[] getAdditionalTerminalCapabilities() {
        return additionalTerminalCapabilities;
    }

    public synchronized Iterable<? extends JSAppData> getCtlsAppList() {
        return ctlsAppList;
    }

    public synchronized Iterable<? extends CtlsCombinationList> getCombinationLists() {
        return combinationLists;
    }

}
