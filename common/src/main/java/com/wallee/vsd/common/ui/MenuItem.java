package com.wallee.vsd.common.ui;

import android.graphics.drawable.Drawable;

public class MenuItem {
    private Drawable icon;
    private String text;
    private int token;
    private boolean passProtected;

    public MenuItem(String text, Drawable icon, boolean passProtected) {
        this.icon = icon;
        this.text = text;
        this.passProtected = passProtected;
    }

    public Drawable getIcon() {
        return this.icon;
    }

    public String getButtonText() {
        return this.text;
    }

    public boolean getPassProtected() {
        return this.passProtected;
    }
}
