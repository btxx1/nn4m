package com.wallee.vsd.common.integration.xml;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Namespace(prefix = "vcs-pos", reference = "http://www.vibbek.com/pos")
@Root(name = "reversalRequest", strict = false)
public class ReversalRequest {
    @Element(required = false)
    public String posId;

    public ReversalRequest(String posId) {
        this.posId = posId;
    }
}
