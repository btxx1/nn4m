package com.wallee.vsd.common.integration.xml;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Namespace(prefix = "vcs-pos", reference = "http://www.vibbek.com/pos")
@Root(name = "beFinalBalanceResponse", strict = false)
public class BeFinalBalanceResponse {
    @Element(required = false)
    public String trxSyncNumber;
}
