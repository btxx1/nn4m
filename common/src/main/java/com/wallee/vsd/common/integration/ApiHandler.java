package com.wallee.vsd.common.integration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;

import com.wallee.android.till.sdk.ApiClient;
import com.wallee.android.till.sdk.ApiMessageType;
import com.wallee.android.till.sdk.Utils;
import com.wallee.android.till.sdk.data.CancelationResult;
import com.wallee.android.till.sdk.data.FinalBalanceResult;
import com.wallee.android.till.sdk.data.SubmissionResult;
import com.wallee.android.till.sdk.data.TransactionCompletion;
import com.wallee.android.till.sdk.data.TransactionCompletionResponse;
import com.wallee.android.till.sdk.data.TransactionVoid;
import com.wallee.android.till.sdk.data.TransactionVoidResponse;
import com.wallee.android.till.sdk.data.TransmissionResult;
import com.wallee.vsd.common.integration.data.SdkVersion;

public class ApiHandler extends Handler {
    private static final String TAG = "ApiHandler";

    public ApiHandler(Looper looper) {
        super(looper);
    }

    public static void launchTillApplication(Context context) {
        try {
            Log.d(TAG, "Launching Till Application...");
            Intent intent = new Intent("com.wallee.android.TILL_APPLICATION");
            if (intent.resolveActivity(context.getPackageManager()) != null) {
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } else {
                Log.d(TAG, "Till Application is not installed...");
            }
        } catch (Exception e) {
            Log.e(TAG, "Failed to launch Till Application", e);
        }
    }

    @Override
    public void handleMessage(Message request) {
        Log.d(TAG, "handleMessage: " + request);
        try {
            ApiMessageType type = ApiMessageType.values()[request.arg1];
            if (type != ApiMessageType.CHECK_API_SERVICE_COMPATIBILITY && !isSdkVersionCompatible(request)) {
                handleSdkVersionNotSupported(request);
                return;
            }
            switch (type) {
                case CHECK_API_SERVICE_COMPATIBILITY:
                    handleCheckApiServiceCompatibility(request);
                    break;
                case AUTHORIZE_TRANSACTION:
                    handleAuthorizeTransaction(request);
                    break;
                case COMPLETE_TRANSACTION:
                    handleCompleteTransaction(request);
                    break;
                case VOID_TRANSACTION:
                    handleVoidTransaction(request);
                    break;
                case CANCEL_LAST_TRANSACTION_OPERATION:
                    handleCancelLastTransactionOperation(request);
                    break;
                case EXECUTE_TRANSMISSION:
                    handleExecuteTransmission(request);
                    break;
                case EXECUTE_SUBMISSION:
                    handleExecuteSubmission(request);
                    break;
                case EXECUTE_FINAL_BALANCE:
                    handleExecuteFinalBalance(request);
                    break;
            }
        } catch (Exception e) {
            Log.d(TAG, "Failed to handleMessage", e);
        }
    }

    // add here more conditions in the future
    private boolean isSdkVersionCompatible(Message request) {
        SdkVersion requestVersion = new SdkVersion(Utils.getSdkVersion(request.getData()));
        SdkVersion thisVersion = new SdkVersion(ApiClient.VERSION);
        return thisVersion.compareTo(requestVersion) >= 0; // backward compatibility support
    }

    private void handleSdkVersionNotSupported(Message request) throws RemoteException {
        String sdkVersion = Utils.getSdkVersion(request.getData());
        Message response = Message.obtain();
        response.arg1 = ApiMessageType.SDK_VERSION_NOT_SUPPORTED_REPLY.ordinal();
        Bundle bundle = Utils.toBundle("The service API SDK version (" + ApiClient.VERSION + ") does not have support for your SDK version (" + sdkVersion + ")");
        response.setData(bundle);
        request.replyTo.send(response);
    }

    private void handleCheckApiServiceCompatibility(Message request) throws RemoteException {
        Message response = Message.obtain();
        response.arg1 = ApiMessageType.CHECK_API_SERVICE_COMPATIBILITY.ordinal();
        Bundle bundle = Utils.toBundle((Boolean) isSdkVersionCompatible(request));
        response.setData(bundle);
        request.replyTo.send(response);
    }

    private void handleAuthorizeTransaction(Message request) {
        ThirdPartyAppsHandlerActivity.start(request);
    }

    private void handleCompleteTransaction(Message request) throws RemoteException {
        TransactionCompletion transaction = Utils.getTransactionCompletion(request.getData());
        TransactionCompletionResponse transactionResponse = new CompleteTransactionHandler().handle(transaction);
        Message response = Message.obtain();
        response.arg1 = ApiMessageType.COMPLETE_TRANSACTION.ordinal();
        Bundle bundle = Utils.toBundle(transactionResponse);
        response.setData(bundle);
        request.replyTo.send(response);
    }

    private void handleVoidTransaction(Message request) throws RemoteException {
        TransactionVoid transaction = Utils.getTransactionVoid(request.getData());
        TransactionVoidResponse transactionResponse = new VoidTransactionHandler().handle(transaction);
        Message response = Message.obtain();
        response.arg1 = ApiMessageType.VOID_TRANSACTION.ordinal();
        Bundle bundle = Utils.toBundle(transactionResponse);
        response.setData(bundle);
        request.replyTo.send(response);
    }

    private void handleCancelLastTransactionOperation(Message request) throws RemoteException {
        CancelationResult result = new CancelLastTransactionOperationHandler().handle();
        Message response = Message.obtain();
        response.arg1 = ApiMessageType.CANCEL_LAST_TRANSACTION_OPERATION.ordinal();
        Bundle bundle = Utils.toBundle(result);
        response.setData(bundle);
        request.replyTo.send(response);
    }

    private void handleExecuteSubmission(Message request) throws RemoteException {
        SubmissionResult result = new ExecuteSubmissionHandler().handle();
        Message response = Message.obtain();
        response.arg1 = ApiMessageType.EXECUTE_SUBMISSION.ordinal();
        Bundle bundle = Utils.toBundle(result);
        response.setData(bundle);
        request.replyTo.send(response);
    }

    private void handleExecuteTransmission(Message request) throws RemoteException {
        TransmissionResult result = new ExecuteTransmissionHandler().handle();
        Message response = Message.obtain();
        response.arg1 = ApiMessageType.EXECUTE_TRANSMISSION.ordinal();
        Bundle bundle = Utils.toBundle(result);
        response.setData(bundle);
        request.replyTo.send(response);
    }

    private void handleExecuteFinalBalance(Message request) throws RemoteException {
        FinalBalanceResult result = new ExecuteFinalBalanceHandler().handle();
        Message response = Message.obtain();
        response.arg1 = ApiMessageType.EXECUTE_FINAL_BALANCE.ordinal();
        Bundle bundle = Utils.toBundle(result);
        response.setData(bundle);
        request.replyTo.send(response);
    }
}
