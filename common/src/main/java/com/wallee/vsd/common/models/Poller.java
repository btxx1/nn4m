package com.wallee.vsd.common.models;

import android.util.Log;
import com.wallee.vsd.common.models.vsd.MiscIface;

import static com.wallee.vsd.common.vsd.XG.TAG_RC;

public abstract class Poller {
    protected static final String TAG = "Poller";
    protected Thread pollingThread = null;
    private MiscIface misc;

    abstract protected boolean eventDetected();

    protected abstract String getResultTag();

    private int getResultValue() {
        return 0;
    }

    public Poller(MiscIface misc) {
        this.misc = misc;
    }

    private class PollingRunnable implements Runnable {
        private Poller runningObject;
        protected final int pollingInterval = 100;
        private PollingRunnable(Poller runningObject) {
            this.runningObject = runningObject;
        }


        public void run() {
            try {
                while (true) {
                    synchronized (runningObject) {
                        if (pollingThread == null) {
                            return;
                        }
                        if (eventDetected()) {
                            Log.e(TAG, runningObject.getClass().getSimpleName() + ": event detected");
                            misc.postResult(String.format("{\\\"%s\\\":%d,\\\"%s\\\":%d}",
                                    TAG_RC, 0,
                                    getResultTag(), getResultValue()));
                            pollingThread = null;
                            return;
                        }
                    }
                    Thread.sleep(pollingInterval);
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    protected synchronized void startPollingThread() {
        if (pollingThread != null) {
            return;
        }
        pollingThread = new Thread(new PollingRunnable(this));
        pollingThread.start();
    }

    protected void stopPollingThread() {
        Thread thread;
        synchronized (this) {
            thread = pollingThread;
            if (thread == null) {
                return;
            }
            pollingThread = null;
        }
        try {
            thread.interrupt();
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}