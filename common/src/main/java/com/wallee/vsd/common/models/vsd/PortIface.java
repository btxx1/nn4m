package com.wallee.vsd.common.models.vsd;

public interface PortIface {
    int open(int portNumber, String params);

    int getDescriptor(int portNumber);

    int close(int portNumber);

    int send(int portNumber, String msg);

    String receive(int portNumber, int timeoutMs);
}
