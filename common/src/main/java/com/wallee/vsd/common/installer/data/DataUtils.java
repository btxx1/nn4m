package com.wallee.vsd.common.installer.data;

import android.os.Build;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public final class DataUtils {
    private static final Gson GSON = (new GsonBuilder()).setVersion(1.0D).create();

    public static UrlResponse toUrlResponse(String json) {
        return GSON.fromJson(json, UrlResponse.class);
    }

    public static String toJson(AppReport report) {
        return GSON.toJson(report);
    }

    public static String getSerial() {
        return Build.SERIAL; // Misc.getSerialNumber
    }

    public static String getApkFileName(String downloadUrl) {
        String[] urlParts = downloadUrl.split("/");
        return urlParts[urlParts.length - 1];
    }
}
