package com.wallee.vsd.common.integration.xml;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Namespace(prefix = "vcs-pos", reference = "http://www.vibbek.com/pos")
@Root(name = "financialTrxRequest", strict = false)
public class FinancialTrxRequest {
    @Element(required = false)
    public String posId;
    @Element(required = false)
    public TrxData trxData;

    public FinancialTrxRequest(String posId, TrxData trxData) {
        this.posId = posId;
        this.trxData = trxData;
    }
}
