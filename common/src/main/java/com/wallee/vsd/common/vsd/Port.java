package com.wallee.vsd.common.vsd;

import android.util.Log;
import android.webkit.JavascriptInterface;

import com.pax.dal.IComm;
import com.wallee.vsd.common.models.IOReader;
import com.wallee.vsd.common.models.vsd.MiscIface;
import com.wallee.vsd.common.models.vsd.PortIface;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class Port implements PortIface {
    protected abstract class ComReader implements IOReader, Runnable {
        protected boolean eventAvailable;
        protected int descriptor;
        protected Lock lock;
        protected Condition eventConsumed;
        protected byte[] data;
        protected MiscIface misc;
        protected Thread thread;
        protected boolean isCloseRequested;
        private static final String TAG = "common/Port";

        public class ReadCanceledException extends Exception {}
        public class ReadOtherException extends Exception {}

        public ComReader(int descriptor, MiscIface misc) {
            this.misc = misc;
            this.descriptor = descriptor;
            lock = new ReentrantLock();
            eventConsumed = lock.newCondition();
            isCloseRequested = false;
        }

        protected synchronized byte[] getData() {
            byte[] res = data;
            data = null;
            eventAvailable = false;
            this.signal();
            return res;
        }

        public void clearEvent() {
            getData();
        }

        public void signal() {
            lock.lock();
            eventConsumed.signal();
            lock.unlock();
        }

        public synchronized  boolean isEventAvailable() {
            return eventAvailable;
        }

        public int getDescriptor() {
            return descriptor;
        }

        public synchronized void open() {
            if (thread != null) {
                Log.e(TAG, "Already open");
            } else {
                thread = new Thread(this);
                thread.start();
            }
        }

        protected abstract void cancelRecv();
        protected abstract void disconnect();

        public synchronized void close() {
            if (thread == null) {
                Log.e(TAG, "Already closed");
                return;
            }
            isCloseRequested = true;
            this.cancelRecv();
            Log.d(TAG, "Signal...");
            this.signal();
            Log.d(TAG, "Signal sent");
            try {
                Log.d(TAG, "Joining...");
                thread.join();
                Log.d(TAG, "Joined...");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            thread = null;
            Log.d(TAG, "Disconnecting...");
            this.disconnect();
            Log.d(TAG, "Disconnected");
        }

        protected abstract void waitForData() throws ReadCanceledException, ReadOtherException;
        protected abstract int send(byte[] data);
        protected abstract byte[] receive();

        protected boolean dataAvailable() {
            return data.length > 0;
        }


        public void run() {
            while (true) {
                lock.lock();
                try {
                    Log.d(TAG, "RECEIVING...");
                    waitForData();
                    Log.d(TAG, "SYNCHRONIZED...");
                    synchronized (this) {
                        Log.d(TAG, "Data available...");
                        if (dataAvailable()) {
                            Log.d(TAG, "Data detected on port");
                            eventAvailable = true;
                            misc.newEventNotify();
                        } else {
                            Log.e(TAG, "Can't read any data");
                            break;
                        }
                    }
                    Log.d(TAG, "Awaiting...");
                    eventConsumed.await();
                    Log.d(TAG, "Re-entering loop...");
                } catch (ReadCanceledException e) {
                    try {
                        eventConsumed.await();
                        if (isCloseRequested) {
                            Log.d(TAG, "Exiting thread..");
                            break;
                        } else {
                            Log.d(TAG, "Reading interrupted, awaiting...");
                        }
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                } catch (ReadOtherException e) {
                    Log.d(TAG, "Thread ends");
                    e.printStackTrace();
                    break;
                } finally {
                    lock.unlock();
                }
            }
        }
    }

    private static final int MAX_PORTS = 20;
    private static final String TAG = "common/Port";
    private static final int FIRST_DESCRIPTOR = 100;
    protected MiscIface misc;

    private ComReader[] ports;

    public Port(MiscIface misc) {
        this.misc = misc;
        ports = new ComReader[MAX_PORTS];
    }


    protected abstract ComReader openPort(int portNumber, String params);

    @Override
    @JavascriptInterface
    public synchronized int open(int portNumber, String params) {
        if (portNumber >= MAX_PORTS) {
            Log.e(TAG, "Port not supported");
            return -1;
        }

        if (ports[portNumber] != null) {
            this.close(portNumber);
        }

        ports[portNumber] = openPort(FIRST_DESCRIPTOR + portNumber, params);
        if (ports[portNumber] == null) {
            Log.e(TAG, "Can't open port " + portNumber);
            return -1;
        }
        Misc.getInstance().addIOReaderMapping(ports[portNumber]);
        ports[portNumber].open();
        return 0;
    }

    @Override
    @JavascriptInterface
    public synchronized int getDescriptor(int portNumber) {
        if (portNumber >= MAX_PORTS) {
            Log.e(TAG, "Port not supported");
            return -1;
        }
        if (ports[portNumber] == null) {
            return -1;
        }

        return ports[portNumber].getDescriptor();
    }

    @Override
    @JavascriptInterface
    public synchronized int close(int portNumber) {
        if (portNumber >= MAX_PORTS) {
            Log.e(TAG, "Incorrect port number: " + portNumber);
            return -1;
        }
        if (ports[portNumber] == null) {
            return 0;
        }
        Log.d(TAG, "Closing port...");
        ports[portNumber].close();
        Log.d(TAG, "Port closed");
        Misc.getInstance().removeIOReaderMapping(ports[portNumber]);
        ports[portNumber] = null;
        return 0;
    }


    @Override
    @JavascriptInterface
    public synchronized int send(int portNumber, String msg) {
        if (portNumber >= MAX_PORTS) {
            Log.e(TAG, "Incorrect port number: " + portNumber);
            return -1;
        }
        if (ports[portNumber] == null) {
            Log.e(TAG, String.format("Port %d not open", portNumber));
            return -1;
        }
        byte[] toSend;
        try {
            toSend = msg.getBytes("windows-1252");
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "Can't get message bytes");
            e.printStackTrace();
            return -1;
        }
        return ports[portNumber].send(toSend);
    }

    @Override
    @JavascriptInterface
    public synchronized String receive(int portNumber, int timeoutMs) {
        if (portNumber >= MAX_PORTS) {
            Log.e(TAG, "Incorrect port number: " + portNumber);
            return null;
        }

        if (ports[portNumber] == null) {
            Log.e(TAG, String.format("Port %d not open", portNumber));
            return null;
        }

        if (!ports[portNumber].isEventAvailable()) {
            return null;
        }

        byte[] bytes = ports[portNumber].receive();


        if (bytes == null) {
            Log.e(TAG, "NO data received!");
            return null;
        }

        try {
            String result = new String(bytes, "windows-1252");
            Log.d(TAG, String.format("Received %d bytes", result.length()));
            return result;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

}
