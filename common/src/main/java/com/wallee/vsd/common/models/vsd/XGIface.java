package com.wallee.vsd.common.models.vsd;

public interface XGIface {

    int display(String strdef, int minimalDisplayTimeMs);

    String pollEvent();

    void ignoreTokens(int first, int last);

    void setBackgroundMode(boolean backgroundMode);

    String getDisplayContent();
}
