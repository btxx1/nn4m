package com.wallee.vsd.common.models.vsd;

public interface CtlsIface {
    int init();
    String appSelection();
    String reselectApp();
    String getTlvData(String tags);
    String getCvmResults();
    int updatePreProcInfo(String config, int currCode, int exponent, long amount, long amountOther, int trxNum, int emvTrxType, String parameters, String date, String time);
    int updateConfig(String config);
    String processTransaction(int kernelId);
}

