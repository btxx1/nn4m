package com.wallee.vsd.common.integration.xml;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Namespace(prefix = "vcs-pos", reference = "http://www.vibbek.com/pos")
@Root(name = "financialTrxResponse", strict = false)
public class FinancialTrxResponse {
    @Element(required = false)
    public String ep2AuthResponseCode;
    @Element(required = false)
    public String ep2AuthResult;
    @Element(required = false)
    public String ep2AuthCode;
    @Element(required = false)
    public String ep2TrmId;
    @Element(required = false)
    public String ep2TrxSeqCnt;
    @Element(required = false)
    public String trxResult;
    @Element(required = false)
    public String amountAuth;
    @Element(required = false)
    public String amountAuthCurr;
    @Element(required = false)
    public String amountTip;
    @Element(required = false)
    public String transactionRefNumber;
    @Element(required = false)
    public String cardNumber;
    @Element(required = false)
    public String cardSeqNumber;
    @Element(required = false)
    public String cardExpDate;
    @Element(required = false)
    public String cardAppLabel;
    @Element(required = false)
    public String cardAppId;
    @Element(required = false)
    public String transactionTime;
    @Element(required = false)
    public String acquirerId;
    @Element(required = false)
    public String dccTrxAmount;
    @Element(required = false)
    public String dccTrxAmountCurr;
    @Element(required = false)
    public String cvm;
    @Element(required = false)
    public String amountRemaining;
    @Element(required = false)
    public String partialApprovalFlag;
    @Element(required = false)
    public String trxSyncNumber;
    @Element(required = false)
    public String panToken;
}
