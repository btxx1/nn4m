package com.wallee.vsd.common;

public interface JSReadyListener {
    void onJSReady();
}
