package com.wallee.vsd.common.applications;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wallee.vsd.common.R;
import com.wallee.vsd.common.applications.data.LauncherEntry;
import com.wallee.vsd.common.ui.CurrentActivity;
import com.wallee.vsd.common.vsd.Misc;

import java.util.List;

public class ApplicationsActivity extends CurrentActivity {

    private ApplicationsAdapter applicationsAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.applications_activity);
        findViewById(R.id.applications_back_button).setOnClickListener(v -> finish());

        RecyclerView recyclerView = findViewById(R.id.applications_list);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));

        applicationsAdapter = new ApplicationsAdapter(this, v -> launchApplication((LauncherEntry) v.getTag()));
        recyclerView.setAdapter(applicationsAdapter);

        if (savedInstanceState == null) {
            new ApplicationsLoaderThread(this).start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Misc.getInstance().restoreSystemBarsStatus();
        Misc.getInstance().activateSystemBars(true);
    }

    void renderApplications(List<LauncherEntry> entries) {
        applicationsAdapter.setData(entries);
    }

    private void launchApplication(LauncherEntry entry) {
        Intent intent = new Intent();
        intent.setComponent(entry.getComponentName());
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            startActivity(intent);
            Misc.getInstance().activateSystemBars(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
