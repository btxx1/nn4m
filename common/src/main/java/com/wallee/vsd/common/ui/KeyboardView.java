package com.wallee.vsd.common.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AppCompatActivity;

import com.wallee.vsd.common.R;
import com.wallee.vsd.common.vsd.XG;

import java.util.LinkedHashMap;

import novelpay.pl.npf.Npf;
import novelpay.pl.npf.pal.enums.EPinKeyType;
import novelpay.pl.npf.pal.exceptions.PedException;

public class KeyboardView extends FrameLayout implements View.OnClickListener {

    private static final String TAG = "KeyboardView";
    private KeyboardListener listener;

    private enum NumButton {
        BTN_1(R.id.key_1, R.string._1, (byte) 49),
        BTN_2(R.id.key_2, R.string._2, (byte) 50),
        BTN_3(R.id.key_3, R.string._3, (byte) 51),
        BTN_4(R.id.key_4, R.string._4, (byte) 52),
        BTN_5(R.id.key_5, R.string._5, (byte) 53),
        BTN_6(R.id.key_6, R.string._6, (byte) 54),
        BTN_7(R.id.key_7, R.string._7, (byte) 55),
        BTN_8(R.id.key_8, R.string._8, (byte) 56),
        BTN_9(R.id.key_9, R.string._9, (byte) 57),
        BTN_0(R.id.key_0, R.string._0, (byte) 48);

        private final int viewId;
        private final int textId;
        private final byte order;

        NumButton(int viewId, int textId, byte order) {
            this.viewId = viewId;
            this.textId = textId;
            this.order = order;
        }
    }


    public static LinkedHashMap<Object, EPinKeyType> getNumKbdButtons() {
        final AppCompatActivity mainActivity = XG.getMainActivity();
        KeyboardView numKbd = mainActivity.findViewById(R.id.numKbd);
        LinkedHashMap<Object, EPinKeyType> res = new LinkedHashMap<>();
        for (NumButton nb : NumButton.values()) {
            res.put(numKbd.findViewById(nb.viewId), EPinKeyType.NUM);
        }
        res.put(numKbd.findViewById(R.id.key_x), EPinKeyType.CANCEL);
        res.put(numKbd.findViewById(R.id.key_back), EPinKeyType.CLEAR);
        res.put(numKbd.findViewById(R.id.key_enter), EPinKeyType.ENTER);
        return res;
    }

    public static void reorderNumKbdButtons(byte[] buttonsOrder) {
        final AppCompatActivity mainActivity = XG.getMainActivity();
        KeyboardView numKbd = mainActivity.findViewById(R.id.numKbd);
        for (NumButton nb1 : NumButton.values()) {
            Button button = numKbd.findViewById(nb1.viewId);
            byte order = buttonsOrder[nb1.ordinal()];
            for (NumButton nb2 : NumButton.values()) {
                if (nb2.order == order) {
                    button.setText(nb2.textId);
                }
            }
        }
    }

    public static void resetNumKbdButtons() {
        final AppCompatActivity mainActivity = XG.getMainActivity();
        KeyboardView numKbd = mainActivity.findViewById(R.id.numKbd);
        for (NumButton nb : NumButton.values()) {
            Button button = numKbd.findViewById(nb.viewId);
            button.setText(nb.textId);
        }
    }

    public static void openPinKbd() {
        final AppCompatActivity mainActivity = XG.getMainActivity();
        synchronized (mainActivity) {
            mainActivity.runOnUiThread(() -> {
                synchronized (mainActivity) {
                    try {
                        LinkedHashMap<Object, EPinKeyType> buttonsMap = KeyboardView.getNumKbdButtons();
                        final byte[] buttonsOrder = Npf.pal().pedSetPinCustomKeyboard(true, buttonsMap);
                        KeyboardView.reorderNumKbdButtons(buttonsOrder);
                    } catch (PedException e) {
                        e.printStackTrace();
                    } finally {
                        mainActivity.notifyAll();
                    }
                }

            });
            try {
                mainActivity.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public KeyboardView(Context context) {
        super(context);
        init();
    }

    public KeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public KeyboardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.num_keyboard, this);
        initViews();
    }

    private void initViews() {
        $(R.id.key_0).setOnClickListener(this);
        $(R.id.key_1).setOnClickListener(this);
        $(R.id.key_2).setOnClickListener(this);
        $(R.id.key_3).setOnClickListener(this);
        $(R.id.key_4).setOnClickListener(this);
        $(R.id.key_5).setOnClickListener(this);
        $(R.id.key_6).setOnClickListener(this);
        $(R.id.key_7).setOnClickListener(this);
        $(R.id.key_8).setOnClickListener(this);
        $(R.id.key_9).setOnClickListener(this);
        $(R.id.key_x).setOnClickListener(this);
        $(R.id.key_back).setOnClickListener(this);
        $(R.id.key_enter).setOnClickListener(this);
        $(R.id.key_dot).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick ");
        if(this.listener!=null) {
            Log.d(TAG, "onClick - listener");
            int id = v.getId();
            if (id == R.id.key_x) {
                this.listener.onCancel();
            } else if (id == R.id.key_back) {
                this.listener.onClear();
            } else if (id == R.id.key_enter) {
                this.listener.onCommit();
            } else if (id == R.id.key_dot) {
                this.listener.onEnter(".");
            } else {
                this.listener.onEnter(v.getTag().toString());
            }
        }
    }

    protected <T extends View> T $(@IdRes int id) {
        return (T) super.findViewById(id);
    }

    public void setKeyboardListener(KeyboardListener listener) {
        Log.d(TAG, "setKeyboardListener");
        this.listener = listener;
    }

    public void resetKeyboardListener() {
        Log.d(TAG, "resetKeyboardListener");
        this.listener = null;
    }

}
