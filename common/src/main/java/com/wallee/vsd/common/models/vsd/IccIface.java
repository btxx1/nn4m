package com.wallee.vsd.common.models.vsd;

public interface IccIface {
    int present();
    int open();
    void close();
    int resetChip();
    int detect();
}
