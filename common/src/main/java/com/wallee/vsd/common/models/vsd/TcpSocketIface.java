package com.wallee.vsd.common.models.vsd;

import java.nio.channels.Selector;

public interface TcpSocketIface {
    int create();

    int connect(int fd, String hostname, int port);

    int close(int fd);

    int send(int fd, String msg);

    int sendIsoString(int fd, String msg);

    String receive(int fd);

    String receive(int fd, String encoding);

    int setBlocking(int fd, boolean blocking);

    int bind(int fd, String address, int port);

    int listen(int fd, int maxPending);

    String accept(int fd);

    int setTimeout(int fd, int timeoutMs);

    boolean isConnecting(int fd);

    int getLastErrorCode();

    void closeAll();

    int register(Selector selector, int[] readFds, int[] writeFds, int[] exceptionFds);
}
