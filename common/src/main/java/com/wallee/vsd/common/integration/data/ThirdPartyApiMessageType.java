package com.wallee.vsd.common.integration.data;

import com.wallee.android.till.sdk.ApiMessageType;

public enum ThirdPartyApiMessageType {
    AUTHORIZE_TRANSACTION(ApiMessageType.AUTHORIZE_TRANSACTION, "com.wallee.android.AUTHORIZE_TRANSACTION_BEFORE", "com.wallee.android.AUTHORIZE_TRANSACTION_AFTER");
    // TODO add more if required

    public ApiMessageType original;
    public String actionBefore;
    public String actionAfter;

    ThirdPartyApiMessageType(ApiMessageType original, String actionBefore, String actionAfter) {
        this.original = original;
        this.actionBefore = actionBefore;
        this.actionAfter = actionAfter;
    }

    public static ThirdPartyApiMessageType find(ApiMessageType original) {
        for (ThirdPartyApiMessageType type : ThirdPartyApiMessageType.values()) {
            if (type.original == original) {
                return type;
            }
        }
        throw new IllegalStateException(original + " is not supported for third party integration");
    }
}
