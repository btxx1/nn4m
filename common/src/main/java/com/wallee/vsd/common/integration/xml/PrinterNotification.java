package com.wallee.vsd.common.integration.xml;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Namespace(prefix = "vcs-pos", reference = "http://www.vibbek.com/pos")
@Root(name = "printerNotification", strict = false)
public class PrinterNotification {
    @Element(required = false)
    public String merchantReceipt;
    @Element(required = false)
    public String cardholderReceipt;
}
