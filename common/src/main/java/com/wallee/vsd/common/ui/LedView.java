package com.wallee.vsd.common.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import com.wallee.vsd.common.R;


public class LedView extends View {

    Paint painter;
    Paint border_painter;
    private final int ledColor;
    private final int ledSize;

    private int status;

    public LedView(Context context, AttributeSet attrs) {
        super(context, attrs);

        status = 0x0;

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.LedView, 0, 0);
        try {
            ledColor = a.getInteger(R.styleable.LedView_ledColor, 0x50ff50);
            ledSize = a.getDimensionPixelSize(R.styleable.LedView_ledSize, 50);
        } finally {
            a.recycle();
        }
        painter = new Paint();
        painter.setStyle(Paint.Style.FILL);
        painter.setAntiAlias(true);

        border_painter = new Paint();
        border_painter.setStyle(Paint.Style.STROKE);
        border_painter.setColor(Color.BLACK);
        border_painter.setAntiAlias(true);
        border_painter.setStrokeWidth(3);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        painter.setColor(ledColor);
        int viewWidthSpacing = this.getMeasuredWidth()/8;
        int viewHeightHalf = this.getMeasuredHeight()/2;
        int posX = this.getMeasuredWidth() - viewWidthSpacing;
        for (int mask = 0x08; mask != 0; mask >>= 1) {
            if ((mask & status) != 0) {
                canvas.drawCircle(posX, viewHeightHalf, ledSize / 2, painter);
            }
            canvas.drawCircle(posX, viewHeightHalf, ledSize / 2, border_painter);
            posX -= 2 * viewWidthSpacing;
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int desiredWidth = getSuggestedMinimumWidth();
        int desiredHeight = getSuggestedMinimumHeight();
        if(status>0) {
            desiredHeight = Math.max(desiredHeight, 2 * ledSize);
            desiredWidth = Math.max(desiredWidth, 8 * ledSize);
            setMeasuredDimension(measureDimension(desiredWidth, widthMeasureSpec), desiredHeight);
        } else {
            setMeasuredDimension(
                    measureDimension(desiredWidth, widthMeasureSpec),
                    measureDimension(desiredHeight, heightMeasureSpec));
        }
    }

    private int measureDimension(int desiredSize, int measureSpec) {
        int result;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = desiredSize;
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }

        if (result < desiredSize){
            Log.e("LedView", "The view is too small, the content might get cut: " + MeasureSpec.toString(measureSpec));
        }
        return result;
    }

    public void setStatus(int status) {
        boolean layoutChanges = (status == 0) ^ (this.status == 0);
        this.status = status;
        if(layoutChanges) requestLayout();
        invalidate();
    }

}
