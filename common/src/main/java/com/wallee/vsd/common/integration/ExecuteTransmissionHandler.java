package com.wallee.vsd.common.integration;

import com.wallee.android.till.sdk.data.Receipt;
import com.wallee.android.till.sdk.data.ResultCode;
import com.wallee.android.till.sdk.data.State;
import com.wallee.android.till.sdk.data.TransmissionResult;
import com.wallee.vsd.common.integration.data.DataUtils;
import com.wallee.vsd.common.integration.xml.BeTransmissionRequest;
import com.wallee.vsd.common.integration.xml.XmlUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class ExecuteTransmissionHandler {
    private final NetClient client = new NetClient();

    TransmissionResult handle() {
        if (client.connectWithServer()) {
            try {
                BeTransmissionRequest xmlRequest = createXmlRequest();
                List<Receipt> receipts = new ArrayList<>();
                TransmissionResult result = null;

                client.send(XmlUtils.toXml(xmlRequest));
                while (result == null) {
                    String xmlResponse = client.receive();
                    if (xmlResponse.isEmpty()) {
                        result = createErrorResult("Internal error - empty response");
                    } else if (XmlUtils.shouldConfirm(xmlResponse)) {
                        client.send(XmlUtils.CONFIRMATION);
                    } else if (XmlUtils.isPrinterNotification(xmlResponse)) {
                        Receipt receipt = DataUtils.printerNotificationToReceipt(XmlUtils.toPrinterNotification(xmlResponse));
                        if (receipt != null) {
                            receipts.add(receipt);
                        }
                    } else if (XmlUtils.isErrorNotification(xmlResponse)) {
                        result = createErrorResult(XmlUtils.toErrorNotification(xmlResponse).errorText);
                    } else if (XmlUtils.isTransmissionResponse(xmlResponse)) {
                        result = createResult(receipts);
                    }
                }

                return result;
            } catch (Exception e) {
                e.printStackTrace();
                return createErrorResult(e.getMessage());
            } finally {
                client.disConnectWithServer();
            }
        } else {
            return createErrorResult("Failed to connect to VPJ");
        }
    }

    private BeTransmissionRequest createXmlRequest() {
        return new BeTransmissionRequest(
                DataUtils.POS_ID
        );
    }

    private TransmissionResult createResult(List<Receipt> receipts) {
        return new TransmissionResult(
                State.SUCCESSFUL,
                new ResultCode(
                        "0",
                        "Success"
                ),
                receipts
        );
    }

    private TransmissionResult createErrorResult(String error) {
        return new TransmissionResult(
                State.FAILED,
                new ResultCode(
                        "error",
                        error
                ),
                Collections.emptyList()
        );
    }
}

