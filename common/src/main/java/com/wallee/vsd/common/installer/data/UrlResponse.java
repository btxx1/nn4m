package com.wallee.vsd.common.installer.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UrlResponse {
    @SerializedName("last-changed")
    public String lastChanged;
    public String reportUrl;
    public List<UrlApplication> applications;
}
