package com.wallee.vsd.common.ui;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.wallee.vsd.common.R;
import com.wallee.vsd.common.vsd.XG;

import java.util.ArrayList;


public class IconMenuAdapter extends RecyclerView.Adapter<IconMenuAdapter.MyViewHolder> {
    public static final int LOCK_INSET = 2;
    public static final int LOCK_ICON_COLOR = Color.parseColor("#11d9cc");
    private final Integer fgColor;
    private final Integer scColor;
    private Context context;
    private ArrayList<MenuItem> entries;
    private View.OnClickListener listener;
    private Drawable lockIcon;

    public IconMenuAdapter(@NonNull Context context, ArrayList<MenuItem> entries, Integer fgColor, Integer scColor, View.OnClickListener listener) {
        this.context = context;
        this.entries = entries;
        this.listener = listener;
        this.fgColor = fgColor;
        this.scColor = scColor;
        this.lockIcon = XG.getDrawable("lock", null).getConstantState().newDrawable().mutate();
        this.lockIcon.setTint(scColor == null ? LOCK_ICON_COLOR : scColor);
    }

    @NonNull
    @Override
    public IconMenuAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.grid_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull IconMenuAdapter.MyViewHolder holder, int position) {
        MenuItem entry = entries.get(position);
        Drawable icon = entry.getIcon();
        boolean passProtected = entry.getPassProtected();
        if (fgColor != null) {
            holder.label.setTextColor(fgColor);
            if (icon != null) {
                icon.setTint(fgColor);
            }
        }
        if (icon != null) {
            if (passProtected) {
                ShapeDrawable lockBg = new ShapeDrawable(new OvalShape());
                lockBg.setIntrinsicHeight(IconMenuView.ICON_SIZE / 3);
                lockBg.setIntrinsicWidth(IconMenuView.ICON_SIZE / 3);
                lockBg.getPaint().setColor(Color.WHITE);
                LayerDrawable finalDrawable = new LayerDrawable(new Drawable[]{icon, lockBg, lockIcon});

                int horizontalInset = (lockIcon.getIntrinsicWidth() - lockIcon.getIntrinsicWidth()) / 2;

                finalDrawable.setLayerInset(0, 0, 0, 0, 0); // icon.getIntrinsicHeight());
                finalDrawable.setLayerInset(1, 0, IconMenuView.ICON_SIZE * 2 / 3 - 2 * LOCK_INSET, IconMenuView.ICON_SIZE * 2 / 3 - 2 * LOCK_INSET, 0);
                finalDrawable.setLayerInset(2, LOCK_INSET, IconMenuView.ICON_SIZE * 2 / 3 - LOCK_INSET, IconMenuView.ICON_SIZE * 2 / 3 - LOCK_INSET, LOCK_INSET);
                holder.imageButton.setImageDrawable(finalDrawable);
            } else {
                holder.imageButton.setImageDrawable(icon);
            }
        }

        holder.imageButton.setId(position);
        {
            String labelText = entry.getButtonText();
            if (labelText != null) {
                holder.label.setVisibility(View.VISIBLE);
                holder.label.setText(labelText);
            } else {;
                holder.label.setVisibility(View.GONE);
            }
        }
        holder.imageButton.setOnClickListener(listener);

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.imageButton.getLayoutParams();

        // Set the height of this ImageButton
        params.height = IconMenuView.ICON_SIZE;

        // Set the width of that ImageButton
        params.width = IconMenuView.ICON_SIZE;

        // Apply the updated layout parameters to last ImageButton
        holder.imageButton.setLayoutParams(params);

        // Set the ImageButton image scale type for fourth ImageButton
        holder.imageButton.setScaleType(ImageView.ScaleType.FIT_XY);
    }

    @Override
    public int getItemCount() {
        return entries.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView label;
        ImageButton imageButton;


        public MyViewHolder(View itemView) {
            super(itemView);
            label = itemView.findViewById(R.id.label);
            imageButton = itemView.findViewById(R.id.icon);
        }

    }
}
