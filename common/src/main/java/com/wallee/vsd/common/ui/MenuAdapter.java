package com.wallee.vsd.common.ui;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.Space;
import android.widget.TextView;

import com.wallee.vsd.common.R;

import java.util.ArrayList;

public class MenuAdapter extends BaseAdapter {
    private static final String TAG = "MenuAdapter";
    private Context context;
    public static final String SEPARATOR = "-----";
    public static final String UP = "<----";
    public static final String RADIO_ON = "+ ";
    public static final String RADIO_OFF = "- ";

    LayoutInflater inflter;
    private View[] views;
    private RadioButton[] radioButtons;
    private int checkedPos = -1;

    private ArrayList<String> items; //data source of the list adapter

    //public constructor
    public MenuAdapter(Context context, ArrayList<String> items) {
        this.context = context;
        this.items = items;
        this.views = new View[items.size()];
        this.radioButtons = new RadioButton[items.size()];
        inflter = (LayoutInflater.from(context));
        for (int i = 0; i < items.size(); ++i) {
            if (items.get(i).startsWith(RADIO_ON)) {
                checkedPos = i;
            }
        }
        showContent();
    }

    @Override
    public int getCount() {
        return items.size(); //returns total of items in the list
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = inflter.inflate(R.layout.activity_listview, null);
        String itemdef = items.get(position);
        if(views[position]==null) {
            if(itemdef.equals(SEPARATOR)) {
                views[position] = new Space(context);
                views[position].setMinimumHeight(40);
            } else {
                if(itemdef.startsWith(RADIO_OFF) || itemdef.startsWith(RADIO_ON)) {
                    RadioButton r = (RadioButton) v.findViewById(R.id.radioButton);
                    r.setVisibility(View.VISIBLE);
                    if(itemdef.startsWith(RADIO_ON)) {
                        checkedPos = position;
                    }
                    r.setChecked(checkedPos == position);
                    itemdef = itemdef.substring(RADIO_ON.length());
                    r.setText(itemdef);
                    r.setVisibility(View.VISIBLE);
                    radioButtons[position] = r;
                } else {
                    TextView tv = (TextView) v.findViewById(R.id.textView);
                    tv.setText(itemdef);
                    tv.setVisibility(View.VISIBLE);
                }
                views[position] = v;
            }
            if (position % 2 == 0) {
                views[position].setBackgroundColor(Color.parseColor("#2000bfff"));
            }
        } else {
            if (radioButtons[position] != null) {
                radioButtons[position].setChecked(checkedPos == position);
            }
        }
        return views[position];
    }

    public void setContent(ArrayList<String> items) {
        this.items = items;
        this.views = new View[items.size()];
        showContent();
        notifyDataSetChanged();
    }

    public void showContent() {
        Log.d(TAG, "MenuAdapter content:");
        for (int i = 0; i < items.size(); ++i) {
            Log.d(TAG, "        . " + items.get(i));
        }
    }

    public void userSelection(int position) {
        String selItem = items.get(position);
        if(selItem.startsWith(RADIO_OFF) || selItem.startsWith(RADIO_ON)) {
            checkedPos = position;
            notifyDataSetChanged();
        }
    }
}
