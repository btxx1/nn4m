package com.wallee.vsd.common.print;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.SystemClock;
import android.util.Log;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.pax.dal.IDAL;
import com.pax.dal.IPrinter;
import com.pax.gl.page.IPage;
import com.pax.gl.page.PaxGLPage;
import com.pax.neptunelite.api.NeptuneLiteUser;
import com.wallee.vsd.common.models.vsd.PrinterIface;
import com.wallee.vsd.common.vsd.Updt;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

abstract public class Printer implements PrinterIface {
    final static String TAG = "Printer";
    protected static final String PRINTER_DEFAULT_FONT = "ubuntu_mono_fixed.ttf";
    protected static final int PRINTER_OK_STATUS = 0;
    protected static final int PRINTER_BUSY_STATUS = 1;
    protected static final int PRINTER_OUT_OF_PAPER_STATUS = 2;
    protected static final int PRINTER_TOO_HOT_STATUS = 8;
    protected static final int PRINTER_VOLTAGE_TOO_LOW = 9;

    protected static final char FONT_TAG_PREFIX = '#';
    protected static final char CENTERED_FONT_TAG = '!';
    protected static final char BOLD_FONT_TAG = '?';
    protected static final char HALF_FONT_TAG = 'h';
    protected static final char TALL_FONT_TAG = 't';
    protected static final char INVERTED_TAG = 'n';
    protected static final char PICTURE_TAG = 'p';
    protected static final char BARCODE_TAG = 'c';

    protected static final int FONT_SIZE_REGULAR = 24;
    protected static final int FONT_SIZE_LARGE = 30;
    protected static final int FONT_SIZE_DEFAULT = 30;

    protected static final float FONT_WEIGHT_REGULAR = 300; //todo: these values have to be corrected
    protected static final float FONT_WEIGHT_BOLD = 600;

    protected static final int NUMBER_OF_CHARACTERS_PER_LINE_HALF_FONT_DEFAULT = 48;
    protected static final int NUMBER_OF_CHARACTERS_PER_LINE_REGULAR_HALF_FONT = 60;
    protected static final int NUMBER_OF_CHARACTERS_PER_LINE_LARGE_HALF_FONT = 48;
    protected static final int NUMBER_OF_CHARACTERS_PER_LINE_REGULAR_FONT = 30;
    protected static final int NUMBER_OF_CHARACTERS_PER_LINE_LARGE_FONT = 24;
    protected static final int NUMBER_OF_CHARACTERS_PER_LINE_DEFAULT = 24;

    private Context appContext = null;
    public static IDAL iDal;
    public static PaxGLPage paxGLPage;
    public static IPrinter printer;

    abstract public int print(String text);

    public Printer(Context appContext) {
        try {
            this.appContext = appContext;
            iDal = NeptuneLiteUser.getInstance().getDal(appContext);
            paxGLPage = PaxGLPage.getInstance(appContext);
            printer = iDal.getPrinter();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String printerTest() {
        String test = "LINE line 1 regular\n";
        test += "LINE line 2 regular\n";
        test += "LINE line 3 regular\n";
        test += "#!Normal centered line\n\n";
        test += "#?Bold line\n\n";
        test += "#?#!Bold centered line\n\n";
        test += "#hHalf line\n\n";
        test += "#h#?Half bold line\n\n";
        test += "#h#?#!Half bold centered line\n\n";
        test += "#tLINE line tall\n\n";
        test += "#t#?LINE line tall bold\n\n";
        test += "#t#!LINE line tall centered\n\n";
        test += "#t#!#?LINE line tall centered bold\n\n";
        test += "#nInverted line\n\n";
        test += "#n#!Inverted centered line\n\n";
        test += "#t#nTall inverted line\n\n";
        test += "#c1234567890\n\n";
        test += "Polish chars ąęśćżźńó ŚĆŻŹŃÓĄĘ\n\n";
        return test;
    }

    public int adjustAndPrintLines(PageBuilder pb, String text, int fontSize, IPage.EAlign defaultAlign) {
        String emptyRow = " ";
        IPage.EAlign align = defaultAlign;
        if (text.length() == 0) {
            Log.i(TAG, "text is empty");
            return -1;
        }
        String line = "";
        String remainText = "";
        int NLidx = 0;
        for (int i = 0; i < text.length(); i++) {
            remainText = text.substring(i);
            NLidx = remainText.indexOf("\n");
            if (NLidx != -1) {
                line = remainText.substring(0, NLidx);
                if (align == IPage.EAlign.CENTER)
                    line = line.trim();
                if (line.isEmpty()) {
                    pb.addTextLine(emptyRow, fontSize, align);
                } else
                    pb.addTextLine(line, fontSize, align);
                i += NLidx;
            }
        }
        return 0;
    }

    public class ValueCountPair {
        private int value;
        private int count;
        public ValueCountPair(int value, int count) {
            this.value = value;
            this.count = count;
        }
        public void incrementCount() {
            this.count++;
        }
    }

    protected int getCharsPerLine(String text) {
        List<ValueCountPair> valueCountPairList = new ArrayList<>();
        String split[] = text.split("\\n");
        if (split.length == 0) {
            return NUMBER_OF_CHARACTERS_PER_LINE_DEFAULT;
        }
        int len = 0;
        boolean found = false;

        try {
            for (int i = 0; i < split.length; i++) {
                len = split[i].length();
                if (len == 0 || split[i].indexOf("#h") != -1 || split[i].indexOf("#t") != -1 || split[i].indexOf("#p") != -1 || split[i].indexOf("#c") != -1)
                    continue;
                found = false;
                if (i == 0)
                    valueCountPairList.add(new ValueCountPair(len, 1));
                else {
                    for (int j = 0; j < valueCountPairList.size(); j++) {
                        if (valueCountPairList.get(j).value == len) {
                            found = true;
                            valueCountPairList.get(j).incrementCount();
                            break;
                        }
                    }
                    if (!found)
                        valueCountPairList.add(new ValueCountPair(len, 1));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return NUMBER_OF_CHARACTERS_PER_LINE_DEFAULT;
        }

        if (valueCountPairList.size() == 0)
            return NUMBER_OF_CHARACTERS_PER_LINE_DEFAULT;
        Log.d(TAG, valueCountPairList.size() + " unique number of chars found in " + split.length + "lines");
        int maxCount = valueCountPairList.get(0).count;
        int maxCountIndex = 0;
        for (int i = 1; i < valueCountPairList.size(); i++) {
            if (valueCountPairList.get(i).count > maxCount) {
                maxCount = valueCountPairList.get(i).count;
                maxCountIndex = i;
            }
        }
        Log.d(TAG, "dominanting number of chars per line = " + valueCountPairList.get(maxCountIndex).value);
        Log.d(TAG, "supported number of chars per line = " + getSupportedNumberOfCharsPerLine(valueCountPairList.get(maxCountIndex).value));
        return getSupportedNumberOfCharsPerLine(valueCountPairList.get(maxCountIndex).value);
    }

    private IPage.EAlign getAlignFromTextFormat(String line, IPage.EAlign align) {
        char space = ' ';
        if (align != IPage.EAlign.CENTER && !line.isEmpty() && line.charAt(0) == space && line.charAt(line.length()-1) == space)
            return IPage.EAlign.CENTER;
        return align;
    }

    protected void processTagsAndPrintLine(String line, int fontSize, PageBuilder pb, IPage.EAlign defaultAlign) throws Exception {
        boolean isBold = false, isHalf = false, isTall = false, isInverted = false;
        boolean isCentered = false, isBarcode = false, isPicture = false;

        String finalLine = "";
        int offset = 0;
        IPage.EAlign align = defaultAlign;
        float fontWeight = FONT_WEIGHT_REGULAR;
        int style = IPage.ILine.IUnit.TEXT_STYLE_NORMAL;

        while (line.length() > (offset + 1) && (line.charAt(offset) == FONT_TAG_PREFIX)) {
            char tag = line.charAt(offset + 1);
            switch (tag) {
                case BOLD_FONT_TAG: {
                    Log.d(TAG, "bold");
                    isBold = true;
                    break;
                }
                case CENTERED_FONT_TAG: {
                    Log.d(TAG, "centered");
                    isCentered = true;
                    break;
                }
                case HALF_FONT_TAG: {
                    Log.d(TAG, "half");
                    isHalf = true;
                    break;
                }
                case TALL_FONT_TAG: {
                    Log.d(TAG, "tall");
                    isTall = true;
                    break;
                }
                case INVERTED_TAG: {
                    Log.d(TAG, "inverted");
                    isInverted = true;
                    break;
                }
                case PICTURE_TAG: {
                    Log.d(TAG, "picture");
                    isPicture = true;
                    break;
                }
                case BARCODE_TAG: {
                    Log.d(TAG, "barcode");
                    isBarcode = true;
                    break;
                }
            }
            offset += 2;
        }

        if (offset < line.length()) {
            finalLine = line.substring(offset);
        } else {
            finalLine = "";
        }


        if (isBold) {
            style = IPage.ILine.IUnit.TEXT_STYLE_BOLD;
            fontWeight = FONT_WEIGHT_BOLD;
        }

        if (isPicture) {
            String fileName = finalLine;
            String picturePath = Updt.getPicturesDirectory() + "/" + fileName + Updt.getPictureFileExt();
            File pictureFile = new File(picturePath);
            if (pictureFile.exists() == true && pictureFile.isFile() == true) {
                Log.d(TAG, "image file found at path: " + picturePath);
                Bitmap picture = BitmapFactory.decodeFile(picturePath);
                if (picture != null) {
                    pb.addBitmap(picture, IPage.EAlign.CENTER, fontSize);
                }
            } else {
                Log.d(TAG, "image file does not exist or is not a file at path: " + picturePath);
            }
            return;
        }
        if (isBarcode) {
            String bc_text = finalLine;
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bc_image = null;
            try {
                bc_image = barcodeEncoder.encodeBitmap(bc_text, BarcodeFormat.CODE_128, Updt.getBarcodeWidth(), Updt.getBarcodeHeight());
            } catch (WriterException e) {
                e.printStackTrace();
                throw e;
            }
            if (bc_image != null) {
                pb.addBitmap(bc_image, IPage.EAlign.CENTER, fontSize);
            }
            isCentered = true; // print barcode number under picture, centered
        }

        if (isCentered) {
            align = IPage.EAlign.CENTER;
        }


        if (isInverted) {
            pb.addStringInvert(finalLine, fontSize, getAlignFromTextFormat(finalLine, align));
        } else if (isTall) {
            pb.addTextLine(finalLine, fontSize, 1, 2, align, fontWeight);
        } else if (isHalf) {
            pb.addTextLine(finalLine, fontSize, 0.5f, 1, align, fontWeight);
        } else {
            pb.addTextLine(finalLine, fontSize, align, style);
        }

    }

    protected static int printBitmap(final Bitmap bitmap) throws Exception {
        printer.init();
        printer.setGray(4);
        //printer.fontSet(com.pax.dal.entity.EFontTypeAscii.FONT_16_32, com.pax.dal.entity.EFontTypeExtCode.FONT_16_32);
        printer.printBitmap(bitmap);
        return startPrint(printer);
    }

    private static int startPrint(IPrinter printer) throws Exception {
        while (true) {
            int busyCount = 0;
            int ret = printer.start();
            if (ret == PRINTER_BUSY_STATUS) {
                SystemClock.sleep(100);
                busyCount++;
                if (busyCount > 20)
                    return PRINTER_BUSY_STATUS;
                continue;
            } else if (ret == PRINTER_OUT_OF_PAPER_STATUS) {
                Log.e(TAG, "Printer is Out of Paper");
                return ret;
            } else if (ret == PRINTER_TOO_HOT_STATUS) {
                Log.e(TAG, "Printer is too hot");
                return ret;
            } else if (ret == PRINTER_VOLTAGE_TOO_LOW) {
                Log.e(TAG, "Voltage is too low!");
                return ret;
            } else if (ret != PRINTER_OK_STATUS) {
                throw new Exception("unknown printer status");
            }
            return PRINTER_OK_STATUS;
        }
    }

    public int getFontSize(int charsPerLine) {
        switch (charsPerLine) {
            case (NUMBER_OF_CHARACTERS_PER_LINE_REGULAR_FONT) :
                return FONT_SIZE_REGULAR;
            case (NUMBER_OF_CHARACTERS_PER_LINE_LARGE_FONT) :
                return FONT_SIZE_LARGE;
            default:
                return FONT_SIZE_DEFAULT;
        }
    }

    public int getNumberofHalfCharsPerLine(int fontSize){
        switch (fontSize) {
            case (FONT_SIZE_REGULAR) :
                return NUMBER_OF_CHARACTERS_PER_LINE_REGULAR_HALF_FONT;
            case (FONT_SIZE_LARGE) :
                return NUMBER_OF_CHARACTERS_PER_LINE_LARGE_HALF_FONT;
            default:
                return NUMBER_OF_CHARACTERS_PER_LINE_HALF_FONT_DEFAULT;
        }
    }

    public int getSupportedNumberOfCharsPerLine(int number) {
        return (number < 27) ? NUMBER_OF_CHARACTERS_PER_LINE_LARGE_FONT : NUMBER_OF_CHARACTERS_PER_LINE_REGULAR_FONT;
    }

}
