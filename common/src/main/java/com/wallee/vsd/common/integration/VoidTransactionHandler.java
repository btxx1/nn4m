package com.wallee.vsd.common.integration;

import com.wallee.android.till.sdk.data.Receipt;
import com.wallee.android.till.sdk.data.ResultCode;
import com.wallee.android.till.sdk.data.State;
import com.wallee.android.till.sdk.data.TransactionVoid;
import com.wallee.android.till.sdk.data.TransactionVoidResponse;
import com.wallee.vsd.common.integration.data.DataUtils;
import com.wallee.vsd.common.integration.xml.CancelReservationRequest;
import com.wallee.vsd.common.integration.xml.CancelReservationResponse;
import com.wallee.vsd.common.integration.xml.XmlUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class VoidTransactionHandler {
    private final NetClient client = new NetClient();

    TransactionVoidResponse handle(TransactionVoid transaction) {
        if (client.connectWithServer()) {
            try {
                CancelReservationRequest xmlRequest = createXmlRequest(transaction);
                List<Receipt> receipts = new ArrayList<>();
                TransactionVoidResponse response = null;

                client.send(XmlUtils.toXml(xmlRequest));
                while (response == null) {
                    String xmlResponse = client.receive();
                    if (xmlResponse.isEmpty()) {
                        response = createErrorResponse(transaction, "Internal error - empty response");
                    } else if (XmlUtils.shouldConfirm(xmlResponse)) {
                        client.send(XmlUtils.CONFIRMATION);
                    } else if (XmlUtils.isPrinterNotification(xmlResponse)) {
                        Receipt receipt = DataUtils.printerNotificationToReceipt(XmlUtils.toPrinterNotification(xmlResponse));
                        if (receipt != null) {
                            receipts.add(receipt);
                        }
                    } else if (XmlUtils.isErrorNotification(xmlResponse)) {
                        response = createErrorResponse(transaction, XmlUtils.toErrorNotification(xmlResponse).errorText);
                    } else if (XmlUtils.isCancelReservationResponse(xmlResponse)) {
                        response = createResponse(transaction, receipts, XmlUtils.toCancelReservationResponse(xmlResponse));
                    }
                }

                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return createErrorResponse(transaction, e.getMessage());
            } finally {
                client.disConnectWithServer();
            }
        } else {
            return createErrorResponse(transaction, "Failed to connect to VPJ");
        }
    }

    private CancelReservationRequest createXmlRequest(TransactionVoid transaction) {
        return new CancelReservationRequest(
                DataUtils.POS_ID,
                transaction.getAcquirerId(),
                transaction.getReserveReference().toString()
        );
    }

    private TransactionVoidResponse createResponse(TransactionVoid transaction, List<Receipt> receipts, CancelReservationResponse response) {
        return new TransactionVoidResponse(
                transaction,
                DataUtils.authResultToState(response.ep2AuthResult),
                DataUtils.authResultToResultCode(response.ep2AuthResult),
                response.ep2TrmId,
                DataUtils.stringToLong(response.ep2TrxSeqCnt),
                response.transactionTime,
                receipts
        );
    }

    private TransactionVoidResponse createErrorResponse(TransactionVoid transaction, String error) {
        return new TransactionVoidResponse(
                transaction,
                State.FAILED,
                new ResultCode(
                        "error",
                        error
                ),
                null,
                null,
                null,
                Collections.emptyList()
        );
    }
}
