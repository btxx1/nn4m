package com.wallee.vsd.common.vsd;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.PowerManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.JavascriptInterface;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Space;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;


import com.wallee.vsd.common.JSReadyListener;
import com.wallee.vsd.common.PartialRegexInputFilter;
import com.wallee.vsd.common.R;
import com.wallee.vsd.common.models.IOReader;
import com.wallee.vsd.common.models.HardwareSpecification;
import com.wallee.vsd.common.models.vsd.MiscIface;
import com.wallee.vsd.common.models.vsd.XGIface;
import com.wallee.vsd.common.ui.IconMenuAdapter;
import com.wallee.vsd.common.ui.IconMenuView;
import com.wallee.vsd.common.ui.InfoScreenAdapter;
import com.wallee.vsd.common.ui.InfoScreenView;
import com.wallee.vsd.common.ui.KeyboardListener;
import com.wallee.vsd.common.ui.KeyboardView;
import com.wallee.vsd.common.ui.MenuAdapter;
import com.wallee.vsd.common.ui.MenuItem;

import net.glxn.qrgen.android.QRCode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;

import static android.content.Context.POWER_SERVICE;

public class XG implements XGIface, IOReader, JSReadyListener {
    public static final int ICON_MENU_COLUMNS = 3;
    private static final long WAKELOCK_TIMEOUT = 130 * 1000;
    public static String TAG = "XG";

    public static final String TAG_TYPE = "typ";
    public static final String TAG_COLUMN = "col";
    public static final String TAG_COLUMN_EXT = "cole";
    public static final String TAG_ROW = "row";
    public static final String TAG_ICON = "ico";
    public static final String TAG_LABEL = "lbl";
    public static final String TAG_TOKEN = "tok";
    public static final String TAG_FONT = "fnt";
    public static final String TAG_SIZE = "size";
    public static final String TAG_BOLD = "bold";
    public static final String TAG_ETOKEN = "etok";
    public static final String TAG_ELEMS = "elems";
    public static final String TAG_BUTTON_TEXTSIZE = "btnTextSize";
    public static final String TAG_BUTTON_TEXTSIZE_SMALLER = "smaller";
    public static final String TAG_BACKGROUND_COLOR = "bg";
    public static final String TAG_SUPPORT_COLOR = "sc";
    public static final String TAG_FOREGROUND_COLOR = "fg";
    public static final String TAG_VALIDATOR_REGEX = "re";
    public static final String TAG_UI_MODE = "mod";
    public static final String TAG_TOP = "top";
    public static final String TAG_BOTTOM = "btm";
    public static final String TAG_ALIGNMENT = "aln";
    public static final String TAG_ALTERNATIVE_LABEL = "alt";
    public static final String TAG_RC = "_RC";
    public static final String TAG_CONNECTIVITY = "_CONN";
    public static final String TAG_CONNECTIVITY_STATUS = "status";
    public static final String TAG_DETECT_RES = "detectRes";
    public static final String TAG_ICC_DETECT_RES = "iccDetectRes";
    public static final String TAG_SWIPE_RES = "swipeRes";
    public static final String TAG_RM = "_MSG";
    public static final String TAG_FD = "fd";
    public static final String TAG_PORT = "port";
    public static final String TAG_ADDR = "addr";
    public static final String TAG_VALUE = "val";
    public static final String TAG_DATA = "dat";
    public static final String TAG_CANCEL_ACTION = "cancelAction";
    public static final String TAG_KERNEL_ID = "kernelID";
    public static final String TAG_SELECTED_AID = "selectedAid";
    public static final String TAG_CVM_RESULT = "cvmR";
    public static final String TAG_READ_DESCRIPTORS = "readDescriptors";
    public static final String TAG_WRITE_DESCRIPTORS = "writeDescriptors";
    public static final String TAG_EXCEPT_DESCRIPTORS = "exceptDescriptors";
    public static final String TAG_PATH = "path";
    public static final String TAG_AC_TYPE = "acType";
    public static final String TAG_ERROR_TEXT = "errorText";
    public static final String TAG_PASSWORD = "pass";
    public static final String TAG_HINT = "hint";
    public static final String TAG_KB_TYPE = "kbType";
    public static final String TAG_QRCODE = "qrc";

    public static final String TAG_MENU = "menu";
    public static final String TAG_CTLS = "ctls";

    public static final String UI_LABEL = "LBL";
    public static final String UI_ICON = "ICO";
    public static final String UI_EDITOR = "ED";
    public static final String UI_MENU = "MENU";
    public static final String UI_ICOMENU = "ICOMENU";
    public static final String UI_SPACE = "SPC";
    public static final String UI_BUTTON = "BTN";
    public static final String UI_ROW_ATTR = "RAT";
    public static final String UI_KEYBOARD_TYPE_NUMERIC = "NKB";
    public static final String UI_KEYBOARD_TYPE_ALPHANUMERIC = "AKB";
    public static final String UI_KEYBOARD_TYPE_PIN = "PKB";
    public static final String UI_EDITOR_TYPE_AMOUNT = "EDAM";
    public static final String UI_EDITOR_TYPE_PASSWORD = "PSED";
    public static final String UI_EDITOR_TYPE_PIN = "PNED";
    public static final String UI_MENU_SEPARATOR = "MIS";
    public static final String UI_MENU_RADIO = "MIR";
    public static final String UI_INFO_SCREEN = "INFOSCREEN";
    public static final String TAG_EXPONENT = "exp";

    public static final String UI_MODE_WAIT = "WAIT";
    public static final String UI_MODE_DEFAULT = "DEF";

    public static final String UI_FONT_SIZE_SMALL = "small";
    public static final String UI_FONT_SIZE_XSMALL = "xsmall";
    public static final String UI_FONT_SIZE_LARGE = "large";
    public static final String UI_FONT_SIZE_XLARGE = "xlarge";


    public static final int XG_SOCKET = TcpSocket.MAX_SOCKETS + 1000;

    public static final int TOKEN_CANCEL = 104;
    public static final int TOKEN_CLOSE = 115;
    public static final int TOKEN_START_TRANSACTION_COMPLETED = 200;
    public static final int XG_TOKEN_PIN_BEFORE_EDIT = 201;
    public static final int XG_TOKEN_PIN_ENTERED = 202;

    public static final int XG_TOKEN_F1 = 1001;
    public static final int PREFERRED_IMAGE_ROW_HEIGHT_PERCENT = 80;
    public static final String TAG_MIN_HEIGHT = "mnH";

    public static final int LAYOUT_MARGIN = 6;

    public static final int maxEditAmountDigits = 10;

    PowerManager.WakeLock wakeLock;

    private static MiscIface misc;
    private static boolean isActivityPaused = false;

    static private AppCompatActivity mainActivity = null;
    private HardwareSpecification hardwareSpecification;

    public static XG xgInstance = null;
    static long cleanupDisplayTimeMs = 0;
    static Semaphore displayEmpty = new Semaphore(1), displayReady = new Semaphore(1);

    private String displayContentStr = null;
    private boolean isWaitingForJsReady = false;

    static class GuiTokenizer {
        private int currentScreenToken;
        private int nextScreenToken;
        static private long ACTION_AUTOENABLE_DELAY = 2000;
        private long lastActionTs;

        GuiTokenizer() {
            currentScreenToken = 1000;
            lastActionTs = 0;
        }

        boolean checkTokenValidity(int elemToken) {
            /*if (elemToken < currentScreenToken && lastActionTs + ACTION_AUTOENABLE_DELAY > System.currentTimeMillis()) {
                Log.d(TAG, "ignoring action on AGED gui elem; token: " + elemToken + ", screenSectionToken: " + currentScreenToken);
                return false;
            } else {
                ++currentScreenToken; // locking current section
                lastActionTs = System.currentTimeMillis();
                return true;
            }*/
            return true;
        }

        public void renderedActiveControl() {
            nextScreenToken = currentScreenToken + 1;
        }

        public int getNextScreenToken() {
            return nextScreenToken;
        }

        private void commitScreen() {
            if (currentScreenToken < nextScreenToken) {
                Log.d(TAG, "switching to new gui screen section: " + nextScreenToken);
                currentScreenToken = nextScreenToken;
                lastActionTs = System.currentTimeMillis();
            }
        }
    }

    public static GuiTokenizer topTokenizer = new GuiTokenizer();
    public static GuiTokenizer btmTokenizer = new GuiTokenizer();
    public static GuiTokenizer focusTokenizer = null;

    private class EditorContext {
        public EditorType editorType;
        public EditText editor;
        public JSONObject editorDef;
        EditorContext() {
            this.editorType = EditorType.UNKNOWN;
            this.editor = null;
            this.editorDef = null;
        }
    }

    static EditorContext topEditorContext = null;
    static EditorContext btmEditorContext = null;

    static private IconMenuView menuThatNeedsScrolling = null;
    static private Boolean isBackgroundMode = true;

    public static void setActivityPaused(boolean isActivityPaused) {
        XG.isActivityPaused = isActivityPaused;
    }

    public static void requestFocus() {
        final AppCompatActivity activity = getMainActivity();
        if (!activity.hasWindowFocus()) {
            activity.sendBroadcast(new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
        }
    }

    public static boolean activityPaused() {
        return XG.isActivityPaused;
    }

    public static void handleCorrectPin() {
        EditText defaultEditor = XG.getDefaultEditor();

        if (defaultEditor != null) {
            int length = defaultEditor.getText().length();
            if (length > 0) {
                defaultEditor.getText().delete(length - 1, length);
            }
        }
    }

    public static void handlePinDigit() {
        EditText defaultEditor = XG.getDefaultEditor();
        if (defaultEditor != null) {
            defaultEditor.append("*");
        }
    }

    public static void handlePinDigits(int numberOfDigits) {
        EditText defaultEditor = XG.getDefaultEditor();
        if (defaultEditor != null) {
            defaultEditor.setText("");
            if (numberOfDigits > 0) {
                char[] chars = new char[numberOfDigits];
                Arrays.fill(chars, '*');
                defaultEditor.append(new String(chars));
            }
        }
    }

    public int getDescriptor() {
        return XG.XG_SOCKET;
    }

    public boolean isEventAvailable() {
        return !eventQueue.isEmpty();
    }

    public void clearEvent() {
        eventQueue.poll();
    }

    static public AppCompatActivity getMainActivity() {
        return mainActivity;
    }

    static Thread displayCleanupThread = new Thread(new Runnable() {
        public void run() {
            while (true) {
                try {
                    displayReady.acquire();
                    Log.d(TAG, "cleanupThread: cleaning screen in " + (cleanupDisplayTimeMs - System.currentTimeMillis()) + " ms.");
                    while (cleanupDisplayTimeMs > 0 && System.currentTimeMillis() < cleanupDisplayTimeMs) {
                        Thread.sleep(cleanupDisplayTimeMs - System.currentTimeMillis());
                    }
                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            final TableLayout dspTop = (TableLayout) mainActivity.findViewById(R.id.dspTop);
                            if (cleanupDisplayTimeMs > 0) {
                                dspTop.removeAllViews();
                            }
                            cleanupDisplayTimeMs = -1;
                            displayEmpty.release();
                        }
                    });
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    });

    static {
        try {
            displayReady.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        displayCleanupThread.start();
    }

    private static final BlockingQueue<String> eventQueue = new ArrayBlockingQueue<String>(10);
    private PowerManager powerManager;
    private  Class<?> mainActivityClass;
    private boolean isProcessingUIEvent = false;
    private String pendingUIEvent = null;

    public XG(AppCompatActivity mainActivity, MiscIface misc, Class<?> mainActivityClass,
              HardwareSpecification hardwareSpecification) {
        XG.xgInstance = this;
        XG.mainActivity = mainActivity;

        this.misc = misc;
        this.mainActivityClass = mainActivityClass;
        this.hardwareSpecification = hardwareSpecification;
        powerManager = (PowerManager) mainActivity.getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "VSD:XG");
        misc.addIOReaderMapping(this);
        misc.addJSReadyListener(this);
    }

    public void wakeUp() {
        wakeLock.acquire(XG.WAKELOCK_TIMEOUT);
    }

    static void synchronizeMinimalDisplayTime(int minimalDisplayTimeMs) {
        try {
            displayEmpty.acquire();
            if (minimalDisplayTimeMs <= 0) {
                XG.cleanupDisplayTimeMs = -1;
            } else {
                XG.cleanupDisplayTimeMs = System.currentTimeMillis() + minimalDisplayTimeMs;
            }
            displayReady.release();
        } catch (InterruptedException e) {
            Log.w(TAG, "synchronizeMinimalDisplayTime interrupted", e);
        }
    }

    private static int getGravityForElement(JSONObject elem) throws JSONException {
        int gravity = 0;
        if (elem.has(TAG_ALIGNMENT)) {
            for (int i = 0; i < elem.getString(TAG_ALIGNMENT).length(); ++i) {
                switch (elem.getString(TAG_ALIGNMENT).charAt(i)) {
                    case 'R':
                        gravity |= Gravity.RIGHT;
                        break;
                    case 'L':
                        gravity |= Gravity.LEFT;
                        break;
                    case 'T':
                        gravity |= Gravity.TOP;
                        break;
                    case 'B':
                        gravity |= Gravity.BOTTOM;
                        break;
                }
            }
        }
        if (gravity == 0) {
            gravity = Gravity.CENTER;
        }
        return gravity;
    }

    private static int getRowHeightForElement(JSONObject elem) throws JSONException {
        if (!elem.has(TAG_MIN_HEIGHT))
            return 0;

        String value = elem.getString(TAG_MIN_HEIGHT);

        int len = value.length();
        if (len == 0) {
            Log.e(TAG, TAG_MIN_HEIGHT + " may not be empty");
            return 0;
        }

        Boolean isPercent = false;

        if (value.charAt(len-1) == '%') {
            value = value.substring(0, len - 1);
            isPercent = true;
        }

        int numValue = Integer.parseInt(value);

        if (isPercent == false)
            return numValue;

        //minimal height becomes the final only if exceeds the preferred value
        numValue = (numValue > PREFERRED_IMAGE_ROW_HEIGHT_PERCENT) ? numValue : PREFERRED_IMAGE_ROW_HEIGHT_PERCENT;
        return Resources.getSystem().getDisplayMetrics().heightPixels * numValue / 100;
    }

    private static View getAlternativeLabel(JSONObject elem) throws JSONException {
        TextView view = new TextView(mainActivity);
        view.setText(elem.getString(TAG_ALTERNATIVE_LABEL));
        float size = mainActivity.getResources().getDimension(R.dimen.textsize);
        if (elem.has(TAG_FONT)) {
            JSONObject fontDef = elem.getJSONObject(TAG_FONT);
            if (UI_FONT_SIZE_LARGE.equals(fontDef.optString(TAG_SIZE, null))) {
                size = 2.0f * size;
                view.setTypeface(null, Typeface.BOLD);
            }
            setViewTextColorFromElem(view, fontDef, null);
        }
        view.setTextSize(size);
        view.setSingleLine(false);
        view.setHorizontallyScrolling(false);
        view.setGravity(getGravityForElement(elem));
        return view;
    }

    enum EditorType {UNKNOWN, NUMERIC, ALPHANUMERIC, PASSWORD, PIN};

    private XG.EditorType getSectionEditorType(JSONArray ea) {
        try {
            for (int elemn = 0; elemn < ea.length(); ++elemn) {
                JSONObject elem = null;
                elem = ea.getJSONObject(elemn);
                switch (elem.getString(TAG_TYPE)) {
                    case UI_KEYBOARD_TYPE_NUMERIC:
                        return EditorType.NUMERIC;
                    case UI_KEYBOARD_TYPE_ALPHANUMERIC:
                        return EditorType.ALPHANUMERIC;
                    case UI_EDITOR_TYPE_PASSWORD:
                        return EditorType.PASSWORD;
                    case UI_KEYBOARD_TYPE_PIN:
                        return EditorType.PIN;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return EditorType.UNKNOWN;
    }

    private XG.EditorType getDefaultEditorType(JSONObject def) {
        XG.EditorType res = EditorType.UNKNOWN;
        try {
            String tags[] = {TAG_TOP, TAG_BOTTOM};
            for (String tag : tags) {
                if (def.has(tag)) {
                    res = getSectionEditorType(def.getJSONArray(tag));
                    if (res != EditorType.UNKNOWN) {
                        return res;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return res;
    }

    static public EditText getDefaultEditor() {
        if (topEditorContext != null) {
            return topEditorContext.editor;
        } else if (btmEditorContext != null) {
            return btmEditorContext.editor;
        }
        return null;
    }

    private EditorContext renderArray(TableLayout dsp, JSONArray array) throws JSONException {
        EditorContext ectx = new EditorContext();
        int pcol = -1, prow = -1;
        TableRow row = new TableRow(mainActivity);
        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        lp.setMarginStart(LAYOUT_MARGIN);
        lp.setMarginEnd(LAYOUT_MARGIN);
        row.setLayoutParams(lp);
        String viewDebugString = "|";
        for (int elemn = 0; elemn < array.length(); ++elemn) {
            JSONObject elem = array.getJSONObject(elemn);
            View view = null;
            int expandRowTo = 0;
            int ecol = -1, ecol1 = -1, ecol2 = -1;
            if (elem.has(TAG_COLUMN)) {
                ecol = elem.getInt(TAG_COLUMN);
            } else {
                Log.w(TAG, "Invalid or absent col/col1/col2");
                break;
            }

            if (elem.has(TAG_COLUMN_EXT)) {
                ecol1 = ecol;
                ecol = -1;
                ecol2 = elem.getInt(TAG_COLUMN_EXT);
            }

            int erow = elem.getInt(TAG_ROW);
            if (prow > erow || prow == erow && pcol >= ecol) {
                Log.w(TAG, "Elements have to be in order (regarding col,row attributes)");
            }
            if (erow > prow) {
                dsp.addView(row);
                row = new TableRow(mainActivity);
                lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                lp.setMarginStart(LAYOUT_MARGIN);
                lp.setMarginEnd(LAYOUT_MARGIN);
                row.setLayoutParams(lp);
            }
            prow = erow; pcol = ecol;
            switch (elem.getString(TAG_TYPE)) {
                case UI_LABEL:
                    viewDebugString += elem.optString(TAG_LABEL) + "|";
                    view = renderLabel(elem);
                    if ((elem.has(TAG_ICON) || elem.has(TAG_LABEL)) && getGravityForElement(elem) == Gravity.CENTER) {
                        expandRowTo = getRowHeightForElement(elem);
                    }
                    break;
                case UI_INFO_SCREEN :
                    viewDebugString += elem.optString(TAG_LABEL) + "|";
                    view = renderInfoScreen(elem);
                    break;
                case UI_EDITOR:
                    viewDebugString += "____|";
                    EditorType editorType = EditorType.ALPHANUMERIC;
                    if (elem.has(TAG_KB_TYPE)) {
                        editorType = (elem.getString(TAG_KB_TYPE).equals(UI_KEYBOARD_TYPE_NUMERIC)) ?
                                EditorType.NUMERIC : EditorType.ALPHANUMERIC;
                    } else if (elem.getString(TAG_VALIDATOR_REGEX).startsWith("[0-9]")) {
                        editorType = EditorType.NUMERIC;
                    }

                    view = renderEditor(elem, editorType);
                    ectx.editorType = editorType;
                    ectx.editorDef = elem;
                    ectx.editor = (EditText) view.findViewById(R.id.edit);
                    break;
                case UI_EDITOR_TYPE_AMOUNT:
                    viewDebugString += "__._|";
                    view = renderAmountEditor(elem);
                    ectx.editorType = EditorType.NUMERIC;
                    ectx.editorDef = elem;
                    ectx.editor = (EditText) view.findViewById(R.id.edit);
                    focusTokenizer.renderedActiveControl();
                    break;
                case UI_KEYBOARD_TYPE_NUMERIC:
                    viewDebugString += " [0..9] |";
                    view = renderNumericKeyboard(elem);
                    ectx.editorDef = elem;
                    ectx.editorType = EditorType.NUMERIC;
                    focusTokenizer.renderedActiveControl();
                    break;
                case UI_KEYBOARD_TYPE_ALPHANUMERIC:
                    viewDebugString += " [a..z0..9] |";
                    view = renderAlnumKeyboard(elem);
                    ectx.editorType = EditorType.ALPHANUMERIC;
                    focusTokenizer.renderedActiveControl();
                    break;
                case UI_EDITOR_TYPE_PASSWORD:
                    viewDebugString += " ***0|";
                    view = renderEditor(elem, EditorType.PASSWORD);
                    ectx.editorType = EditorType.PASSWORD;
                    ectx.editorDef = elem;
                    ectx.editor = (EditText) view.findViewById(R.id.edit);
                    break;
                case UI_KEYBOARD_TYPE_PIN:
                    final KeyboardView numKbd = mainActivity.findViewById(R.id.numKbd);
                    numKbd.resetKeyboardListener();
                    numKbd.setVisibility(View.VISIBLE);
                    focusTokenizer.renderedActiveControl();
                    break;
                case UI_EDITOR_TYPE_PIN:
                    viewDebugString += " ****|";
                    view = renderEditor(elem, EditorType.PIN);
                    ectx.editorType = EditorType.PIN;
                    ectx.editorDef = elem;
                    ectx.editor = (EditText) view.findViewById(R.id.edit);
                    break;
                case UI_MENU:
                    viewDebugString += "☰|";
                    view = renderMenu(elem);
                    break;
                case UI_ICOMENU:
                    viewDebugString += "☰|";
                    view = renderIconMenu(elem, ecol2);
                    break;
                case UI_SPACE:
                    viewDebugString += "~|";
                    view = renderSpace(elem);
                    break;
                case UI_BUTTON:
                    viewDebugString += String.format(" [%s] |", elem.optString(TAG_LABEL));
                    view = renderButton(elem);
                    break;
                case UI_ROW_ATTR:
                    viewDebugString += String.format(" @ |");
                    view = renderRowAttr(row, elem);
                    break;

                default:
                    Log.w(TAG, "Unsupported elem type: " + elem.getString(TAG_TYPE));
                    break;
            }

            //TODO: without this, the application crashes with:
            //    java.lang.ArithmeticException: divide by zero
            //        at android.widget.TableLayout.mutateColumnsWidth(TableLayout.java:584)
            // How to make it smarter?
            if (view == null && array.length() == 1) {
                view = renderSpace(new JSONObject());
            }

            if (view != null) {
                TableRow.LayoutParams params = new TableRow.LayoutParams(Math.max(ecol, ecol1));
                params.width = TableRow.LayoutParams.WRAP_CONTENT;
                params.setMarginStart(LAYOUT_MARGIN);
                params.setMarginEnd(LAYOUT_MARGIN);
                if (expandRowTo != 0) {
                    params.height = expandRowTo;
                    params.gravity = Gravity.CENTER;
                }
                if (ecol == -1 && ecol1 != -1 && ecol2 != -1) {
                    params.span = ecol2 - ecol1 + 1;
                }
                if (UI_LABEL.equals(elem.getString(TAG_TYPE)) && elem.has(TAG_ICON)) {
                    params.gravity = Gravity.CENTER;
                }
                view.setLayoutParams(params);
                row.addView(view);
            }
        }
        dsp.addView(row);
        if (ectx.editor == null || ectx.editorType == EditorType.UNKNOWN) {
            return null;
        } else {
            return ectx;
        }
    }

    private View renderRowAttr(TableRow row, JSONObject elem) {
        setViewBgColorFromElem(row, elem);
        return null;
    }

    static private View renderSpace(final JSONObject elem) {
        Space view = new Space(mainActivity);
        view.setMinimumHeight(40);
        setElemSize(view, elem);
        setViewBgColorFromElem(view, elem);
        return view;
    }

    @Override
    public synchronized void onJSReady() {
        Log.d(TAG, "XG::onJSReady: "+this.pendingUIEvent);
        isProcessingUIEvent = false;
        if(this.pendingUIEvent != null) {
            String copyEvent = ""+this.pendingUIEvent;
            this.pendingUIEvent = null;
            postUIEvent(copyEvent);
        } else if (isWaitingForJsReady) {
            isWaitingForJsReady = false;
            notify();
        }
    }

    static private View renderButton(final JSONObject elem) throws JSONException {
        focusTokenizer.renderedActiveControl();
        final GuiTokenizer myTokenizer = focusTokenizer;
        final int myToken = myTokenizer.getNextScreenToken();
        View bt_container = LayoutInflater.from(mainActivity).inflate(R.layout.vsd_button, null);
        Button bt = (Button) bt_container.findViewById(R.id.button);
        setViewBgTintColorFromElem(bt, elem);
        setViewTextColorFromElem(bt, elem, Color.BLACK);
        bt.setText(elem.getString(TAG_LABEL));
        setButtonTextSizeFromElem(bt, elem);
        bt.setGravity(getGravityForElement(elem));
        setElemSize(bt, elem);

        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!myTokenizer.checkTokenValidity(myToken)) {
                    return;
                }
                try {
                    final EditorContext editorContext = topEditorContext != null ? topEditorContext : btmEditorContext;
                    if (editorContext != null) {
                        XG.postUIEvent(String.format("{\"%s\":0,\"%s\":%d,\"%s\":\"%s\",\"%s\":\"%s\",\"%s\":\"%s\"}",
                                TAG_RC,
                                TAG_TOKEN, elem.getInt(TAG_TOKEN),
                                TAG_ETOKEN, editorContext.editorDef.optString(TAG_ETOKEN, ""),
                                TAG_VALUE, editorContext.editor.getText().toString(),
                                TAG_DATA, elem.optString(TAG_DATA, "")));
                    } else {
                        XG.postUIEvent(String.format("{\"%s\":%d,\"%s\":\"%s\",\"%s\":\"%s\"}",
                                TAG_TOKEN, elem.getInt(TAG_TOKEN),
                                TAG_DATA, elem.optString(TAG_DATA, ""),
                                TAG_ETOKEN, elem.optString(TAG_ETOKEN, "")));
                    }
                } catch (JSONException e) {
                    Log.w(TAG, "failed to generate click button event: " + e.toString());
                }

            }
        });
        return bt_container;
    }

    static private void setElemFontProperties(TextView view, JSONObject elem) {
        float size = mainActivity.getResources().getDimension(R.dimen.textsize);
        try {
            if (elem.has(TAG_FONT)) {
                JSONObject fontDef = elem.getJSONObject(TAG_FONT);
                switch (fontDef.optString(TAG_SIZE, null)) {
                    case UI_FONT_SIZE_SMALL:
                        size = 0.8f * size;
                        break;
                    case UI_FONT_SIZE_XSMALL:
                        size = 0.5f * size;
                        break;
                    case UI_FONT_SIZE_LARGE:
                        size = 1.0f * size;
                        break;
                    case UI_FONT_SIZE_XLARGE:
                        size = 2.0f * size;
                        break;
                }
                if (fontDef.has(TAG_BOLD) && fontDef.getBoolean(TAG_BOLD) == true){
                    view.setTypeface(null, Typeface.BOLD);
                }
                setViewTextColorFromElem(view, fontDef, null);
            }
        } catch (JSONException e) {
        }
        Log.d(TAG, "Text. size: "+ size);
        view.setTextSize(size);
    }

    static private void setElemSize(View view, JSONObject elem) {
        if (!elem.has(TAG_MIN_HEIGHT)) {
            return;
        }
        try {
            String minH = elem.getString(TAG_MIN_HEIGHT);
            int h, scrHeight = -1;
            if(minH.endsWith("%")) {
                Point size = new Point();
                mainActivity.getWindowManager().getDefaultDisplay().getRealSize(size);
                scrHeight = size.y;
                h = scrHeight * Integer.valueOf(minH.substring(0, minH.length() - 1)) / 100;
            } else {
                h = Integer.valueOf(minH);
            }
            view.setMinimumHeight(h);
        } catch (JSONException e) {
        }
    }


    private View renderLabel(JSONObject elem) throws JSONException {
        if (elem.has(TAG_ICON)) {
            ImageView view = new ImageView(mainActivity);
            view.setAdjustViewBounds(true);
            if (getGravityForElement(elem) != Gravity.CENTER) {
                Log.w(TAG, "Aligning images is not supported");
            }
            AssetManager assetManager = mainActivity.getAssets();

            String iconPath = elem.getString(TAG_ICON);
            InputStream inputStream  = null;

            try  {
                if (misc.fileExists(Updt.getAppDirectory() + iconPath)) {
                    inputStream = new FileInputStream(Updt.getAppDirectory() + iconPath);
                } else {
                    inputStream = assetManager.open(iconPath);
                }


                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                view.setScaleType(ImageView.ScaleType.CENTER);
                view.setAdjustViewBounds(false);

                view.setImageBitmap(
                        Bitmap.createScaledBitmap(
                                bitmap,
                                (int) bitmap.getWidth(), (int) bitmap.getHeight(),
                                false
                        ));

            } catch (IOException ex) {
                Log.w(TAG, "displaying image failed - looking for alternative text", ex);
                if (elem.has(TAG_ALTERNATIVE_LABEL))
                    return getAlternativeLabel(elem);
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return view;
        } else if (elem.has(TAG_LABEL)) {
            TextView view = new TextView(mainActivity);
            view.setText(elem.getString(TAG_LABEL));
            setElemFontProperties(view, elem);
            view.setSingleLine(false);
            view.setHorizontallyScrolling(false);
            view.setGravity(getGravityForElement(elem));
            return view;
        } else if (elem.has(TAG_QRCODE)) {
            ImageView view = new ImageView(mainActivity);
            view.setAdjustViewBounds(true);
            if (getGravityForElement(elem) != Gravity.CENTER) {
                Log.w(TAG, "Aligning images is not supported");
            }

            try  {
                String qrCodeString = elem.getString(TAG_QRCODE);
                Bitmap bitmap = QRCode.from(qrCodeString).bitmap();
                view.setScaleType(ImageView.ScaleType.CENTER);
                view.setAdjustViewBounds(false);
                view.setImageBitmap(
                        Bitmap.createScaledBitmap(
                                bitmap,
                                250, 250,
                                false
                        ));

            } catch (Exception ex) {
                Log.w(TAG, "displaying qr code image failed ", ex);
            }

            return view;
        }
        return null;
    }





    private boolean verifyAmountString(String amount, int exponent) {
        if (exponent == 0) {
            String regNoExp = String.format("^[1-9][0-9]{1,%d}$", maxEditAmountDigits - 1);
            return amount.matches("^[0-9]$") || amount.matches(regNoExp);
        }

        String regex1 = String.format("^0\\.[0-9]{%d}$", exponent);
        String regex2 = String.format("^[1-9][0-9]{0,%d}\\.[0-9]{%d}$", maxEditAmountDigits - exponent - 1, exponent);
        return amount.matches(regex1) || amount.matches(regex2) ;
    }

    private View renderAmountEditor(JSONObject elem) throws JSONException {
        focusTokenizer.renderedActiveControl();
        final GuiTokenizer myTokenizer = focusTokenizer;
        final int myToken = myTokenizer.getNextScreenToken();

        final View editContainer = LayoutInflater.from(mainActivity).inflate(R.layout.vsd_amedit, null);
        final EditText editComponent = (EditText)  editContainer.findViewById(R.id.edit);
        editComponent.setInputType(InputType.TYPE_CLASS_NUMBER);
        final int token = elem.getInt(TAG_TOKEN);
        final String etoken = elem.optString(TAG_ETOKEN, "");
        final String data = elem.optString(TAG_DATA, "");
        setViewBgTintColorFromElem(editComponent, elem);
        setViewTextColorFromElem(editComponent, elem, null);
        int exp = 0;
        // check if currency exponent has been passed
        if (elem.has(TAG_EXPONENT)) {
            try {
                exp = elem.getInt(TAG_EXPONENT);
            } catch (JSONException e) {
                Log.e(TAG, "Invalid exponent: " + e.toString());
                exp = 0;
            }
        }
        
        final int exponent = exp;

        editComponent.setTextSize(mainActivity.getResources().getDimension(R.dimen.textsize));
        editComponent.setGravity(getGravityForElement(elem));
        setElemFontProperties(editComponent, elem);
        setElemSize(editComponent, elem);
        editComponent.setImeOptions(EditorInfo.IME_ACTION_GO);
        editComponent.requestFocus();
        String temp = "0";
        if (exponent > 0) {
            temp += ".";
            for (int i = 0; i < exponent; i++) {
                temp += "0";
            }
        }
        final String initAmount = temp;

        String val = elem.optString(TAG_VALUE);
        String amount = verifyAmountString(val, exponent) ? val : initAmount;
        editComponent.setText(amount);

        editComponent.setSelection(temp.length());

        InputFilter[] editFilters = editComponent.getFilters();
        InputFilter[] newFilters = new InputFilter[editFilters.length + 1];
        System.arraycopy(editFilters, 0, newFilters, 0, editFilters.length);

        int editorMaxLength = (exponent == 0) ? maxEditAmountDigits : (maxEditAmountDigits + 1);
        newFilters[editFilters.length] = new InputFilter.LengthFilter(editorMaxLength);
        editComponent.setFilters(newFilters);

        editComponent.addTextChangedListener(
            new TextWatcher() {
                String amountBefore = initAmount;

                @Override
                public void afterTextChanged(Editable s) {
                    editComponent.setSelection(editComponent.getText().length());
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                    editComponent.setSelection(editComponent.getText().length());
                }

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    if (!s.toString().equals(amountBefore)) {
                        String newAmount = setDot(s.toString(), exponent);
                        amountBefore = newAmount;
                        editComponent.setText(newAmount);
                        editComponent.setSelection(editComponent.getText().length());
                    }
                }

                private String setDot(String s, int currencyExponent) {
                    s = s.replace(".", "");
                    while (s.length() < currencyExponent + 1) {
                        s = "0" + s;
                    }
                    s = s.substring(0, s.length() - currencyExponent) + (currencyExponent > 0 ? "." : "") + s.substring(s.length() - currencyExponent);
                    // remove additional leading zeroes
                    while (true) {
                        if (s.length() == 1) {
                            break;
                        }
                        if (s.charAt(0) != '0' || (s.charAt(0) == '0' && s.charAt(1) == '.')) {
                            break;
                        }
                        if (s.charAt(0) == '0') {
                            s = s.substring(1);
                        }
                    }

                    return s;
                }
            }
        );

        editComponent.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (!myTokenizer.checkTokenValidity(myToken)) {
                    return false;
                }
                if (actionId == EditorInfo.IME_ACTION_PREVIOUS) {
                    Log.d(TAG, "cancelInput: actionId == EditorInfo.IME_ACTION_PREVIOUS");
                    cancelInput(v);
                    return true;
                } else if (actionId == EditorInfo.IME_ACTION_GO) {
                    commitInput(v, null, token, etoken, data);
                    return true;
                }
                return false;
            }
        });

        return editContainer;
    }

    protected static void setViewTextColorFromElem(TextView view, JSONObject elem, Integer defaultColor) throws JSONException {
        Integer color = defaultColor;
        if (elem.has(TAG_FOREGROUND_COLOR)) {
            try {
                color = Color.parseColor(elem.getString(TAG_FOREGROUND_COLOR));
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "Invalid fg color specification: " + elem.getString(TAG_FOREGROUND_COLOR));
            }
        }
        if (color != null) {
            view.setTextColor(color);
        }
    }

    protected static void setButtonTextSizeFromElem(TextView view, JSONObject elem) throws JSONException {
        float btnTextSize = mainActivity.getResources().getDimension(R.dimen.button_textsize);
        if (elem.has(TAG_BUTTON_TEXTSIZE)) {
            String textSizeExpr = elem.getString(TAG_BUTTON_TEXTSIZE);
            if(textSizeExpr != null) {
                if(textSizeExpr.equals(TAG_BUTTON_TEXTSIZE_SMALLER)) {
                    btnTextSize = mainActivity.getResources().getDimension(R.dimen.button_textsize_smaller);
                    Log.i(TAG, "Using button text size specified: " + elem.getString(TAG_BUTTON_TEXTSIZE)+"="+btnTextSize);
                }
                else {
                    Log.e(TAG, "Unrecognized button text size specified: " + elem.getString(TAG_BUTTON_TEXTSIZE));
                }
            }
        }
        view.setTextSize(btnTextSize);
    }

    private static void setViewBgColorFromElem(View view, JSONObject elem) {
        try {
            if (elem.has(TAG_BACKGROUND_COLOR)) {
                try {
                    view.setBackgroundColor(Color.parseColor(elem.getString(TAG_BACKGROUND_COLOR)));
                } catch (IllegalArgumentException e) {
                    Log.e(TAG, "Invalid bg color specification: " + elem.getString(TAG_BACKGROUND_COLOR));
                }
            }
        } catch( JSONException e ) {
        }
    }

    private static void setViewBgTintColorFromElem(View view, JSONObject elem) {
        try {
            if (elem.has(TAG_BACKGROUND_COLOR)) {
                try {
                    view.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(elem.getString(TAG_BACKGROUND_COLOR))));
                } catch (IllegalArgumentException e) {
                    Log.e(TAG, "Invalid bg color specification: " + elem.getString(TAG_BACKGROUND_COLOR));
                }
            }
        } catch( JSONException e ) {
        }
    }

    static private boolean isValidInput(TextView editor, String finalRe) {
        String value = editor.getText().toString();
        return finalRe == null || value.matches(finalRe);
    }

    private boolean commitInput(TextView editor, String finalRe, int token, String etoken, String data) {
        if(isValidInput(editor, finalRe)) {
            String value = editor.getText().toString();
            hideKeyboardFrom();
            editor.clearFocus();
            XG.postUIEvent(String.format("{\"%s\":0,\"%s\":%d,\"%s\":\"%s\",\"%s\":\"%s\",\"%s\":\"%s\"}",
                    TAG_RC, TAG_TOKEN, token, TAG_ETOKEN, etoken, TAG_VALUE, value, TAG_DATA, data));
            return true;
        } else {
            // invalid input
            misc.beep(750, 100);
            return false;
        }
    }

    private static Integer getInteger(String strNum) {
        if (strNum == null) {
            return null;
        }
        Integer i = null;
        try {
            i = Integer.parseInt(strNum);
        } catch (NumberFormatException nfe) {
            return null;
        }
        return i;
    }

    private void cancelInput(TextView editor) {
        Log.d(TAG, "cancelInput");
        hideKeyboardFrom();
        editor.clearFocus();
        int cancelToken = TOKEN_CANCEL;
        final EditorContext editorContext = topEditorContext != null ? topEditorContext : btmEditorContext;
        String etoken = "";
        String data = "";
        if (editorContext != null) {
            etoken = editorContext.editorDef.optString(TAG_ETOKEN, "");
            data = editorContext.editorDef.optString(TAG_DATA, "");
            String cancelActionStr = editorContext.editorDef.optString(TAG_CANCEL_ACTION, null);
            Integer cancelAction = getInteger(cancelActionStr);
            if(cancelAction != null) {
                if(cancelAction == TOKEN_CANCEL || cancelAction == TOKEN_CLOSE) {
                    cancelToken = cancelAction;
                }
                else {
                    Log.e(TAG, "Unknown "+TAG_CANCEL_ACTION+" specified: " + cancelActionStr);
                }
            }
        }

        XG.postUIEvent(String.format("{\"%s\":%d,\"%s\":\"%s\",\"%s\":\"%s\"}",
                TAG_TOKEN, cancelToken, TAG_DATA, data, TAG_ETOKEN, etoken));
    }


    private EditorContext getEditorContext() {
        final EditorContext editorContext = (topEditorContext != null && topEditorContext.editor != null) ?
                topEditorContext : btmEditorContext;

        if (editorContext != null && editorContext.editor != null) {
            editorContext.editor.requestFocus();
        }
        return editorContext;
    }

    private View renderEditor(JSONObject elem, EditorType defaultEditorType) throws JSONException {
        focusTokenizer.renderedActiveControl();
        final GuiTokenizer myTokenizer = focusTokenizer;
        final int myToken = myTokenizer.getNextScreenToken();

        View edContainer = LayoutInflater.from(mainActivity).inflate(R.layout.vsd_edit, null);
        EditText view = (EditText) edContainer.findViewById(R.id.edit);

        if (elem.has(TAG_HINT)) {
            view.setHint(elem.getString(TAG_HINT));
        }
        switch (defaultEditorType) {
            case NUMERIC:
                view.setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
            case PASSWORD:
                view.setInputType(InputType.TYPE_CLASS_NUMBER);
                view.setTransformationMethod(PasswordTransformationMethod.getInstance());
                break;
            case PIN:
            case ALPHANUMERIC:
            default:
                view.setInputType(InputType.TYPE_CLASS_TEXT);

                view.setOnTouchListener(new View.OnTouchListener() {
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        EditorContext editorContext = getEditorContext();
                        if (editorContext != null && editorContext.editor != null) {
                            InputMethodManager imm = (InputMethodManager) mainActivity.getSystemService(
                                    Context.INPUT_METHOD_SERVICE);

                            imm.showSoftInput(editorContext.editor, 0);
                        }
                        return true;
                    }
                } );

                break;
        }
        final int token = elem.has(TAG_TOKEN) ? elem.getInt(TAG_TOKEN) : -1;
        if (view != null) {
            int inputInvalidColor = Color.RED;
            setViewBgTintColorFromElem(view, elem);
            setViewTextColorFromElem(view, elem, Color.BLACK);
            Integer inputValidColor = Color.BLACK;
            if (elem.has(TAG_FOREGROUND_COLOR)) {
                try {
                    inputValidColor = Color.parseColor(elem.getString(TAG_FOREGROUND_COLOR));
                } catch (IllegalArgumentException e) {
                }
            }

            if (elem.has(TAG_SUPPORT_COLOR)) {
                try {
                    inputInvalidColor = Color.parseColor(elem.getString(TAG_SUPPORT_COLOR));
                } catch (IllegalArgumentException e) {
                    Log.e(TAG, "Invalid sc color specification: " + elem.getString(TAG_SUPPORT_COLOR));
                }
            }
            view.setTextSize(mainActivity.getResources().getDimension(R.dimen.textsize));
            view.setGravity(getGravityForElement(elem));
            view.setCursorVisible(true);
            setElemFontProperties(view, elem);
            setElemSize(view, elem);
            view.requestFocus();
            int inpType = view.getInputType();
            if(inpType != InputType.TYPE_CLASS_TEXT || defaultEditorType == XG.EditorType.PIN) {
                //prevent android keyboard from popping up for numbers as we handle them from pinpad
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // click disabled - ignoring
                    }
                });
                view.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        // touch disabled - ignoring
                        return true;
                    }
                } );
            }
            view.setImeOptions(EditorInfo.IME_ACTION_GO);
            final String data = elem.optString(TAG_DATA, "");
            final String etoken = elem.optString(TAG_ETOKEN, "");
            view.setText(elem.optString(TAG_VALUE, null));
            String re = null;
            if (elem.has(TAG_VALIDATOR_REGEX)) {
                re = elem.getString(TAG_VALIDATOR_REGEX);
                view.setFilters(
                        new InputFilter[]{
                                new PartialRegexInputFilter(re)
                        }
                );
                final EditText finalView = view;
                final String finalRe = re;
                final int finalInputValidColor = inputValidColor;
                final int finalInputInvalidColor = inputInvalidColor;
                view.addTextChangedListener(
                        new TextWatcher() {

                            @Override
                            public void afterTextChanged(Editable s) {
                                String value = s.toString();
                                if (value.matches(finalRe)) {
                                    finalView.setTextColor(finalInputValidColor);
                                } else {
                                    finalView.setTextColor(finalInputInvalidColor);
                                }
                            }

                            @Override
                            public void beforeTextChanged(CharSequence s, int start,
                                                          int count, int after) {
                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start,
                                                      int before, int count) {
                            }

                        }
                );

            }
            int textEnd = view.getText().length();
            view.setSelection(textEnd);

            //view.setMovementMethod(new ScrollingMovementMethod());
            //view.setHorizontallyScrolling(true);

            final String finalRe1 = re;
            view.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if(actionId != EditorInfo.IME_ACTION_GO || isValidInput(v, finalRe1)) {
                        if (!myTokenizer.checkTokenValidity(myToken)) {
                            return false;
                        }
                    }
                    if (token == -1) {
                        return false;
                    }
                    if (actionId == EditorInfo.IME_ACTION_GO) {
                        commitInput(v, finalRe1, token, etoken, data);
                        return true;
                    } else if(actionId == EditorInfo.IME_ACTION_PREVIOUS) {
                        Log.d(TAG, "cancelInput: actionId == EditorInfo.IME_ACTION_PREVIOUS");
                        cancelInput(v);
                    }
                    return false;
                }
            });

        }
        return edContainer;
    }

    static private View renderNumericKeyboard(JSONObject elem) {
        KeyboardView.resetNumKbdButtons();
        View numKbd = mainActivity.findViewById(R.id.numKbd);
        numKbd.setVisibility(View.VISIBLE);
        return null;
    }

    static private View renderAlnumKeyboard(JSONObject elem) {
        Log.i(TAG, "Rendering AlnumKeyboard skipped, native android keyboard will be used");
        return null;
    }

    public static Drawable getDrawable(String name, String defaultName) {
        int resourceId = mainActivity.getResources().getIdentifier(name, "drawable", mainActivity.getPackageName());
        if (resourceId == 0 && defaultName != null) {
            resourceId = mainActivity.getResources().getIdentifier(defaultName, "drawable", mainActivity.getPackageName());
        }
        if (resourceId == 0) {
            return null;
        }
        return mainActivity.getDrawable(resourceId);
    }

    static private ArrayList<MenuItem> getMenuItems(JSONObject menuRoot) throws JSONException {
        ArrayList<MenuItem> list = new ArrayList<MenuItem>();
        JSONArray elems = menuRoot.getJSONArray(TAG_ELEMS);
//        int checkedRadio = -1;
//        int firstRadio = -1;
        for (int elemn = 0; elemn < elems.length(); ++elemn) {
            JSONObject menuItem = elems.getJSONObject(elemn);
            String label = null;
            int pass = menuItem.optInt(TAG_PASSWORD, -1);
            Drawable icon = getDrawable(menuItem.optString(TAG_ICON, "ic_baseline_directions_run_24"), "ic_baseline_directions_run_24");
            switch (menuItem.getString(TAG_TYPE)) {
                case UI_LABEL:
                    list.add(new MenuItem(menuItem.getString(TAG_LABEL), icon, pass!=-1));
                    break;
                case UI_ICON:
                    list.add(new MenuItem(menuItem.optString(TAG_LABEL,null), icon, pass!=-1));
                    //list.add(menuItem.getString(TAG_ICON));
                    break;
                case UI_MENU_RADIO:
                    if (menuItem.optInt(TAG_VALUE, 0) == 0 && menuItem.has(TAG_ICON)) {
                        icon = getDrawable(menuItem.getString(TAG_ICON) + "_", "ic_baseline_directions_run_24");
                        icon.setColorFilter(mainActivity.getResources().getColor(R.color.colorLightGrey), PorterDuff.Mode.MULTIPLY);
                    }
                    list.add(new MenuItem(menuItem.getString(TAG_LABEL), icon, pass!=-1));
                    //TODO: radio button in icons menu
//                    if (menuItem.optInt(TAG_VALUE, 0) == 1) {
//                        checkedRadio = list.size();
//                        label = MenuAdapter.RADIO_ON + label;
//                    } else {
//                        label = MenuAdapter.RADIO_OFF + label;
//                    }
//                    if (firstRadio == -1) {
//                        firstRadio = list.size();
//                    }
//                    list.add(label);
                    break;
                case UI_MENU_SEPARATOR:
                  //  list.add(MenuAdapter.SEPARATOR);
                    break;
                default:
                    Log.w(TAG, "Not implemented menu item: " + menuItem.getString(TAG_TYPE));
                    break;
            }
        }

        //TODO: radio buttons in icon menu?
//        if (checkedRadio == -1) {
//            checkedRadio = firstRadio;
//        }
//        if (checkedRadio != -1 && !list.get(checkedRadio).startsWith("+ ")) {
//            list.set(checkedRadio, MenuAdapter.RADIO_ON + list.get(checkedRadio).substring(2));
//        }
        return list;

    }

    static private ArrayList<String> parseMenuStructure(JSONObject menuRoot) throws JSONException {
        ArrayList<String> list = new ArrayList<String>();
        JSONArray elems = menuRoot.getJSONArray(TAG_ELEMS);
        int checkedRadio = -1;
        int firstRadio = -1;
        for (int elemn = 0; elemn < elems.length(); ++elemn) {
            JSONObject menuItem = elems.getJSONObject(elemn);
            String label = null;
            switch (menuItem.getString(TAG_TYPE)) {
                case UI_LABEL:
                    list.add(menuItem.getString(TAG_LABEL));
                    break;
                case UI_ICON:
                    list.add(menuItem.getString(TAG_LABEL));
                    //list.add(menuItem.getString(TAG_ICON));
                    break;
                case UI_MENU_RADIO:
                    label = menuItem.getString(TAG_LABEL);
                    if (menuItem.optInt(TAG_VALUE, 0) == 1) {
                        checkedRadio = list.size();
                        label = MenuAdapter.RADIO_ON + label;
                    } else {
                        label = MenuAdapter.RADIO_OFF + label;
                    }
                    if (firstRadio == -1) {
                        firstRadio = list.size();
                    }
                    list.add(label);
                    break;
                case UI_MENU_SEPARATOR:
                    list.add(MenuAdapter.SEPARATOR);
                    break;
                default:
                    Log.w(TAG, "Not implemented menu item: " + menuItem.getString(TAG_TYPE));
                    break;
            }
        }
        if (checkedRadio == -1) {
            checkedRadio = firstRadio;
        }
        if (checkedRadio != -1 && !list.get(checkedRadio).startsWith("+ ")) {
            list.set(checkedRadio, MenuAdapter.RADIO_ON + list.get(checkedRadio).substring(2));
        }
        return list;
    }

    static private View renderMenu(final JSONObject elem) throws JSONException {
        focusTokenizer.renderedActiveControl();
        final GuiTokenizer myTokenizer = focusTokenizer;
        final int myToken = myTokenizer.getNextScreenToken();

        ListView view = new ListView(mainActivity);
        final int token = elem.getInt(TAG_TOKEN);
        ArrayList<String> list = null;
        String elems = elem.getString(TAG_ELEMS);
        list = parseMenuStructure(elem);
        final MenuAdapter adapter = new MenuAdapter(mainActivity, list);
        view.setAdapter(adapter);
        final int menuToken = elem.optInt(TAG_TOKEN, -1);
        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!myTokenizer.checkTokenValidity(myToken)) {
                    return;
                }
                Log.d(TAG, "onItemClick> position: " + position + ", id: " + id);
                adapter.userSelection(position);
                Log.d(TAG, "        >> SELECTED pos: " + position);
                try {
                    JSONObject clicked = null;
                    clicked = elem.getJSONArray(TAG_ELEMS).getJSONObject(position);
                    Log.d(TAG, "            val: " + clicked.optString(TAG_VALUE, ""));
                    Log.d(TAG, "        clicked: " + clicked.toString());
                    XG.postUIEvent(String.format("{\"%s\":0,\"%s\":%d,\"%s\":%d,\"%s\":\"%s\",\"%s\":\"%s\",\"%s\":\"%s\"}",
                            TAG_RC,
                            TAG_TOKEN, clicked.optInt(TAG_TOKEN, token),
                            TAG_ETOKEN, clicked.optInt(TAG_ETOKEN, -1),
                            TAG_VALUE, clicked.optString(TAG_VALUE, ""),
                            TAG_DATA, elem.optString(TAG_DATA, ""),
                            TAG_PASSWORD, clicked.optInt(TAG_PASSWORD, -1)));
                } catch (JSONException e) {
                    Log.e(TAG, "Failed to prepare click event: " + e, e);
                }
            }
        });
        return view;
    }

    static private ArrayList<String> parseInfoScreen(String text) {
        ArrayList<String> list = new ArrayList<String>();
        String emptyLine = " ";
        if (text.isEmpty()) {
            Log.i(TAG, "text is empty");
            return null;
        }
        String line = "";
        String remainText = "";
        int NLidx = 0;
        for (int i = 0; i < text.length(); i++) {
            remainText = text.substring(i);
            NLidx = remainText.indexOf("\n");
            if (NLidx != -1) {
                line = remainText.substring(0, NLidx);
                if (line.isEmpty()) {
                    list.add(emptyLine);
                } else
                    list.add(line);
                i += NLidx;
            }
        }
        return list;
    }

    static private float getFontSize(JSONObject elem) {
        float size = mainActivity.getResources().getDimension(R.dimen.textsize);
        try {
            if (elem.has(TAG_FONT)) {
                JSONObject fontDef = elem.getJSONObject(TAG_FONT);
                switch (fontDef.optString(TAG_SIZE, null)) {
                    case UI_FONT_SIZE_XSMALL:
                        size = 0.6f * size;
                        break;
                    case UI_FONT_SIZE_SMALL:
                        size = 0.8f * size;
                        break;
                    case UI_FONT_SIZE_LARGE:
                        size = 1.2f * size;
                        break;
                    case UI_FONT_SIZE_XLARGE:
                        size = 1.5f * size;
                        break;
                }
            }
        } catch (JSONException e) {
        }
        return size;
    }

    static private String getFontColor(JSONObject elem) {
        String color = "#000000";
        try {
            if (elem.has(TAG_FONT)) {
                JSONObject fontDef = elem.getJSONObject(TAG_FONT);
                if (fontDef.has(TAG_FOREGROUND_COLOR))
                    return fontDef.getString((TAG_FOREGROUND_COLOR));

            }
        } catch (JSONException e) {
        }
        return color;
    }

    static private String getSupportColor(JSONObject elem) {
        String color = "#ffffff";
        try {
            if (elem.has(TAG_SUPPORT_COLOR) && !elem.getString(TAG_SUPPORT_COLOR).isEmpty())
                return elem.getString(TAG_SUPPORT_COLOR);
        } catch (JSONException e) {
        }
        return color;
    }

    static private View renderInfoScreen(JSONObject elem) throws JSONException{
        if (!elem.has(TAG_LABEL) || elem.getString(TAG_LABEL).isEmpty()) {
            Log.e(TAG, "missing or empty label value for infoScreen");
            return null;
        }

        InfoScreenView view = new InfoScreenView(mainActivity);
        ArrayList<String> list = parseInfoScreen(elem.getString(TAG_LABEL));
       if (list == null) {
          Log.e(TAG, "failed to render info");
            return null;
        }

        final InfoScreenAdapter adapter = new InfoScreenAdapter(mainActivity, list, getFontSize(elem), getFontColor(elem), getSupportColor(elem));
        view.setAdapter(adapter);
        return view;
    }
    
    static public Integer getOptColor(final JSONObject elem, String tag) {
        Integer col = null;
        if (elem.has(tag)) {
            try {
                col = Color.parseColor(elem.optString(tag, null));
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "Invalid color specification for: " + tag);
            }
        }
        return col;
    }

    static private View renderIconMenu(final JSONObject elem, int ecol) throws JSONException {
        focusTokenizer.renderedActiveControl();
        final GuiTokenizer myTokenizer = focusTokenizer;
        final int myToken = myTokenizer.getNextScreenToken();

        int iconMenuColumns = (ecol==-1)?ICON_MENU_COLUMNS:ecol+1;
        final IconMenuView view = new IconMenuView(mainActivity, iconMenuColumns);
        final int token = elem.getInt(TAG_TOKEN);

        view.setLayoutManager(new GridLayoutManager(mainActivity, iconMenuColumns));

        setViewBgTintColorFromElem(view, elem);

        ArrayList<MenuItem> list = getMenuItems(elem);

        if (list.size() > view.FWD_SCROLL_ITEMS) {
            menuThatNeedsScrolling = view;
        }
        final IconMenuAdapter adapter = new IconMenuAdapter(mainActivity, list,
                getOptColor(elem, TAG_FOREGROUND_COLOR),
                getOptColor(elem, TAG_SUPPORT_COLOR),
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = v.getId();
                Log.d(TAG, "onItemClick> position: " + position );
                try {
                    JSONObject clicked = null;
                    clicked = elem.getJSONArray(TAG_ELEMS).getJSONObject(position);
                    Log.d(TAG, "            val: " + clicked.optString(TAG_VALUE, ""));
                    Log.d(TAG, "        clicked: " + clicked.toString());
                    if (clicked.optInt(TAG_TOKEN, -1) == XG_TOKEN_F1) {
                        if (menuThatNeedsScrolling != null) {
                            menuThatNeedsScrolling.scrollForward();
                        }
                        return;
                    }
                    if (!myTokenizer.checkTokenValidity(myToken)) {
                        return;
                    }
                    XG.postUIEvent(String.format("{\"%s\":0,\"%s\":%d,\"%s\":%d,\"%s\":\"%s\",\"%s\":\"%s\",\"%s\":\"%s\"}",
                            TAG_RC,
                            TAG_TOKEN, clicked.optInt(TAG_TOKEN, token),
                            TAG_ETOKEN, clicked.optInt(TAG_ETOKEN, -1),
                            TAG_VALUE, clicked.optString(TAG_VALUE, ""),
                            TAG_DATA, elem.optString(TAG_DATA, ""),
                            TAG_PASSWORD, clicked.optInt(TAG_PASSWORD, -1)));
                } catch (JSONException e) {
                    Log.e(TAG, "Failed to prepare click event: " + e, e);
                }
            }
        });

        view.setAdapter(adapter);

        return view;
    }

    private JSONObject parsedDisplay;

    @Override
    @JavascriptInterface
    public String getDisplayContent() {
        return displayContentStr;
    }
    private int renderDisplayResult = 0;

    private final Runnable renderDisplayThread = () -> {
        synchronized(this) {
            wakeLock.acquire(XG.WAKELOCK_TIMEOUT);
            hideKeyboardFrom();

            synchronized (isBackgroundMode) {
                renderDisplayResult = 0;
                if (XG.isActivityPaused && !isBackgroundMode) {
                    Intent homeIntent = new Intent(mainActivity, mainActivityClass);
                    homeIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    mainActivity.startActivity(homeIntent);
                }
            }

            if (hardwareSpecification.isCtlsReaderBehindDisplay && !isUIModeUpdate(parsedDisplay)) {
                final ImageView ctlsIcon = mainActivity.findViewById(R.id.ctlsIcon);
                if (parsedDisplay.optBoolean(TAG_CTLS)) {
                    ctlsIcon.setVisibility(View.VISIBLE);
                } else {
                    ctlsIcon.setVisibility(View.INVISIBLE);
                }
            }

            final ProgressBar progressBar = (ProgressBar) mainActivity.findViewById(R.id.progressBar);
            try {
                if (parsedDisplay.has(TAG_SUPPORT_COLOR)) {
                    int progressBarColor = Color.parseColor(parsedDisplay.getString(TAG_SUPPORT_COLOR));
                    progressBar.setIndeterminateTintList(ColorStateList.valueOf(progressBarColor));
                }
                if (parsedDisplay.has(TAG_UI_MODE)) {
                    if (UI_MODE_WAIT.equals(parsedDisplay.getString(TAG_UI_MODE))) {
                        progressBar.setVisibility(View.VISIBLE);
                    } else {
                        progressBar.setVisibility(View.GONE);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
                renderDisplayResult = -1;
            }

            try {
                boolean rerenderTop = parsedDisplay.has(TAG_TOP) && parsedDisplay.getJSONArray(TAG_TOP).length() > 0;
                boolean rerenderBtm = parsedDisplay.has(TAG_BOTTOM) && parsedDisplay.getJSONArray(TAG_BOTTOM).length() > 0;
                boolean newEditor = false;
                if (rerenderTop) {
                    focusTokenizer = topTokenizer;
                    final TableLayout dspTop = (TableLayout) mainActivity.findViewById(R.id.dspTop);
                    dspTop.removeAllViews();
                    topEditorContext = renderArray(dspTop, parsedDisplay.getJSONArray(TAG_TOP));
                    newEditor = newEditor || topEditorContext != null;
                    focusTokenizer.commitScreen();
                }
                if (rerenderBtm) {
                    focusTokenizer = btmTokenizer;
                    final TableLayout dspBtm = (TableLayout) mainActivity.findViewById(R.id.dspBtm);
                    dspBtm.removeAllViews();
                    btmEditorContext = renderArray(dspBtm, parsedDisplay.getJSONArray(TAG_BOTTOM));
                    newEditor = newEditor || btmEditorContext != null;
                    focusTokenizer.commitScreen();
                }

                final EditorContext editorContext = getEditorContext();

                if (editorContext != null && editorContext.editor != null) {
                    switch (editorContext.editorType) {
                        case ALPHANUMERIC:
                            InputMethodManager imm = (InputMethodManager) mainActivity.getSystemService(
                                    Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(editorContext.editor, 0);
                            mainActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                            break;
                        case NUMERIC:
                        case PASSWORD:
                            mainActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                            final KeyboardView kv = mainActivity.findViewById(R.id.numKbd);
                            final boolean newEditorP = newEditor;
                            kv.setKeyboardListener(new KeyboardListener() {
                                @Override
                                public void onCancel() {
                                    editorContext.editor.onEditorAction(EditorInfo.IME_ACTION_PREVIOUS);
                                }

                                @Override
                                public void onClear() {
                                    int length = editorContext.editor.getText().length();
                                    if (length > 0) {
                                        editorContext.editor.getText().delete(length - 1, length);
                                    }
                                }

                                @Override
                                public void onCommit() {
                                    editorContext.editor.onEditorAction(EditorInfo.IME_ACTION_GO);
                                }

                                @Override
                                public void onEnter(String chars) {
                                    editorContext.editor.append(chars);
                                }
                            });
                            break;
                    }
                }

                if (editorContext == null || (editorContext.editorType != EditorType.NUMERIC && editorContext.editorType != EditorType.PASSWORD && editorContext.editorType != EditorType.PIN)) {
                    View numKbd = mainActivity.findViewById(R.id.numKbd);
                    numKbd.setVisibility(View.GONE);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                renderDisplayResult = -1;
            }

            finally {
                notifyAll();
            }
        }
    };

    private boolean isUIModeUpdate(JSONObject parsedDisplay) {
        return parsedDisplay.has(TAG_UI_MODE) && !parsedDisplay.has(TAG_TOP) && !parsedDisplay.has(TAG_BOTTOM);
    }

    @Override
    @JavascriptInterface
    public synchronized int display(final String strdef, final int minimalDisplayTimeMs) {
        Log.d(TAG, "Rendering: " + strdef + ", minimalDisplayTimeMs: " + minimalDisplayTimeMs);
        try {
            parsedDisplay = new JSONObject(strdef);
            if (parsedDisplay.has(TAG_TOP) || minimalDisplayTimeMs > 0) {
                synchronizeMinimalDisplayTime(minimalDisplayTimeMs);
                menuThatNeedsScrolling = null;
            }

            displayContentStr = strdef;

            mainActivity.runOnUiThread(renderDisplayThread);
            wait();
            return renderDisplayResult;

        } catch (JSONException  e) {
            Log.w(TAG, "Invalid display specs: " + strdef, e);
            return -1;
        } catch (InterruptedException e) {
            Log.w(TAG, "Thread interrupted", e);
            return -1;
        }

    }

    @Override
    @JavascriptInterface
    public String pollEvent() {
        return eventQueue.poll();
    }

    private static int firstIgnoredToken = -1;
    private static int lastIgnoredToken = -1;

    public static void postEvent(String event) {
        postEvent(event, false);
    }

    public static void postEventBlocking(String event) {
        if (XG.xgInstance == null) {
            Log.e(TAG, "No XG instance");
            return;
        }
        XG.xgInstance.postEventAndWait(event);
    }

    private synchronized void postEventAndWait(String event) {
        postEvent(event, true);
        isWaitingForJsReady = true;
        try {
            wait(10000);
        } catch (InterruptedException e) {
            Log.e(TAG, "interrupt while waiting for JsReady");
            e.printStackTrace();
        }
    }

    private static void postUIEvent(String event) {
        try {
            JSONObject evtObj = new JSONObject(event);
            evtObj.put("uievt", true);
            event = evtObj.toString();
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        postEvent(event, true);
    }

    public static void postEvent(String event, boolean sendEventDataDirectly) {
        if (misc == null) {
            Log.e(TAG, "Can't call postEvent, since object is not created yet");
            return;
        }
        if(sendEventDataDirectly) { //for now it comes from XG UI events only
            Log.d(TAG, "XGpostEvent prepare: "+ Thread.currentThread().getName()+"; "+event);
            boolean postedUIEvent = misc.postEventToJS(event, true);
            if(postedUIEvent) {
                XG.xgInstance.isProcessingUIEvent = true;
                Log.d(TAG, "XGpostEvent executed: " + event);
            }
            else {
                if(!XG.xgInstance.isProcessingUIEvent) {
                    XG.xgInstance.pendingUIEvent = event;
                }
                Log.d(TAG, "XGpostEvent ignored: " + event+"; "+XG.xgInstance.pendingUIEvent+"; "+XG.xgInstance.isProcessingUIEvent);
            }
        }
        else {
            synchronized (eventQueue) {
                if (firstIgnoredToken >= 0) {
                    try {
                        JSONObject json = new JSONObject(event);
                        int token = json.getInt(TAG_TOKEN);
                        if (token >= firstIgnoredToken && token <= lastIgnoredToken) {
                            Log.d(TAG, "Ignoring token: " + token);
                            return;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                maximizeApp();
                eventQueue.add(event); // to consider: this function throws exception when the queue is full
                misc.newEventNotify();
                Log.d(TAG, "New XG event posted: " + event + ", queue size:" + eventQueue.size());
            }
        }
    }

    @Override
    @JavascriptInterface
    public void ignoreTokens(int first, int last) {
        synchronized(eventQueue) {
            firstIgnoredToken = first;
            lastIgnoredToken = last;
        }
    }

    public static void maximizeApp() {
        // TODO
    }

    public static void hideKeyboardFrom() {
        View view = mainActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) mainActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    @JavascriptInterface
    public void setBackgroundMode(boolean backgroundMode) {
        synchronized (isBackgroundMode) {
            isBackgroundMode = backgroundMode;
        }
    }
}
