package com.wallee.vsd.common.integration;

import com.wallee.android.till.sdk.data.FinalBalanceResult;
import com.wallee.android.till.sdk.data.Receipt;
import com.wallee.android.till.sdk.data.ResultCode;
import com.wallee.android.till.sdk.data.State;
import com.wallee.vsd.common.integration.data.DataUtils;
import com.wallee.vsd.common.integration.xml.BeFinalBalanceRequest;
import com.wallee.vsd.common.integration.xml.XmlUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class ExecuteFinalBalanceHandler {
    private final NetClient client = new NetClient();

    FinalBalanceResult handle() {
        if (client.connectWithServer()) {
            try {
                BeFinalBalanceRequest xmlRequest = createXmlRequest();
                List<Receipt> receipts = new ArrayList<>();
                FinalBalanceResult result = null;

                client.send(XmlUtils.toXml(xmlRequest));
                while (result == null) {
                    String xmlResponse = client.receive();
                    if (xmlResponse.isEmpty()) {
                        result = createErrorResult("Internal error - empty response");
                    } else if (XmlUtils.shouldConfirm(xmlResponse)) {
                        client.send(XmlUtils.CONFIRMATION);
                    } else if (XmlUtils.isPrinterNotification(xmlResponse)) {
                        Receipt receipt = DataUtils.printerNotificationToReceipt(XmlUtils.toPrinterNotification(xmlResponse));
                        if (receipt != null) {
                            receipts.add(receipt);
                        }
                    } else if (XmlUtils.isErrorNotification(xmlResponse)) {
                        result = createErrorResult(XmlUtils.toErrorNotification(xmlResponse).errorText);
                    } else if (XmlUtils.isFinalBalanceResponse(xmlResponse)) {
                        result = createResult(receipts);
                    }
                }

                return result;
            } catch (Exception e) {
                e.printStackTrace();
                return createErrorResult(e.getMessage());
            } finally {
                client.disConnectWithServer();
            }
        } else {
            return createErrorResult("Failed to connect to VPJ");
        }
    }

    private BeFinalBalanceRequest createXmlRequest() {
        return new BeFinalBalanceRequest(
                DataUtils.POS_ID
        );
    }

    private FinalBalanceResult createResult(List<Receipt> receipts) {
        return new FinalBalanceResult(
                State.SUCCESSFUL,
                new ResultCode(
                        "0",
                        "Success"
                ),
                receipts
        );
    }

    private FinalBalanceResult createErrorResult(String error) {
        return new FinalBalanceResult(
                State.FAILED,
                new ResultCode(
                        "error",
                        error
                ),
                Collections.emptyList()
        );
    }
}

