package com.wallee.vsd.common.models;

public class PaymentData {
    private static PaymentData instance;
    private long id;
    private int amount;
    private int cashbackAmount;
    private byte transactionType;
    private String currency;
    private String timeStamp;

    public static PaymentData getInstance() {
        if (instance == null) {
            instance = new PaymentData();
        }
        return instance;
    }

    public static void clear() {
        instance = null;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setCashbackAmount(int cashbackAmount) {
        this.cashbackAmount = cashbackAmount;
    }

    public int getCashbackAmount() {
        return cashbackAmount;
    }

    public void setTransactionType(byte transactionType) {
        this.transactionType = transactionType;
    }

    public byte getTransactionType() {
        return transactionType;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCurrencyCode() {
        if(currency.equals("PLN")) return "985";
        else if(currency.equals("EUR")) return "978";
        else return null;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public int getAmount() {
        return amount;
    }

    public long getId() {
        return id;
    }
}
