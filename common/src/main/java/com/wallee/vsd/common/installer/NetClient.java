package com.wallee.vsd.common.installer;

import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class NetClient extends OkHttpClient {
    private final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    public NetClient() {
        dispatcher().setMaxRequests(1);
        dispatcher().setMaxRequestsPerHost(1);
    }

    void get(String url, Callback callback) {
        Request request = new Request.Builder()
                .url(url)
                .build();
        newCall(request).enqueue(callback);
    }

    void post(String url, String json, Callback callback) {
        Request request = new Request.Builder()
                .url(url)
                .post(RequestBody.create(json, JSON))
                .build();
        newCall(request).enqueue(callback);
    }
}
