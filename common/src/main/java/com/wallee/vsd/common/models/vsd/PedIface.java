package com.wallee.vsd.common.models.vsd;

public interface PedIface {
    int MODE_ECB_DECRYPT = 0;
    int MODE_ECB_ENCRYPT = 1;
    int MODE_CBC_DECRYPT = 2;
    int MODE_CBC_ENCRYPT = 3;
    int MODE_OFB_DECRYPT = 4;
    int MODE_OFB_ENCRYPT = 5;

    enum PinResult {
        OK(0), TIMEOUT(2), three(3);
        private int value;
        PinResult(int value) {
            this.value = value;
        }
        public int getValue(){
            return value;
        }
    }

    int getPinBlock(
            int keyIdx,
            int mode,
            String pan,
            int minLen,
            int maxLen,
            int timeoutMs,
            int timeoutReminderMs,
            boolean detectCardRemoval,
            boolean pinBypassAlwd,
            int token);

    int pedDisplayRandomKeyboardForPin(boolean random);

    int pedSetInputPinBoxVisible(boolean visible);

    String des(int keyIdx, String data, String vector, int mode);

    String mac(int keyIdx, String data, int mode);

    int storePlainKey(int dstType, int dstIndex, String value);

    int storeKey(int encrypt, String value, int srcKeyIndex, int srcKeyType, int dstKeyIndex, int dstKeyType, int dstKeyLen);

    int deleteKey(int keyType, int keyIndex);

    String getKcv(int dstType, int dstIndex);

    int genRandomKey(int dstType, int dstIndex, int len);

    int createSessionKeyVar(int dstType, int dstIndex, int sessionIndex, String xorMask);

    String createEp2Cryptogram(int sessionKeyId, int compSecretId, String senderId, String publicKeyFormat, int version);

    String createEp2Cryptogram(int sessionKeyId, int compSecretId, String senderId, String publicKeyFormat, int version, String pubKeyId);

    int storePublicKey(int macRecvKeyIdx, String message,  String macHex,  String pubKeyId, String publicKey);

    int storePublicKey(int macRecvKeyIdx, String message,  String macHex,  String pubKeyId, String publicKey, boolean isJSON);

    int erase();

    int cancelInput();
}
