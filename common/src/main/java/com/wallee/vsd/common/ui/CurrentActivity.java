package com.wallee.vsd.common.ui;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public abstract class CurrentActivity extends AppCompatActivity {
    public static CurrentActivity thisActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        thisActivity = this;
    }

    @Override
    protected void onResume() {
        super.onResume();
        thisActivity = this;
    }
}
