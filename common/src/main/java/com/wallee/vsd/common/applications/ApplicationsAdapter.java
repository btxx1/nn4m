package com.wallee.vsd.common.applications;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wallee.vsd.common.R;
import com.wallee.vsd.common.applications.data.LauncherEntry;

import java.util.ArrayList;
import java.util.List;

class ApplicationsAdapter extends RecyclerView.Adapter<ApplicationsAdapter.MyViewHolder> {
    private final Context context;
    private final View.OnClickListener listener;

    private List<LauncherEntry> entries = new ArrayList<>();

    ApplicationsAdapter(Context context, View.OnClickListener listener) {
        this.context = context;
        this.listener = listener;
    }

    void setData(List<LauncherEntry> entries) {
        this.entries = entries;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.grid_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        LauncherEntry entry = entries.get(position);
        holder.imageButton.setImageDrawable(entry.getApplicationIcon());
        holder.imageButton.setTag(entry);
        holder.imageButton.setOnClickListener(listener);
        holder.label.setText(entry.getApplicationName());
    }

    @Override
    public int getItemCount() {
        return entries.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        final ImageButton imageButton;
        final TextView label;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageButton = itemView.findViewById(R.id.icon);
            label = itemView.findViewById(R.id.label);
        }
    }
}
