package com.wallee.vsd.common.models.vsd;

public interface RfIface {
    void close();
    int open();
    int detect();
}

