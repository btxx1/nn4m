package com.wallee.vsd.common.vsd;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.webkit.JavascriptInterface;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.wallee.vsd.common.installer.AppsInstaller;
import com.wallee.vsd.common.installer.NetClient;
import com.wallee.vsd.common.models.vsd.UpdtIface;



public class Updt implements UpdtIface {

    static final int MAX_FILENAME_LEN = 127;

    protected static final String FT_OS = "OS";
    protected static final String FT_SO = "SO";
    protected static final String FT_APP = "APP";
    protected static final String FT_AUP = "AUP";
    protected static final String FT_FONT = "FONT";
    protected static final String FT_RES = "RES";
    protected static final String FT_EXE = "EXE";

    private static final String TAG = "Updt";
    private static UpdtIface instance = null;


    static protected Context appContext = null;
    private static long firstInstallTime;
    private static AppsInstaller appsInstaller;

    public Updt(Context appContext) {
        Updt.appContext = appContext;
        Updt.instance = this;
        appsInstaller = new AppsInstaller(appContext, new NetClient(), Updt.getInstance());
    }

    public static UpdtIface getInstance() {
        return Updt.instance;
    }

    @Override
    @JavascriptInterface
    public int updateFirmware(String fileName) {
        return -1;
    }

    protected static String getUpdatePath() {
        return Environment.getExternalStorageDirectory() + "/" + appContext.getPackageName();
    }
    @Override
    @JavascriptInterface
    public int updateFileToApp(String fileName) {
        File srcFile = new File(fileName);

        if (!srcFile.exists()) {
            Log.w(TAG, "File doesn't exist");
            return -1;
        }

        try {

            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(srcFile), "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            appContext.startActivity(intent);
            return 0;
        } catch (Exception e) {
            Log.e(TAG, "Failed to install apk installation", e);
            return -1;
        }
    }

    @Override
    @JavascriptInterface
    public int uninstallApp(String packageName) {
        return -1;
    }

    @Override
    @JavascriptInterface
    public int updateInstallAup(String fileName) {
        return -1;
    }

    @Override
    @JavascriptInterface
    public int updateFileToParam(String fileName, String appName) {
        return -1;
    }

    @Override
    @JavascriptInterface
    public int updateFileToFont(String fileName) {
        return -1;
    }

    @Override
    @JavascriptInterface
    public int checkFileNameLen(String name) {
        if (name.length() > MAX_FILENAME_LEN) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    @JavascriptInterface
    public String getCodeTypeFromName(String fileName) {
        String extension = fileName.substring(fileName.lastIndexOf(".")).toLowerCase();
        switch (extension) {
            case ".apk":
            case ".xapk":
            case ".cap":
                return FT_APP;
            case ".paydroid":
                return FT_OS;
            case ".otf":
            case ".ttf":
                return FT_FONT;
            case ".jar":
            case ".so":
                return FT_SO;
            case ".js":
                return FT_EXE;
            default:
                return FT_RES;
        }
    }

    static public void listFiles(File root) {
        File[] list = root.listFiles();
        for (File f : list) {
            if (f.isDirectory()) {
                Log.d(TAG, "Dir: " + f.getAbsoluteFile());
                listFiles(f);
            } else {
                Log.d(TAG, "File: " + f.getAbsoluteFile());
            }
        }
    }

    public static boolean copyFile(File source, File dest) {
        try {
            java.io.FileInputStream sourceFile = new java.io.FileInputStream(source);

            try {
                java.io.FileOutputStream destinationFile = null;

                try {
                    destinationFile = new FileOutputStream(dest);

                    byte buffer[] = new byte[512 * 1024];
                    int nbLecture;

                    while ((nbLecture = sourceFile.read(buffer)) != -1) {
                        destinationFile.write(buffer, 0, nbLecture);
                    }
                } catch (FileNotFoundException e) {
                    Log.e(TAG, "copyFile failed: " + e, e);
                } finally {
                    if (destinationFile != null) {
                        destinationFile.close();
                    }
                }
            } finally {
                sourceFile.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public static boolean moveFile(File source, File destination) {
        if (!destination.exists()) {
            boolean result = source.renameTo(destination);
            if (!result) {
                result = true;
                result &= copyFile(source, destination);
                if (result) result &= source.delete();

            }
            return (result);
        } else {
            return (false);
        }
    }

    @Override
    @JavascriptInterface
    public int instalDataToAppResFolder(String sourcename, String destination) {
        File destDir = new File(getAppDirectory(), destination);
        Log.d(TAG, "moveFile '" + sourcename + "' -> '" + destDir.getAbsolutePath() + "' ...");
        copyFile(new File(sourcename), destDir);
        return 0;
    }

    @Override
    @JavascriptInterface
    public int instalDataToAppExeFolder(String sourcename, String destination) {
        File destDir = new File(getAppDirectory(), destination);
        Log.d(TAG, "moveFile '" + sourcename + "' -> '" + destDir.getAbsolutePath() + "' ...");
        copyFile(new File(sourcename), destDir);
        return 0;
    }

    @Override
    @JavascriptInterface
    public int updateInstallSo(String soFile) {
        return -1;
    }

    public static String getAppDirectory() {
        return appContext.getApplicationContext().getFilesDir() + "/app0/";
    }

    public static String getPicturesDirectory() {
        return getAppDirectory() + "pictures";
    }

    public static String getFontsDirectory() {
        return getAppDirectory() + "fonts";
    }

    public static String getPictureFileExt() {
        return ".png";
    }

    public static int getBarcodeHeight() {
        return 128;
    }

    public static int getBarcodeWidth() {
        return 400;
    }

    public static String copyNewDirorfileFromAsset(String arg_assetDir, String dest_dir_path) throws IOException {
        //File int_path = Environment.getExternalStorageDirectory();
        File dest_dir = new File(dest_dir_path);

        AssetManager asset_manager = appContext.getApplicationContext().getAssets();
        String[] files = asset_manager.list(arg_assetDir);
        if (files.length > 0) {
            createDir(dest_dir);
            for (String file : files) {
                copyNewDirorfileFromAsset((arg_assetDir.length() > 0 ? (arg_assetDir + "/") : "") + file, dest_dir_path + "/" + file);
            }
        } else if (!arg_assetDir.contains(".png")) {
            try {
                Log.d(TAG, "copying file " + arg_assetDir);
                copyNewAssetFile(arg_assetDir, dest_dir_path);
            } catch (java.io.FileNotFoundException e) {
                Log.d(TAG, "error copying regular file: " + e.getMessage(), e);
            }
        }
        return dest_dir_path;
    }


    public static void copyNewAssetFile(String assetFilePath, String destinationFilePath) throws IOException {
        File dest = new File(destinationFilePath);
        if (dest.exists()) {
            if (dest.isDirectory()) {
                dest.delete();
            } else {
                File src = new File(assetFilePath);
                if (dest.lastModified() >= firstInstallTime) {
                    return;
                }
                dest.delete();
            }
        }
        InputStream in = appContext.getApplicationContext().getAssets().open(assetFilePath);
        OutputStream out = new FileOutputStream(destinationFilePath);

        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    public static void createDir(File dir) throws IOException {
        if (dir.exists()) {
            if (!dir.isDirectory()) {
                throw new IOException("Can't create directory, a file is in the way");
            }
        } else {
            dir.mkdirs();
            if (!dir.isDirectory()) {
                throw new IOException("Unable to create directory: " + dir.getAbsolutePath());
            }
        }
    }

    static public void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
    }


    static public int init() {
        try {
            File appDirFile = new File(getAppDirectory());
            createDir(appDirFile);

            PackageManager pm = appContext.getApplicationContext().getPackageManager();
            try {
                //firstInstallTime = pm.getPackageInfo(appContext.getPackageName(), 0).firstInstallTime;
                ApplicationInfo appInfo = pm.getApplicationInfo(appContext.getPackageName(), 0);
                String appFile = appInfo.sourceDir;
                firstInstallTime = new File(appFile).lastModified(); //Epoch Time
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                firstInstallTime = 0;
            }
            Log.d(TAG, "PalUpdt.init: copying app files for execution ...");
            copyNewDirorfileFromAsset("", getAppDirectory());
            // listFiles(appDirFile);
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
        return 0;
    }

    @Override
    @JavascriptInterface
    public int installFromUrl(String url) {
        Log.d(TAG, "Installing from URL: " + url);
        appsInstaller.check(url);
        return 0;
    }

    @Override
    @JavascriptInterface
    public void cleanup() {
        Updt.deleteRecursive(new File(Updt.getUpdatePath()));
    }


    }
