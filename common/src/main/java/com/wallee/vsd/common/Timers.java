package com.wallee.vsd.common;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.wallee.vsd.common.models.vsd.MiscIface;
import com.wallee.vsd.common.ui.CurrentActivity;
import com.wallee.vsd.common.vsd.XG;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.wallee.vsd.common.vsd.XG.TAG_RC;
import static com.wallee.vsd.common.vsd.XG.TAG_TOKEN;
import static com.wallee.vsd.common.vsd.XG.TAG_VALUE;

public class Timers extends Handler implements JSReadyListener {
    private static final String TAG = "TIMERS";
    private static Timers instance = null;

    private static final long PAUSED_TIME_UNSET = 0L;
    private long pausedTime = PAUSED_TIME_UNSET;

    private CurrentActivity currentActivity;
    private MiscIface misc;
    private final Map<Integer, Timer> timerMap = new HashMap<>();

    public void setListenerAndActivity(CurrentActivity currentActivity, MiscIface misc) {
        this.currentActivity = currentActivity;
        this.misc = misc;
        this.misc.addJSReadyListener(this);
    }

    private static class Timer extends BroadcastReceiver {
        public static final String ACTION_NOTIFY = "Alarm";
        private final long delay;
        private final int timerId;
        private final long timestamp;
        private boolean expired = false;
        private boolean broadCastReceiverSet = false;

        private final CurrentActivity currentActivity;
        private PendingIntent alarmListener = null;

        Timer(int timerId, long delay, CurrentActivity currentActivity) {
            this.timerId = timerId;
            this.delay = delay;
            this.timestamp = System.currentTimeMillis();
            this.currentActivity = currentActivity;
            schedule(timestamp + delay);
        }

        public void onPause() {
            if (!broadCastReceiverSet) {//when we go to background, Handler based timers will be lost, so re-schedule
                // timer with AlarmManager
                Log.d(TAG, "onPause: " + timerId);
                cancel();
                schedule(timestamp + delay);
            }
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            XG.xgInstance.wakeUp();

            Timers timers = Timers.getInstance();
            expired = true;

            cleanup();

            Log.d(TAG, "    fire timer NOW by alarmmanager: " + timerId);
            if (timers.sendMessage(timerId)) {
                removeTimer(timerId);
                Log.d(TAG, "Timer sent, ID: " + timerId);
            } else {
                Log.d(TAG, "Timer not sent, ID: " + timerId);
            }
        }

        public void schedule(long timeMillis) {
            AlarmManager alarmManager = (AlarmManager) currentActivity.getSystemService(Context.ALARM_SERVICE);
            Log.d(TAG,
                    "schedule timer: " + timerId + "; " + delay + "; " + timeMillis + "; " + Build.VERSION.SDK_INT +
                            "; " + Build.VERSION_CODES.N);
            Timers timers = Timers.getInstance();

            if (timers.pausedTime == Timers.PAUSED_TIME_UNSET && delay < 1000 * 60) {
                broadCastReceiverSet = false;
                timers.sendEmptyMessageDelayed(timerId, delay);
            } else {
                broadCastReceiverSet = true;
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction(ACTION_NOTIFY + timerId);
                currentActivity.registerReceiver(this, intentFilter);

                alarmListener = PendingIntent.getBroadcast(currentActivity, 0,
                        new Intent(Timer.ACTION_NOTIFY + timerId), PendingIntent.FLAG_UPDATE_CURRENT);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, timeMillis, alarmListener);
                } else {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, timeMillis, alarmListener);
                }
            }
        }

        private void cleanup() {
            if (broadCastReceiverSet) {
                currentActivity.unregisterReceiver(this);
                broadCastReceiverSet = false;
            }
            if (!expired) {
                Timers timers = Timers.getInstance();
                Log.d(Timers.TAG, "cancel: " + timerId + "; " + timers.hasMessages(timerId));
                if (alarmListener != null) {
                    AlarmManager alarmManager = (AlarmManager) currentActivity.getSystemService(Context.ALARM_SERVICE);
                    alarmManager.cancel(alarmListener);
                    alarmListener = null;
                }
                if (timers.hasMessages(timerId)) {
                    timers.removeMessages(timerId);
                }
            }
        }

        public void cancel() {
            cleanup();
        }
    }

    @Override
    public synchronized void handleMessage(Message msg) {
        super.handleMessage(msg);

        Timers timers = getInstance();
        int timerId = msg.what;
        Timer timer = timers.timerMap.get(timerId);
        if (timer != null) {
            timer.expired = true;
            boolean sentMessage = timers.sendMessage(timerId);
            if (sentMessage) {
                Log.d(TAG, "    fire timer NOW by handler: " + timerId);
                removeTimer(timerId);
            } else {
                Log.d(TAG, "    queue timer by handler: " + timerId);
            }
        }
    }

    public static synchronized Timers getInstance() {
        if (instance == null) {
            instance = new Timers();
        }
        return instance;
    }

    public static synchronized int setTimer(int timerId, long millis) {
        removeTimer(timerId);
        Log.d(TAG, "Setting timer: " + timerId);
        Timers timers = getInstance();
        timers.timerMap.put(timerId, new Timer(timerId, millis, timers.currentActivity));
        return 0;
    }

    public static synchronized void removeTimer(int timerId) {
        Timers timers = getInstance();
        Timer timer = timers.timerMap.get(timerId);
        if (timer != null) {
            Log.d(TAG, "Removing timer: " + timerId);
            timer.cancel();
            timers.timerMap.remove(timerId);
        }
    }

    public static int cancelTimer(int timerId) {
        Log.d(TAG, "Cancelling timer: " + timerId);
        removeTimer(timerId);
        return 0;
    }

    @Override
    public void onJSReady() {
        executeExpiredTimers();
    }

    private synchronized void executeExpiredTimers() {
        List<Integer> removeTimers = new ArrayList<>();
        for (Timer timer : timerMap.values()) {
            if (timer.expired) {
                boolean sentMessage = this.sendMessage(timer.timerId);
                if (sentMessage) {
                    Log.d(TAG, "executeExpiredTimers timer will be executed: " + timer.timerId);
                    removeTimers.add(timer.timerId);
                }
                break;
            }
        }
        for (int timerId : removeTimers) {
            removeTimer(timerId);
        }
    }

    public boolean sendMessage(int timerId) {
        JSONObject json = new JSONObject();
        boolean messageSent = false;
        try {
            json.put(TAG_TOKEN, timerId);
            json.put(TAG_RC, 0);
            json.put(TAG_VALUE, timerId);
            messageSent = misc.postEventToJS(json.toString(), true);
        } catch (JSONException e) {
            Log.e(TAG, "Failed to construct timer message: " + e, e);
        }

        return messageSent;
    }

    public synchronized void pause() {
        Log.d(TAG, "Timers pause: " + pausedTime);
        if (pausedTime == PAUSED_TIME_UNSET) {
            pausedTime = System.currentTimeMillis();
            for (Timer timer : timerMap.values()) {
                if (!timer.expired) {
                    timer.onPause();
                }
            }
        }
    }

    public void resume() {
        Log.d(TAG, "Timers resume: " + pausedTime + "; " + timerMap.size());
        executeExpiredTimers();
        pausedTime = PAUSED_TIME_UNSET;
    }
}