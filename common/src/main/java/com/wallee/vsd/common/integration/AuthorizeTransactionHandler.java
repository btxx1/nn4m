package com.wallee.vsd.common.integration;

import com.wallee.android.till.sdk.data.Receipt;
import com.wallee.android.till.sdk.data.ResultCode;
import com.wallee.android.till.sdk.data.State;
import com.wallee.android.till.sdk.data.Transaction;
import com.wallee.android.till.sdk.data.TransactionProcessingBehavior;
import com.wallee.android.till.sdk.data.TransactionResponse;
import com.wallee.vsd.common.integration.data.CurrencyType;
import com.wallee.vsd.common.integration.data.DataUtils;
import com.wallee.vsd.common.integration.xml.FinancialTrxRequest;
import com.wallee.vsd.common.integration.xml.FinancialTrxResponse;
import com.wallee.vsd.common.integration.xml.TrxData;
import com.wallee.vsd.common.integration.xml.XmlUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class AuthorizeTransactionHandler {
    private final NetClient client = new NetClient();

    TransactionResponse handle(Transaction transaction) {
        if (client.connectWithServer()) {
            try {
                FinancialTrxRequest xmlRequest = createXmlRequest(transaction);
                List<Receipt> receipts = new ArrayList<>();
                TransactionResponse response = null;

                client.send(XmlUtils.toXml(xmlRequest));
                while (response == null) {
                    String xmlResponse = client.receive();
                    if (xmlResponse.isEmpty()) {
                        response = createErrorResponse(transaction, "Internal error - empty response");
                    } else if (XmlUtils.shouldConfirm(xmlResponse)) {
                        client.send(XmlUtils.CONFIRMATION);
                    } else if (XmlUtils.isPrinterNotification(xmlResponse)) {
                        Receipt receipt = DataUtils.printerNotificationToReceipt(XmlUtils.toPrinterNotification(xmlResponse));
                        if (receipt != null) {
                            receipts.add(receipt);
                        }
                    } else if (XmlUtils.isErrorNotification(xmlResponse)) {
                        response = createErrorResponse(transaction, XmlUtils.toErrorNotification(xmlResponse).errorText);
                    } else if (XmlUtils.isFinancialTrxResponse(xmlResponse)) {
                        response = createResponse(transaction, receipts, XmlUtils.toFinancialTrxResponse(xmlResponse));
                    }
                }

                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return createErrorResponse(transaction, e.getMessage());
            } finally {
                client.disConnectWithServer();
            }
        } else {
            return createErrorResponse(transaction, "Failed to connect to VPJ");
        }
    }

    private FinancialTrxRequest createXmlRequest(Transaction transaction) {
        CurrencyType currencyType = DataUtils.getCurrencyType(transaction.getCurrency());
        long amount = Math.abs(DataUtils.toMinorUnits(transaction.getTotalAmountIncludingTax(), currencyType));
        return new FinancialTrxRequest(
                DataUtils.POS_ID,
                new TrxData(
                        String.valueOf(amount),
                        currencyType.code,
                        getTransactionTypeSymbol(transaction),
                        null,
                        transaction.getMerchantReference()
                )
        );
    }

    private String getTransactionTypeSymbol(Transaction transaction) {
        if (transaction.getTransactionProcessingBehavior() == TransactionProcessingBehavior.RESERVE) {
            return "50"; // reserve
        }
        if (DataUtils.isNegative(transaction.getTotalAmountIncludingTax())) {
            return "4"; // refund
        }
        return "0"; // purchase
    }

    private TransactionResponse createResponse(Transaction transaction, List<Receipt> receipts, FinancialTrxResponse response) {
        return new TransactionResponse(
                transaction,
                DataUtils.authResultToState(response.ep2AuthResult),
                DataUtils.authResultToResultCode(response.ep2AuthResult),
                response.ep2AuthCode,
                response.ep2TrmId,
                DataUtils.stringToLong(response.ep2TrxSeqCnt),
                response.transactionTime,
                DataUtils.stringToLong(response.transactionRefNumber),
                response.acquirerId,
                receipts
        );
    }

    private TransactionResponse createErrorResponse(Transaction transaction, String error) {
        return new TransactionResponse(
                transaction,
                State.FAILED,
                new ResultCode(
                        "error",
                        error
                ),
                null,
                null,
                null,
                null,
                null,
                null,
                Collections.emptyList()
        );
    }
}
