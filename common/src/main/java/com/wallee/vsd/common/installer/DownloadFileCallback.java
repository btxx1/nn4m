package com.wallee.vsd.common.installer;

import android.content.Context;

import androidx.core.util.Consumer;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import okio.BufferedSink;
import okio.Okio;

class DownloadFileCallback implements Callback {
    private final Context context;
    private final String fileName;
    private final Consumer<File> fileConsumer;
    private final Consumer<Exception> errorConsumer;

    DownloadFileCallback(Context context, String fileName, Consumer<File> fileConsumer, Consumer<Exception> errorConsumer) {
        this.context = context;
        this.fileName = fileName;
        this.fileConsumer = fileConsumer;
        this.errorConsumer = errorConsumer;
    }

    @Override
    public void onFailure(@NotNull Call call, @NotNull IOException e) {
        errorConsumer.accept(e);
    }

    @Override
    public void onResponse(@NotNull Call call, @NotNull Response response) {
        File downloadedFile;
        try {
            if (response.isSuccessful()) {
                downloadedFile = new File(context.getCacheDir(), fileName);
                try (BufferedSink sink = Okio.buffer(Okio.sink(downloadedFile))) {
                    sink.writeAll(response.body().source());
                }
                if (!downloadedFile.exists()) {
                    errorConsumer.accept(new IOException("File does not exist after download"));
                }

                fileConsumer.accept(downloadedFile);

            } else {
                errorConsumer.accept(new IOException(response.message()));
            }
        } catch (Exception e) {
            errorConsumer.accept(e);
        }
    }
}
