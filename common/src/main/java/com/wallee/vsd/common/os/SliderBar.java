package com.wallee.vsd.common.os;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.wallee.vsd.common.R;
import com.wallee.vsd.common.vsd.Misc;

import org.json.JSONException;
import org.json.JSONObject;

public class SliderBar extends AppCompatActivity {

    private final String TAG_HEADER_TEXT = "headerText";
    private final String TAG_HEADER_STYLE = "headerStyle";
    private final String TAG_BUTTON_LABEL = "buttonLabel";
    private final String TAG_BUTTON_STYLE = "buttonStyle";
    private final String TAG_ELEMENT_STYLE = "elementStyle";
    private final String TAG_SIZE = "size";
    private final String TAG_BOLD = "bold";

    private final String UI_FONT_SIZE_SMALL = "small";
    private final String UI_FONT_SIZE_XSMALL = "xsmall";
    private final String UI_FONT_SIZE_LARGE = "large";
    private final String UI_FONT_SIZE_XLARGE = "xlarge";
    private final String UI_FONT_SIZE_REGULAR = "regular";
    private final String TAG_FOREGROUND_COLOR = "fg";
    private final String TAG_BACKGROUND_COLOR = "bg";
    private final int DEFAULT_SCREEN_TIMEOUT_SEC = 15;
    AppCompatActivity mainActivity = Misc.getCurrentActivity();
    private float defaultFontSize = mainActivity.getResources().getDimension(R.dimen.textsize);
    private static final String TAG = "SliderBar";

    int timeout = DEFAULT_SCREEN_TIMEOUT_SEC;
    String params;
    public SeekBar sliderBar;
    public Context context;
    TextView textView;
    Button exitBtn;
    CountDownTimer countDownTimer;
    final int[] tickCounter = {timeout};

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "Creating activity");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider_bar);

        Intent intent = getIntent();
        params = intent.getExtras().getString("params");
        timeout = (intent.getExtras().getInt("timeout") > 0) ? intent.getExtras().getInt("timeout"): timeout;
        Log.d(TAG, "timeout = " + timeout);
        tickCounter[0] = timeout;
        textView = findViewById(R.id.textView);
        exitBtn = findViewById(R.id.exitBtn);
        sliderBar = findViewById(R.id.seekBar);
        final JSONObject paramsJSON;
        try {
            paramsJSON = new JSONObject(params);
            setTextParams(paramsJSON);
            setButtonParams(paramsJSON);
            setBarParams(paramsJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        context = getApplicationContext();
        setTimeout();
        adjustParam();
        exitBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                countDownTimer.cancel();
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "Resuming activity...");

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "Pausing activity...");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Destroying activity...");
    }

    private void setTimeout() {
        countDownTimer = new CountDownTimer(timeout * 1000, 1000) {
            public void onTick(long millisUntilFinished) {
                Log.d(TAG, "timeToFinish = " + (tickCounter[0]--));
            }
            public void onFinish() {
                finish();
            }
        }.start();
    }

    public void resetTimeout(){
        Log.d(TAG, "reseting timeout");
        countDownTimer.cancel();
        countDownTimer.start();
        tickCounter[0] = timeout;
    }

    public void adjustParam() {
        //override in platform subclass
    }

    private void setTextParams(JSONObject textParams) throws JSONException {
        if (textParams.has(TAG_HEADER_TEXT)) {
            Log.d(TAG, "headerText = "+ textParams.getString(TAG_HEADER_TEXT));
            textView.setText(textParams.getString(TAG_HEADER_TEXT));
        }

        if (textParams.has(TAG_HEADER_STYLE)) {
            JSONObject headerStyle = textParams.getJSONObject(TAG_HEADER_STYLE);
            switch (headerStyle.optString(TAG_SIZE, UI_FONT_SIZE_REGULAR)) {
                case UI_FONT_SIZE_SMALL:
                    defaultFontSize *= 0.8f;
                    break;
                case UI_FONT_SIZE_XSMALL:
                    defaultFontSize *= 0.5f;
                    break;
                case UI_FONT_SIZE_LARGE:
                    defaultFontSize *= 1.5f;
                    break;
                case UI_FONT_SIZE_XLARGE:
                    defaultFontSize *= 2.0f;
                    break;
                default:
                    break;
            }
            Log.d(TAG, "applied font size = "+ defaultFontSize);
            textView.setTextSize(defaultFontSize);
            if (headerStyle.has(TAG_BOLD) && headerStyle.getBoolean(TAG_BOLD) == true) {
                Log.d(TAG, "bold typeface set");
                textView.setTypeface(null, Typeface.BOLD);
            }
            textView.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
        }
    }

    private void setButtonParams(JSONObject buttonParams) throws JSONException {
        if (buttonParams.has(TAG_BUTTON_LABEL)) {
            Log.d(TAG, "buttonLabel = " + buttonParams.getString(TAG_BUTTON_LABEL));
            exitBtn.setText(buttonParams.getString(TAG_BUTTON_LABEL));
        }
        exitBtn.setTextSize(mainActivity.getResources().getDimension(R.dimen.button_textsize));
        exitBtn.setTextColor(Color.BLACK);
        exitBtn.setBackgroundTintList(ColorStateList.valueOf(mainActivity.getResources().getColor(R.color.colorPrimary)));

        if (buttonParams.has(TAG_BUTTON_STYLE)) {
            JSONObject buttonStyle = buttonParams.getJSONObject(TAG_BUTTON_STYLE);
            setViewTextColorFromElem(buttonStyle);
            setViewBgTintColorFromElem(buttonStyle);
        }
    }

    private void setBarParams(JSONObject barParams) throws JSONException {
        sliderBar.getProgressDrawable().setColorFilter(mainActivity.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        sliderBar.getThumb().setColorFilter(mainActivity.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        if (barParams.has(TAG_ELEMENT_STYLE)) {
            JSONObject elementStyle = barParams.getJSONObject(TAG_ELEMENT_STYLE);
            setViewFgColorFromElem(elementStyle);
            setViewBgColorFromElem(elementStyle);
        }
    }

    private void setViewTextColorFromElem(JSONObject elem) throws JSONException {
        if (elem.has(TAG_FOREGROUND_COLOR)) {
            try {
                int color = Color.parseColor(elem.getString(TAG_FOREGROUND_COLOR));
                exitBtn.setTextColor(color);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "Invalid fg color specification: " + elem.getString(TAG_FOREGROUND_COLOR));
            }
        }
    }

    private void setViewBgTintColorFromElem(JSONObject elem) throws JSONException {
        if (elem.has(TAG_BACKGROUND_COLOR)) {
            try {
                int color = Color.parseColor(elem.getString(TAG_BACKGROUND_COLOR));
                exitBtn.setBackgroundTintList(ColorStateList.valueOf(color));
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "Invalid bg color specification: " + elem.getString(TAG_BACKGROUND_COLOR));
            }
        }
    }

    private void setViewFgColorFromElem(JSONObject elem) throws JSONException {
        if (elem.has(TAG_FOREGROUND_COLOR)) {
            try {
                int color = Color.parseColor(elem.getString(TAG_FOREGROUND_COLOR));
                sliderBar.getThumb().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "Invalid fg color specification: " + elem.getString(TAG_FOREGROUND_COLOR));
            }
        }
    }

    private void setViewBgColorFromElem(JSONObject elem) throws JSONException {
        if (elem.has(TAG_BACKGROUND_COLOR)) {
            try {
                int color = Color.parseColor(elem.getString(TAG_BACKGROUND_COLOR));
                sliderBar.getProgressDrawable().setColorFilter(color, PorterDuff.Mode.MULTIPLY);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "Invalid bg color specification: " + elem.getString(TAG_BACKGROUND_COLOR));
            }
        }
    }
}
