package com.wallee.vsd.common.integration.xml;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Namespace(prefix = "vcs-pos", reference = "http://www.vibbek.com/pos")
@Root(name = "beTransmissionRequest", strict = false)
public class BeTransmissionRequest {
    @Element(required = false)
    public String posId;

    public BeTransmissionRequest(String posId) {
        this.posId = posId;
    }
}
