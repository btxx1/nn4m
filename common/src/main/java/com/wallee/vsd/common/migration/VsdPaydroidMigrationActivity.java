package com.wallee.vsd.common.migration;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import androidx.annotation.Nullable;

import com.wallee.vsd.common.ui.CurrentActivity;
import com.wallee.vsd.common.vsd.Updt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * This activity migrates application from `com.wallee.vsd.paydroid` to `com.wallee.android.pinpad`.
 */
public class VsdPaydroidMigrationActivity extends CurrentActivity {
    private static final String TAG = "VsdPaydroidMigrationAct";

    private static final int TYPE_START = 1;
    private static final int TYPE_COMPLETE = 2;
    private static final int TYPE_ROLLBACK = 3;

    private static final String OLD_PACKAGE_NAME = "com.wallee.vsd.paydroid";
    private static final String NEW_PACKAGE_NAME = "com.wallee.android.pinpad";

    private static final String MIGRATION_APK_NAME = "vsdpaydroidmigration.apk";
    private static final String CONFIG_NAME = "config.json";

    private static final File MIGRATION_FOLDER = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

    public static boolean shouldStartMigration(Context context) {
        return context.getApplicationContext().getPackageName().equals(OLD_PACKAGE_NAME) && !areMigrationFilesPresent();
    }

    private static boolean packageExists(Context context, String targetPackage){
        List<ApplicationInfo> packages;
        PackageManager pm;

        try {

            pm = context.getPackageManager();
            packages = pm.getInstalledApplications(0);
            for (ApplicationInfo packageInfo : packages) {
                if (packageInfo.packageName.equals(targetPackage))
                    return true;
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception when using package manager");
        }
        return false;
    }

    private static boolean isCurrentAppConfigured(Context context) {
        boolean configExists = false;
        try {
            configExists = getInternalConfigFile(context).exists();
        } catch (Exception e) {
            Log.e(TAG, "No config files");
        }
        return configExists;
    }

    public static boolean shouldCompleteMigration(Context context) {
        return context.getApplicationContext().getPackageName().equals(NEW_PACKAGE_NAME) && (areMigrationFilesPresent() || isCurrentAppConfigured(context)) && packageExists(context, OLD_PACKAGE_NAME);
    }

    public static boolean shouldRollback(Context context) {
        return !isCurrentAppConfigured(context) && packageExists(context, OLD_PACKAGE_NAME);
    }

    public static boolean areMigrationFilesPresent() {
        return new File(MIGRATION_FOLDER, MIGRATION_APK_NAME).exists() && new File(MIGRATION_FOLDER, CONFIG_NAME).exists();
    }

    public static void startMigration(Context context) {
        startActivity(context, VsdPaydroidMigrationActivity.TYPE_START);
    }

    public static void completeMigration(Context context) {
        startActivity(context, VsdPaydroidMigrationActivity.TYPE_COMPLETE);
    }

    public static void rollbackMigration(Context context) {
        startActivity(context, VsdPaydroidMigrationActivity.TYPE_ROLLBACK);
    }

    private static void startActivity(Context context, int type) {
        Intent intent = new Intent(context, VsdPaydroidMigrationActivity.class);
        intent.putExtra("TYPE", type);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int type = getIntent().getIntExtra("TYPE", -1);
        if (type == TYPE_START) {
            startMigration();
        } else if (type == TYPE_COMPLETE) {
            completeMigration();
        } else if (type == TYPE_ROLLBACK) {
            rollbackMigration();
        } else {
            Log.e(TAG, "Started without TYPE included, finishing...");
            finish();
        }
    }

    private void startMigration() {
        Log.d(TAG, "Migration started...");

        try {
            copyConfigFromInternalToMigrationFolder();
        } catch (Exception e) {
            Log.e(TAG, "No config to be migrated");
        }
        try {
            File apkToInstall = copyMigrationApkFromAssetsToMigrationFolder();
            install(apkToInstall);
            startNewApplication();
            Log.d(TAG, "Migration finished");
        } catch (Exception e) {
            Log.e(TAG, "Migration failed", e);
        }
        finish();
    }

    private void rollbackMigration() {
        Log.d(TAG, "rollback started...");
        try {
            uninstallApplication(NEW_PACKAGE_NAME);
            Log.d(TAG, "rollback finished");
        } catch (Exception e) {
            Log.e(TAG, "Can't rollback");
        }
        finish();
    }

    private void completeMigration() {
        Log.d(TAG, "Completion started...");

        try {
            copyConfigFromMigrationFolderToInternal();
            deleteMigrationFiles();

        } catch (Exception e) {
            Log.e(TAG, "Somethig wrong with the migration files?");
        }

        try {
            uninstallApplication(OLD_PACKAGE_NAME);
        } catch (Exception e) {
            Log.e(TAG, "Can't uninstall old application");
        }
        try {
            startNewApplication();
            Log.d(TAG, "Completion finished");
        } catch (Exception e) {
            Log.e(TAG, "Completion failed", e);
        }
        finish();
    }

    private void copyConfigFromInternalToMigrationFolder() throws Exception {
        File config = getInternalConfigFile(this);
        if (!config.exists()) {
            try {
                config.createNewFile();
                try (FileOutputStream fos = new FileOutputStream(config)) {
                    fos.write("{}".getBytes("UTF-8"));
                }
            } catch (IOException e) {
                Log.e(TAG, "Can't create config file");
                e.printStackTrace();
            }
        }
        File target = new File(MIGRATION_FOLDER, CONFIG_NAME);
        try (InputStream in = new FileInputStream(config); FileOutputStream fos = new FileOutputStream(target)) {
            doCopy(in, fos);
        }
        if (!target.exists()) {
            throw new IOException("Failed to copy config to migration folder");
        }
    }

    private File copyMigrationApkFromAssetsToMigrationFolder() throws Exception {
        File target = new File(MIGRATION_FOLDER, MIGRATION_APK_NAME);
        try (InputStream in = getAssets().open(MIGRATION_APK_NAME); FileOutputStream fos = new FileOutputStream(target)) {
            doCopy(in, fos);
        }
        if (!target.exists()) {
            throw new IOException("Failed to copy migration apk from assets to migration folder");
        }
        return target;
    }

    private void install(File apkFile) throws Exception {
        if (Updt.getInstance().updateFileToApp(apkFile.getAbsolutePath()) != 0) {
            throw new IOException("Failed to install apk: " + apkFile.getAbsolutePath());
        }
    }

    private void startNewApplication() {
        ComponentName componentName = new ComponentName(NEW_PACKAGE_NAME, "com.wallee.android.LauncherActivity");
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setComponent(componentName);
        startActivity(intent);
    }

    private void copyConfigFromMigrationFolderToInternal() throws Exception {
        File target = new File(MIGRATION_FOLDER, CONFIG_NAME);
        File config = getInternalConfigFile(this);
        try (InputStream in = new FileInputStream(target); FileOutputStream fos = new FileOutputStream(config)) {
            doCopy(in, fos);
        }
        if (!config.exists()) {
            throw new IOException("Failed to copy config from migration folder");
        }
    }

    private void uninstallApplication(String packageName) throws IOException {
        if (Updt.getInstance().uninstallApp(packageName) != 0) {
            throw new IOException("Failed to uninstall old application");
        }
    }

    private void deleteMigrationFiles() {
        new File(MIGRATION_FOLDER, MIGRATION_APK_NAME).delete();
        new File(MIGRATION_FOLDER, CONFIG_NAME).delete();
    }

    private static File getInternalConfigFile(Context ctx) {
        File app0dir = new File(ctx.getFilesDir() + "/app0/");
        if (!app0dir.exists()) {
            app0dir.mkdirs();
        }
        return new File(app0dir, CONFIG_NAME);
    }

    private void doCopy(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }
}
