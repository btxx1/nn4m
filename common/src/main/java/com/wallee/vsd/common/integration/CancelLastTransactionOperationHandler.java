package com.wallee.vsd.common.integration;

import com.wallee.android.till.sdk.data.CancelationResult;
import com.wallee.android.till.sdk.data.Receipt;
import com.wallee.android.till.sdk.data.ResultCode;
import com.wallee.android.till.sdk.data.State;
import com.wallee.vsd.common.integration.data.DataUtils;
import com.wallee.vsd.common.integration.xml.ReversalRequest;
import com.wallee.vsd.common.integration.xml.ReversalResponse;
import com.wallee.vsd.common.integration.xml.XmlUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class CancelLastTransactionOperationHandler {
    private final NetClient client = new NetClient();

    CancelationResult handle() {
        if (client.connectWithServer()) {
            try {
                ReversalRequest xmlRequest = createXmlRequest();
                List<Receipt> receipts = new ArrayList<>();
                CancelationResult result = null;

                client.send(XmlUtils.toXml(xmlRequest));
                while (result == null) {
                    String xmlResponse = client.receive();
                    if (xmlResponse.isEmpty()) {
                        result = createErrorResult("Internal error - empty response");
                    } else if (XmlUtils.shouldConfirm(xmlResponse)) {
                        client.send(XmlUtils.CONFIRMATION);
                    } else if (XmlUtils.isPrinterNotification(xmlResponse)) {
                        Receipt receipt = DataUtils.printerNotificationToReceipt(XmlUtils.toPrinterNotification(xmlResponse));
                        if (receipt != null) {
                            receipts.add(receipt);
                        }
                    } else if (XmlUtils.isErrorNotification(xmlResponse)) {
                        result = createErrorResult(XmlUtils.toErrorNotification(xmlResponse).errorText);
                    } else if (XmlUtils.isReversalResponse(xmlResponse)) {
                        result = createResult(receipts, XmlUtils.toReversalResponse(xmlResponse));
                    }
                }

                return result;
            } catch (Exception e) {
                e.printStackTrace();
                return createErrorResult(e.getMessage());
            } finally {
                client.disConnectWithServer();
            }
        } else {
            return createErrorResult("Failed to connect to VPJ");
        }
    }

    private ReversalRequest createXmlRequest() {
        return new ReversalRequest(
                DataUtils.POS_ID
        );
    }

    private CancelationResult createResult(List<Receipt> receipts, ReversalResponse response) {
        return new CancelationResult(
                State.SUCCESSFUL,
                new ResultCode(
                        "0",
                        "Success"
                ),
                response.ep2TrmId,
                DataUtils.stringToLong(response.ep2TrxSeqCnt),
                DataUtils.stringToLong(response.ep2TrxSeqCntOrigTrx),
                response.transactionTime,
                receipts
        );
    }

    private CancelationResult createErrorResult(String error) {
        return new CancelationResult(
                State.FAILED,
                new ResultCode(
                        "error",
                        error
                ),
                null,
                null,
                null,
                null,
                Collections.emptyList()
        );
    }
}
