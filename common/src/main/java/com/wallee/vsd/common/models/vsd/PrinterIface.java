package com.wallee.vsd.common.models.vsd;

public interface PrinterIface {

    int print(String text);

}
