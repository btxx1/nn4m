package com.wallee.vsd.common.models;


public class HardwareSpecification {

    public final boolean isCtlsReaderBehindDisplay;

    private HardwareSpecification(boolean isCtlsReaderBehindDisplay) {
        this.isCtlsReaderBehindDisplay = isCtlsReaderBehindDisplay;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static HardwareSpecification getDefault() {
        return builder().build();
    }

    public static class Builder {
        private boolean isCtlsReaderBehindDisplay;

        public Builder() {
            this.isCtlsReaderBehindDisplay = false;
        }

        public Builder withCtlsReaderBehindDisplay() {
            this.isCtlsReaderBehindDisplay = true;
            return this;
        }

        public HardwareSpecification build() {
            return new HardwareSpecification(isCtlsReaderBehindDisplay);
        }
    }
}
