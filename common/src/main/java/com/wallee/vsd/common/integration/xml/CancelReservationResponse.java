package com.wallee.vsd.common.integration.xml;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Namespace(prefix = "vcs-pos", reference = "http://www.vibbek.com/pos")
@Root(name = "cancelReservationResponse", strict = false)
public class CancelReservationResponse {
    @Element(required = false)
    public String acquirerId;
    @Element(required = false)
    public String amountAuth;
    @Element(required = false)
    public String amountAuthCurr;
    @Element(required = false)
    public String ep2AuthResult;
    @Element(required = false)
    public String ep2TrmId;
    @Element(required = false)
    public String ep2TrxSeqCnt;
    @Element(required = false)
    public String result;
    @Element(required = false)
    public String transactionTime;
    @Element(required = false)
    public String transactionRefNumber;
}
