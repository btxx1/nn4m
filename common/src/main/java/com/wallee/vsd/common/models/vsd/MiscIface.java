package com.wallee.vsd.common.models.vsd;

import android.webkit.WebView;

import androidx.appcompat.app.AppCompatActivity;

import com.wallee.vsd.common.JSReadyListener;
import com.wallee.vsd.common.models.IOReader;

public interface MiscIface {

    void initInstance(AppCompatActivity mainActivity, TcpSocketIface tcpSocketInterface, WebView webView);

    int init();

    String getOsVersion();

    String getBootVersion();

    int updateDescriptors(String readDescriptors, String writeDescriptors, String exceptDescriptors);

    int select(String readDescriptors, String writeDescriptors, String exceptDescriptors, int tv_sec, int tv_usec);

    String selectNow();

    int beep(int freq, int time);

    int setContactReaderLights(boolean status);

    int animate(String commands);

    int sleepMs(long ms);

    int sleepSec(int sec);

    long getCountMs();

    int reboot();

    int rebootIfNeeded();

    int poweroff();

    int mainAppExit();

    /**
     * Restarts the Android application. All activities are finished and the task is removed.
     */
    void mainAppRestart();

    String getHardwareId();

    String getSerialNumber();

    String getAppResPath();

    int createDirectory(String directory);

    int saveFile(String filePath, String buf);

    int saveBufferToFile(String filePath, String buf);

    String getRandom(int len);

    String loadFile(String filePath);

    int deleteFile(String filename);

    long fileSize(String filepath);

    String fileCalculateSha1(String filePath);

    int renameFile(String filename, String newFileName);

    String getUpdateFolderPath();

    boolean fileExists(String filePath);

    int appendFile(String filePath, String  buf, String encoding);

    int setTimer(int timerId, long timeout);

    int cancelTimer(int timerId);

    boolean hasDisplay();

    boolean hasKeyboard();

    boolean hasTouchscreen();

    String hwGetDeviceModel();

    String hwGetHardwarePlatform();

    String getIPAddress();

    String getMask();

    String getGateway();

    String getDNS();

    String getVendorName();


    int getMaxCardholderDisplayLines();

    int getMaxAttendantDisplayLines();

    String getStoredRandom(int index, int len);

    boolean printerCheck();

    int setTime(int currentTime, String timeZone, int timeZoneOffset);

    String getFilesListFromDirectory(String path);

    String getStoredRandom(int index);

    int storeRandom(String data, int index);

    int toggleSystemBars();

    int selectApplicationView();

    String getSimCardNumber();

    String getSimOperatorName();

    void addIOReaderMapping(IOReader ioReader);

    void newEventNotify();

    void postResult(String result);

    void eventLoop() ;

    void restoreSystemBarsStatus();

    void activateSystemBars(boolean activate);

    void activateBackButton(boolean activate);

    void activateAllSystemButtons(boolean activate);

    void addJSReadyListener(JSReadyListener jsReadyListener);

    boolean postEventToJS(final String event, final boolean sendEventDataDirectly);
}

