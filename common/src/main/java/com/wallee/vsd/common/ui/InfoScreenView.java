package com.wallee.vsd.common.ui;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.widget.ListView;

public class InfoScreenView extends ListView {
    private static final String TAG = "InfoScreenView";
    public InfoScreenView(Context context) {
        super(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int specHeightSize = Resources.getSystem().getDisplayMetrics().heightPixels;
        int specWidthSize = MeasureSpec.getSize(widthMeasureSpec);
        setMeasuredDimension(specWidthSize, specHeightSize - 150);
    }
}
