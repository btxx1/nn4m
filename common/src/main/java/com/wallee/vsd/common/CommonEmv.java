package com.wallee.vsd.common;
import android.util.Log;
import android.webkit.JavascriptInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import novelpay.pl.npf.utils.Utils;

public abstract class CommonEmv {
    protected static final String TAG_PATH = "path";
    protected static final String TAG_AC_TYPE = "acType";
    protected static final String TAG_ERROR_TEXT = "errorText";
    protected static final String TAG_TAG = "tag";
    public static final String TAG_RC = "_RC";
    protected static final String TAG_PIN_TYPE = "pinType";
    protected static final String TAG_PINBLOCK = "pinblock";
    protected static final String TAG_RESULT = "result";
    protected String lastErrorText = "";
    private static final String TAG = "CommonEmv";

    abstract protected byte[] getTlvData(long tag);

    protected int setTlvData(long tag, byte[] value) {
        Log.w(TAG, "setTlvData not implemented");
        return 0;
    }

    @JavascriptInterface
    public String getTlvData(String tags) {
        boolean isFirst = true;
        JSONArray array = null;

        try {
            array = new JSONArray(tags);
        } catch (JSONException  e) {
            e.printStackTrace();
            return "{}";
        }

        StringBuffer result = new StringBuffer(4096);
        result.append("{");

        for (int i = 0; i < array.length(); ++i) {
            String tagName = null;
            try {
                tagName = array.getString(i);
            } catch (JSONException e) {
                e.printStackTrace();
                continue;
            }

            int tagId = Integer.parseInt(tagName.substring(1), 16);
            byte[] tagValue = getTlvData(tagId);
            if (tagValue == null) {
                continue;
            }
            if (isFirst) {
                isFirst = false;
            } else {
                result.append(",");
            }
            result.append('"' + tagName + "\":\"" + Utils.bytesToHexString(tagValue, false) + '"');
        }
        result.append("}");
        return result.toString();
    }

    @JavascriptInterface
    public String setTlvData(String tagValues) {
        String key = "";

        try {
            JSONObject reader = new JSONObject(tagValues);
            Iterator<String> keys = reader.keys();

            while(keys.hasNext()) {
                key = keys.next();
                String value = reader.getString(key);
                Log.d(TAG, String.format("tag=%x", Integer.decode("0" + key)));
                int ret = setTlvData((short)Integer.decode("0" + key).intValue(), Utils.hexStringToByteArray(value));
                if (ret != 0) {
                    return String.format("{\"%s\": %d, \"%s\": \"%s\", \"%s\": \"%s\"}",
                            CommonEmv.TAG_RC, ret, CommonEmv.TAG_ERROR_TEXT, lastErrorText, CommonEmv.TAG_TAG, key);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return String.format("{\"%s\": 1, \"%s\": \"%s\", \"%s\": \"%s\"}",
                    CommonEmv.TAG_RC, CommonEmv.TAG_ERROR_TEXT, e.getMessage(), CommonEmv.TAG_TAG, key);
        }
        return String.format("{\"%s\": 0", CommonEmv.TAG_RC);
    }


}
