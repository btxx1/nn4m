package com.wallee.vsd.common.models;

public interface IOReader {
    boolean isEventAvailable();
    int getDescriptor();
    void clearEvent();
}