package com.wallee.vsd.common.integration.xml;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Namespace(prefix = "vcs-pos", reference = "http://www.vibbek.com/pos")
@Root(name = "reversalResponse", strict = false)
public class ReversalResponse {
    @Element(required = false)
    public String ep2TrmId;
    @Element(required = false)
    public String ep2TrxSeqCnt;
    @Element(required = false)
    public String ep2TrxSeqCntOrigTrx;
    @Element(required = false)
    public String ep2TrxTypeExtOrigTrx;
    @Element(required = false)
    public String amountRev;
    @Element(required = false)
    public String amountRevCurr;
    @Element(required = false)
    public String cardNumber;
    @Element(required = false)
    public String cardSeqNumber;
    @Element(required = false)
    public String cardExpDate;
    @Element(required = false)
    public String cardAppLabel;
    @Element(required = false)
    public String cardAppId;
    @Element(required = false)
    public String transactionTime;
    @Element(required = false)
    public String acquirerId;
}
