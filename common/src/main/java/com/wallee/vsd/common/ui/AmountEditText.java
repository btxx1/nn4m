package com.wallee.vsd.common.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class AmountEditText extends androidx.appcompat.widget.AppCompatEditText {

    public AmountEditText(Context context) {
        super(context);
        installListeners();
    }

    public AmountEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        installListeners();
    }

    private void installListeners() {
        setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
    }

    @Override
    public void onSelectionChanged(int start, int end) {

        CharSequence text = getText();
        if (text != null) {
            if (start != text.length() || end != text.length()) {
                setSelection(text.length());
                return;
            }
        }

        super.onSelectionChanged(start, end);
    }

}
