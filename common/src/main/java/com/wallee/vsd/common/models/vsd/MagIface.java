package com.wallee.vsd.common.models.vsd;

public interface MagIface {
    int open();
    int swiped();
    void close();
    void reset();
    String read();
}
