package com.wallee.vsd.common.installer;

import androidx.core.util.Consumer;

import com.wallee.vsd.common.installer.data.DataUtils;
import com.wallee.vsd.common.installer.data.UrlResponse;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

class UrlResponseCallback implements Callback {
    private final Consumer<UrlResponse> responseConsumer;
    private final Consumer<Exception> errorConsumer;

    UrlResponseCallback(Consumer<UrlResponse> responseConsumer, Consumer<Exception> errorConsumer) {
        this.responseConsumer = responseConsumer;
        this.errorConsumer = errorConsumer;
    }

    @Override
    public void onFailure(@NotNull Call call, @NotNull IOException e) {
        errorConsumer.accept(e);
    }

    @Override
    public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
        UrlResponse result;
        try {
            if (response.isSuccessful()) {
                result = DataUtils.toUrlResponse(response.body().string());
                if (result != null) {
                    responseConsumer.accept(result);
                }
            } else {
                errorConsumer.accept(new IOException(response.message()));
            }
        } catch (Exception e) {
            errorConsumer.accept(e);
        }
    }
}
