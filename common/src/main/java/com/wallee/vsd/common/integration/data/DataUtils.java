package com.wallee.vsd.common.integration.data;

import com.wallee.android.till.sdk.data.Receipt;
import com.wallee.android.till.sdk.data.ReceiptFormat;
import com.wallee.android.till.sdk.data.ReceiptType;
import com.wallee.android.till.sdk.data.ResultCode;
import com.wallee.android.till.sdk.data.State;
import com.wallee.vsd.common.integration.xml.PrinterNotification;

import java.math.BigDecimal;
import java.util.Currency;

public final class DataUtils {
    public static final String POS_ID = "Android Till-SDK";

    private DataUtils() {
    }

    public static CurrencyType getCurrencyType(Currency currency) {
        for (CurrencyType type : CurrencyType.values()) {
            if (type.name().equals(currency.getCurrencyCode())) {
                return type;
            }
        }
        return null;
    }

    public static State authResultToState(String authResult) {
        if (authResult != null) {
            try {
                int value = Integer.parseInt(authResult);
                return value >= 0 && value <= 99 ? State.SUCCESSFUL : State.FAILED;
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return State.FAILED;
    }

    public static ResultCode authResultToResultCode(String authResult) {
        if (authResult != null) {
            try {
                AuthorizationCode code = AuthorizationCode.find(Integer.parseInt(authResult));
                return new ResultCode(authResult, code.description);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return new ResultCode("error", "Failed to get authResult");
    }

    public static Long stringToLong(String value) {
        return value != null ? Long.parseLong(value) : null;
    }

    public static Long toMinorUnits(BigDecimal amount, CurrencyType currency) {
        return amount.movePointRight(currency.exponent).longValue();
    }

    public static boolean isNegative(BigDecimal amount) {
        return amount.signum() < 0;
    }

    public static Receipt printerNotificationToReceipt(PrinterNotification printerNotification) {
        if (printerNotification.merchantReceipt != null) {
            return new Receipt(printerNotification.merchantReceipt, ReceiptType.MERCHANT, ReceiptFormat.TEXT);
        } else if (printerNotification.cardholderReceipt != null) {
            return new Receipt(printerNotification.cardholderReceipt, ReceiptType.CLIENT, ReceiptFormat.TEXT);
        } else {
            return null;
        }
    }
}
