package com.wallee.vsd.common;

public class Util {
    public static int[] getIntArrayFromCsv(String param) {
        if (param == null) {
            return null;
        }
        String strings[] = param.split("\\|");
        if (strings == null || strings.length == 0) {
            return null;
        }
        int[] result = new int[strings.length];

        for (int i = 0; i < strings.length; i++) {
            try {
                result[i] = Integer.decode(strings[i]);
            } catch (NumberFormatException e) {
                result[i] = -1;
            }
        }
        return result;
    }

    final protected static char[] hexArray = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        int v;

        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }

        return new String(hexChars);
    }


    public static byte[] hexToBytes(String s) {
        int len = s.length();
        byte[] data = new byte[len/2];

        for(int i = 0; i < len; i+=2){
            data[i/2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i+1), 16));
        }

        return data;
    }
}
