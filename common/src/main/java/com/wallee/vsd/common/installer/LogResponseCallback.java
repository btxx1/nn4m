package com.wallee.vsd.common.installer;

import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

class LogResponseCallback implements Callback {
    private static final String TAG = "LogResponseCallback";

    @Override
    public void onFailure(@NotNull Call call, @NotNull IOException e) {
        Log.d(TAG, "Failure for url: " + call.request().url(), e);
    }

    @Override
    public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
        Log.d(TAG, "Response: " + response.body().string());
    }
}
