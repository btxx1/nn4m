package com.wallee.vsd.common.vsd;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.webkit.JavascriptInterface;

import com.wallee.vsd.common.models.vsd.TcpSocketIface;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

import static com.wallee.vsd.common.vsd.XG.TAG_ADDR;
import static com.wallee.vsd.common.vsd.XG.TAG_FD;
import static com.wallee.vsd.common.vsd.XG.TAG_PORT;
import static com.wallee.vsd.common.vsd.XG.TAG_RC;


public class TcpSocket implements TcpSocketIface {

    private final ConnectivityManager connectivityManager;

    private class Connection {
        private static final int INITIAL_BUFFER_SIZE = 8*1024;
        private static final int MAX_BUFFER_SIZE = 128*1024;

        int fd;
        private SocketChannel socket;
        private ServerSocketChannel serverSocket;
        private SelectionKey selectionKey;
        private int timeoutMs = 30 * 1000;
        private int bufferSize = INITIAL_BUFFER_SIZE;
        private ByteBuffer buf = null;

        public Connection(int fd) {
            this.fd = fd;
            try {
                this.socket = SocketChannel.open();
                this.socket.configureBlocking(false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public Connection(int fd, SocketChannel socketChannel) {
            this.fd = fd;
            this.socket = socketChannel;
            try {
                this.socket.configureBlocking(false);
            } catch (IOException e) {
                Log.e(TAG, "Failed to configure blocking: " + e, e);
            }
        }

        int connect(String hostname, int port) {
            try {
                logInfo("Connecting [" + this.socket + "] to host " + hostname + ":" + port + ", timeout: " + timeoutMs + " ms");
                long timeIsUp = System.currentTimeMillis() + timeoutMs;

                waitForNetworkUntil(timeIsUp);
                this.socket.connect(new InetSocketAddress(hostname, port));
                finishConnectUntil(timeIsUp);
                socket.configureBlocking(false);
                logInfo("Connection OK");
            } catch (Exception e) {
                logError("Can't connect to host: " + e);
                return -1;
            }
            return 0;
        }

        private void waitForNetworkUntil(long timeout) throws InterruptedException {
            boolean wasNetworkDown = false;

            while (System.currentTimeMillis() < timeout) {
                NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
                if (activeNetwork != null && activeNetwork.isConnected()) {
                    break;
                }
                if (!wasNetworkDown) {
                    Log.i(TAG, "Network not available");
                    wasNetworkDown = true;
                }
                Thread.sleep(100);
            }

            final int additionalTimeMs = 2000; // On A920, connect still fails once network is available
            if (wasNetworkDown && System.currentTimeMillis() + additionalTimeMs < timeout) {
                Log.i(TAG, "Network available, waiting additional " + additionalTimeMs + " ms");
                Thread.sleep(additionalTimeMs);
            } else if (wasNetworkDown) {
                Log.w(TAG, "Network available, but connect might still fail");
            }
        }

        private void finishConnectUntil(long timeIsUp) throws Exception {
            while (!this.socket.finishConnect()) {
                if (System.currentTimeMillis() > timeIsUp) {
                    throw new Exception("Connection timed out");
                }
                Thread.sleep(100);
            }
        }

        int close() {
            try {
                if (socket != null) {
                    socket.close();
                }
                if (serverSocket != null) {
                    if (this.selectionKey != null) {
                        this.selectionKey.cancel();
                        this.selectionKey.selector().selectNow();
                    }
                    serverSocket.close();
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception when closing connection:" + e);
                e.printStackTrace();
                return -1;
            }
            socket = null;
            serverSocket = null;
            return 0;
        }

        int send(byte[] message) {
            try {
                ByteBuffer buf = ByteBuffer.wrap(message);
                while (buf.hasRemaining()) {
                    socket.write(buf);
                }
            } catch (IOException e) {
                e.printStackTrace();
                return -1;
            }
            return 0;
        }

        byte[] receive() {
            if (buf == null) {
                buf = ByteBuffer.allocate(this.bufferSize);
            } else {
                buf.clear();
            }

            try {
                int bytesRead = socket.read(buf);
                if (bytesRead == 0) {
                    buf.clear();
                    return new byte[0];
                } else if (bytesRead < 0) {
                    buf.clear();
                    return null;
                }
                byte[] bytes = new byte[bytesRead];
                buf.rewind();
                buf.get(bytes, 0, bytesRead);
                buf.clear();
                logInfo("Read " + bytes.length + " bytes");
                if (bytesRead == this.bufferSize && this.bufferSize <= MAX_BUFFER_SIZE/2) {
                    this.bufferSize *= 2; // dynamically increase the buffer for next read operations
                    Log.d(TAG, "Buffer increased to " + this.bufferSize/1024 + " kB");
                    buf = ByteBuffer.allocate(this.bufferSize);
                }
                return bytes;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        int bind(String address, int port) {
            try {
                if (serverSocket == null) {
                    serverSocket = ServerSocketChannel.open();
                    Log.d(TAG, "Created serverSocket: " + serverSocket);
                }
                serverSocket.socket().bind(new InetSocketAddress(address, port));
                serverSocket.configureBlocking(false);
            } catch (IOException e) {
                e.printStackTrace();
                return -1;
            }
            return 0;
        }

        int register(Selector selector, int ops) {
            try {
                if (serverSocket != null) {
                    // serverSocket.configureBlocking(false);
                    selectionKey = serverSocket.register(selector, SelectionKey.OP_ACCEPT);
                } else if (socket != null) {
                    // socket.configureBlocking(false);
                    selectionKey = socket.register(selector, ops & (~SelectionKey.OP_ACCEPT));
                }
            } catch (Exception e) {
                e.printStackTrace();
                return -1;
            }

            return 0;
        }

        public boolean isConnected() {
            if (socket == null) {
                return false;
            }
            return socket.isConnected();
        }
    }

    public static final int MAX_SOCKETS = 10;
    private static final int FIRST_SEQ = 20000;
    private static final int LAST_SEQ = 1999999999;
    private static final String TAG = "TcpSocket";

    void logError(String msg) {
        Log.e(TAG, msg);
    }

    void logInfo(String msg) {
        Log.i(TAG, msg);
    }

    Connection[] connections;
    int fd_seq = FIRST_SEQ;

    public TcpSocket(ConnectivityManager connectivityManager) {
        this.connectivityManager = connectivityManager;
        connections = new Connection[MAX_SOCKETS];
    }


    @Override
    @JavascriptInterface
    synchronized public int create() {
        logInfo("Creating socket...");
        return create(null);
    }

    synchronized private int create(SocketChannel socketChannel) {
        int fd;
        for (fd = fd_seq; fd < fd_seq + MAX_SOCKETS; ++fd) {
            if (connections[fd % MAX_SOCKETS] == null) {
                break;
            }
        }
        if (fd == fd_seq + MAX_SOCKETS) {
            logError("Can't create more sockets");
            return -1;
        }
        Connection conn = null;
        fd_seq = fd + 1;
        if (fd_seq > LAST_SEQ) {
            Log.d(TAG, "Sequence counter reset back to " + FIRST_SEQ);
            fd_seq = FIRST_SEQ;
        }
        if (socketChannel != null) {
            conn = new Connection(fd, socketChannel);
        } else {
            conn = new Connection(fd);
        }
        connections[fd % MAX_SOCKETS] = conn;
        logInfo("Created socket idx:" + (fd % MAX_SOCKETS) + ", fd:" + fd + " (this is: " + this + ")");
        return fd;
    }


    @Override
    @JavascriptInterface
    synchronized public int connect(int fd, String hostname, int port) {
        logInfo("connect...");
        if (fd < 0 || connections[fd % MAX_SOCKETS] == null || connections[fd % MAX_SOCKETS].fd != fd) {
            logError("Wrong socket number:" + fd);
            return -1;
        }
        Connection conn = connections[fd % MAX_SOCKETS];
        if (conn == null) {
            logError("Socket not created:" + MAX_SOCKETS);
            return -1;
        }
        return conn.connect(hostname, port);
    }

    @Override
    @JavascriptInterface
    synchronized public int close(int fd) {
        if (fd < 0 || connections[fd % MAX_SOCKETS] == null || connections[fd % MAX_SOCKETS].fd != fd) {
            logError("invalid socket descriptor: " + fd);
            return -1;
        }
        Connection conn = connections[fd % MAX_SOCKETS];
        if (conn == null) {
            return 0;
        }
        conn.close();
        Log.d(TAG, String.format("Closed connection fd:%d", fd));
        connections[fd % MAX_SOCKETS] = null;
        return 0;
    }

    synchronized public void closeAll() {
        for (int i = 0; i < MAX_SOCKETS; i++) {
            Connection conn = connections[i];
            if (conn != null) {
                conn.close();
                connections[i] = null;
            }
        }
    }

    @Override
    @JavascriptInterface
    public int send(int fd, String msg) {
        return sendString(fd, msg, "utf-8");
    }

    @Override
    @JavascriptInterface
    public int sendIsoString(int fd, String msg) {
        return sendString(fd, msg, "windows-1252");
    }

    private int sendString(int fd, String msg, String charsetName) {
        if (fd < 0 || connections[fd % MAX_SOCKETS] == null || connections[fd % MAX_SOCKETS].fd != fd) {
            logError("invalid socket descriptor: " + fd);
            return -1;
        }
        Connection conn = connections[fd % MAX_SOCKETS];
        if (conn == null) {
            logError("connection fd: " + fd + " is null");
            for (int i = 0; i < MAX_SOCKETS; ++i) {
                if (connections[i] != null) {
                    Log.d(TAG, "    " + i + ": fd:" + connections[i].fd + " -> " + connections[i]);
                }
            }
            return -1;
        }

        byte[] bytes = null;
        try {
            bytes = msg.getBytes(charsetName);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return -1;
        }
        int res = conn.send(bytes);
        if (res < 0) {
            logError("conn.send returned error");
        }
        return res;
    }

    @Override
    @JavascriptInterface
    public String receive(int fd) {
        return receive(fd, null);
    }

    @Override
    @JavascriptInterface
    public String receive(int fd, String encoding) {
        if (encoding == null) {
            encoding = "windows-1252";
        }
        if (fd < 0 || connections[fd % MAX_SOCKETS] == null || connections[fd % MAX_SOCKETS].fd != fd) {
            logError("invalid socket descriptor: " + fd);
            return null;
        }
        Connection conn = connections[fd % MAX_SOCKETS];
        if (conn == null) {
            logError("Connection " + fd + "is not open");
            return null;
        }
        byte[] bytes = conn.receive();
        if (bytes == null) {
            return null;
        }
        try {
            String result = new String(bytes, encoding);
            Log.d(TAG, String.format("Received %d bytes", result.length()));
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    @JavascriptInterface
    synchronized public int setBlocking(int fd, boolean blocking) {
        if (fd < 0 || connections[fd % MAX_SOCKETS] == null || connections[fd % MAX_SOCKETS].fd != fd) {
            logError("invalid socket descriptor: " + fd);
            return -1;
        }
        try {
            connections[fd % MAX_SOCKETS].socket.configureBlocking(blocking);
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
        return 0;
    }

    @Override
    @JavascriptInterface
    synchronized public int bind(int fd, String address, int port) {
        if (fd < 0 || connections[fd % MAX_SOCKETS] == null || connections[fd % MAX_SOCKETS].fd != fd) {
            logError("invalid socket descriptor: " + fd);
            return -1;
        }
        return connections[fd % MAX_SOCKETS].bind(address, port);
    }

    @Override
    @JavascriptInterface
    public int listen(int fd, int maxPending) {
        return 0;
    }

    @Override
    @JavascriptInterface
    synchronized public String accept(int fd) {
        String errorStr = String.format("{\"%s\":-1}", TAG_RC);
        if (fd < 0 || connections[fd % MAX_SOCKETS] == null || connections[fd % MAX_SOCKETS].fd != fd) {
            logError("invalid socket descriptor: " + fd);
            return errorStr;
        }
        if (connections[fd % MAX_SOCKETS].serverSocket == null) {
            logError("fd:" + fd + " has no serverSocket");
            return errorStr;
        }

        try {
            SocketChannel sc = connections[fd % MAX_SOCKETS].serverSocket.accept();
            if (sc == null) {
                return errorStr;
            }
            int newFd = create(sc);
            if (newFd < 0) {
                return errorStr;
            }
            return String.format("{\"%s\":0,\"%s\":%d,\"%s\":%d,\"%s\":\"%s\"}",
                    TAG_RC, TAG_FD, newFd, TAG_PORT, sc.socket().getPort(), TAG_ADDR,
                    sc.socket().getInetAddress().getHostAddress());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return errorStr;
    }

    @Override
    @JavascriptInterface
    public int setTimeout(int fd, int timeoutMs) {
        if (fd < 0 || connections[fd % MAX_SOCKETS] == null || connections[fd % MAX_SOCKETS].fd != fd) {
            logError("invalid socket descriptor: " + fd);
            return -1;
        }
        Connection conn = connections[fd % MAX_SOCKETS];
        if (conn == null) {
            logError("connection fd: " + fd + " is null");
            for (int i = 0; i < MAX_SOCKETS; ++i) {
                if (connections[i] != null) {
                    Log.d(TAG, "    " + i + ": fd:" + connections[i].fd + " -> " + connections[i]);
                }
            }
            return -1;
        }

        conn.timeoutMs = timeoutMs;
        return 0;
    }

    @Override
    @JavascriptInterface
    synchronized public boolean isConnecting(int fd) {
        if (fd < 0 || connections[fd % MAX_SOCKETS] == null || connections[fd % MAX_SOCKETS].fd != fd) {
            logError("invalid socket descriptor: " + fd);
            return false;
        }
        return connections[fd % MAX_SOCKETS].socket.isConnectionPending();
    }

    @Override
    @JavascriptInterface
    public int getLastErrorCode() {
        ///TODO
        return 0;
    }


    private boolean removeIfExists(int[] fds, int fd) {
        if (fds == null || fd == -1) {
            return false;
        }
        boolean result = false;
        for (int i = 0; i < fds.length; i++) {
            if (fds[i] == fd) {
                result = true;
                fds[i] = -1;
            }
        }
        return result;
    }

    synchronized private int registerChannel(Selector selector, int ops, int fd) {
        if (fd < 0 || connections[fd % MAX_SOCKETS] == null || connections[fd % MAX_SOCKETS].fd != fd) {
            logError("invalid socket descriptor: " + fd);
            return -1;
        }
        if (connections[fd % MAX_SOCKETS].register(selector, ops) == 0) {
            connections[fd % MAX_SOCKETS].selectionKey.attach(new Integer(fd));
            return 0;
        }
        logError(String.format("Can't register descriptor %d for ops %X", fd, ops));
        return -1;
    }

    synchronized public int register(Selector selector, int[] readFds, int[] writeFds, int[] exceptionFds) {

        if (readFds != null) {
            for (int i = 0; i < readFds.length; i++) {
                if (readFds[i] < 0) {
                    continue;
                }

                if (connections[readFds[i] % MAX_SOCKETS] == null) {
                    Log.w(TAG, "trying to register on not existing connection fd:" + readFds[i]);
                    continue;
                }
                int selectionKey = connections[readFds[i] % MAX_SOCKETS].isConnected() ? SelectionKey.OP_READ :
                        SelectionKey.OP_ACCEPT | SelectionKey.OP_CONNECT;
                if (removeIfExists(writeFds, readFds[i])) {
                    if (connections[readFds[i] % MAX_SOCKETS].isConnected()) {
                        selectionKey |= SelectionKey.OP_WRITE;
                    }
                }
                if (registerChannel(selector, selectionKey, readFds[i]) != 0) {
                    logError(String.format("Failed to register fd %d with operations %x", readFds[i], selectionKey));
                }
            }
        }
        if (writeFds != null) {
            for (int i = 0; i < writeFds.length; i++) {
                int selectionKey = SelectionKey.OP_WRITE;
                if (writeFds[i] < 0) {
                    continue;
                }
                if (registerChannel(selector, selectionKey, writeFds[i]) != 0) {
                    logError(String.format("Failed to register fd %d with operations %x", writeFds[i], selectionKey));
                }
            }
        }

        return 0;
    }
}
