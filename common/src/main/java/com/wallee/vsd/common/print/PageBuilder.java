package com.wallee.vsd.common.print;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;

import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;

import com.pax.gl.page.IPage;
import com.pax.gl.page.PaxGLPage;

import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;


public class PageBuilder {
    final static String TAG = "PageBuilder";
    private static org.slf4j.Logger Logger = LoggerFactory.getLogger(PageBuilder.class);
    private PaxGLPage paxGLPage;
    private int counter = 0;
    private List<IPage> pageList = new ArrayList<>();

    private IPage page;

    protected PageBuilder(PaxGLPage paxGLPage) {
        this.paxGLPage = paxGLPage;
        this.page = paxGLPage.createPage();
        pageList.add(this.page);
    }

    public static PageBuilder create(PaxGLPage paxGLPage) {
        return new PageBuilder(paxGLPage);
    }

    public PageBuilder setFont(String pathToFont) {
        this.page.setTypeFace(pathToFont);
        return this;
    }

    public String getFont() {
        return this.page.getTypeFace();
    }

    public void finalizeObject() {
        paxGLPage = null;
        pageList = null;
        page = null;
        System.gc();
    }

    private void countUp(int lines) {
        counter+=lines;
        if (counter > 50) {
            counter = 0;
            page = paxGLPage.createPage();
            pageList.add(page);
        }
        pageList.set(pageList.size() - 1, page);
    }

    public PageBuilder addStringInvert(String stringBitmap) {
        // black background and white string
        Bitmap bitmap1 = Bitmap.createBitmap(500, 25, Bitmap.Config.ARGB_8888);
        Paint textPaint = new Paint();
        Canvas canvas1 = new Canvas(bitmap1);

        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(26);
        canvas1.drawColor(Color.BLACK, PorterDuff.Mode.SRC_OUT);
        canvas1.drawText(stringBitmap, 0, 25, textPaint);

        page.addLine().addUnit(bitmap1, IPage.EAlign.LEFT);
        countUp(1);
        return this;
    }

    public PageBuilder addStringInvert(String text, IPage.EAlign align) {
        Bitmap bitmap = Bitmap.createBitmap(380, 25, Bitmap.Config.ARGB_8888);
        Paint textPaint = new Paint();
        Rect result = new Rect();
        Canvas canvas = new Canvas(bitmap);

        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(26);
        textPaint.getTextBounds(text, 0, text.length(), result);
        canvas.drawColor(Color.BLACK, PorterDuff.Mode.SRC_OUT);

        switch (align) {
            case LEFT:
                canvas.drawText(text, 0, 25, textPaint);
                break;
            case CENTER:
                canvas.drawText(text, (bitmap.getWidth() / 2) - (result.width() / 2), 25, textPaint);
                break;
            case RIGHT:
                canvas.drawText(text, bitmap.getWidth() - result.width(), 25, textPaint);
                break;
        }

        page.addLine().addUnit(bitmap, align);
        countUp(1);
        return this;
    }

    public PageBuilder addStringInvert(String text, int fontSize, IPage.EAlign align) {
        Bitmap bitmap = Bitmap.createBitmap(380, fontSize - 1, Bitmap.Config.ARGB_8888);
        Paint textPaint = new Paint();
        Rect result = new Rect();
        Canvas canvas = new Canvas(bitmap);

        if (align == IPage.EAlign.CENTER)
            text = text.trim();
        textPaint.setColor(Color.WHITE);
        textPaint.setFakeBoldText(true);
        textPaint.setTextSize(fontSize);
        textPaint.getTextBounds(text, 0, text.length(), result);
        canvas.drawColor(Color.BLACK, PorterDuff.Mode.SRC_OUT);

        switch (align) {
            case LEFT:
                canvas.drawText(text, 0, fontSize - 1, textPaint);
                break;
            case CENTER:
                canvas.drawText(text, (bitmap.getWidth() / 2) - (result.width() / 2), fontSize - 4, textPaint);
                break;
            case RIGHT:
                canvas.drawText(text, bitmap.getWidth() - result.width(), fontSize - 1, textPaint);
                break;
        }

        page.addLine().addUnit(bitmap, align);
        countUp(1);
        return this;
    }

    public PageBuilder addTallString(String text, int fontSize, boolean isBold, IPage.EAlign align) {
        Bitmap bitmap = Bitmap.createBitmap(380, fontSize - 1, Bitmap.Config.ARGB_8888);
        Paint textPaint = new Paint();
        Rect result = new Rect();
        Canvas canvas = new Canvas(bitmap);

        textPaint.setColor(Color.BLACK);
        if (isBold == true)
            textPaint.setFakeBoldText(true);
        textPaint.setTextSize(fontSize);
        textPaint.getTextBounds(text, 0, text.length(), result);

        switch (align) {
            case LEFT:
                canvas.drawText(text, 0, fontSize - 1, textPaint);
                break;
            case CENTER:
                canvas.drawText(text, (bitmap.getWidth() / 2) - (result.width() / 2), fontSize - 4, textPaint);
                break;
            case RIGHT:
                canvas.drawText(text, bitmap.getWidth() - result.width(), fontSize - 1, textPaint);
                break;
        }

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        Bitmap nBitmap = Bitmap.createScaledBitmap(bitmap, width, height * 2, false);
        bitmap.recycle();

        page.addLine().addUnit(nBitmap, align);
        countUp(2);
        return this;
    }

    public Bitmap getBitmap() {
        return paxGLPage.pageToBitmap(page, 384);
    }

    public Bitmap getBitmapFromPage(IPage page) {
        return paxGLPage.pageToBitmap(page, 384);
    }

    public List<IPage> getPageList() {
        return pageList;
    }

    public PageBuilder addTextLine(String text, int fontSize, IPage.EAlign align) {
        page.addLine().addUnit(text, fontSize, align);
        countUp(countLines(text));
        return this;
    }

    public PageBuilder addTextLine(String text, int fontSize, IPage.EAlign align, int style) {
        page.addLine().addUnit(text, fontSize, align, style);
        countUp(countLines(text));
        return this;
    }

    public PageBuilder addTextLine(String text, int fontSize, float scaleX, float scaleY, IPage.EAlign align, float fontWeight) {
        page.addLine().addUnit(text, fontSize, scaleX, scaleY, align, fontWeight);
        countUp(countLines(text));
        return this;
    }

    public PageBuilder addBitmap(Bitmap bitmap, IPage.EAlign align, int fontSize) {
        int lines = 1;
        page.addLine().addUnit(bitmap, align);
        if (fontSize > 0) {
            int height = bitmap.getHeight();
            lines = (int) Math.ceil((double)height / (fontSize * 1.5));
        }
        countUp(lines);
        return this;
    }


    private String formatStringTo2Lines(String toFormat, int length) {
        String[] leftParts = toFormat.split(" ");
        int counter = 0;
        String outputString = "";
        for (int i = 0; i < leftParts.length; i++) {
            counter += leftParts[i].length();
            if (counter > length) {
                counter = 0;
                outputString += "\n" + leftParts[i] + " ";
            } else {
                outputString += leftParts[i] + " ";
            }
        }
        return outputString;
    }

    public IPage getCurrentPage() {
        return this.page;
    }

    private int countLines(String str) {
        if(str == null || str.isEmpty())
        {
            return 0;
        }
        int lines = 0;
        int pos = 0;
        while ((pos = str.indexOf("\n", pos) + 1) != 0) {
            lines++;
        }
        return lines;
    }
}
