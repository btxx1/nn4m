package com.wallee.vsd.common.installer.data;

public class AppReport {
    public final String action;
    public final String serialNumber;
    public final String applicationId;

    public AppReport(String action, String serialNumber, String applicationId) {
        this.action = action;
        this.serialNumber = serialNumber;
        this.applicationId = applicationId;
    }
}
