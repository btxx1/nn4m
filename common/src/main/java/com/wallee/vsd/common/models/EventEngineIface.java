package com.wallee.vsd.common.models;

public interface EventEngineIface {

    void notifyPageReady();

    int notifyJSReady();

    int vsdNotifyJSReady();
}
