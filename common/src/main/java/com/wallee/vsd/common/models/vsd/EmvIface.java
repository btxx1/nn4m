package com.wallee.vsd.common.models.vsd;

public interface EmvIface {
    int init();
    int updateConfig(String emvConfig, int currencyCode, int currencyExponent, int transactionType);
    String applicationSelection(int transNr);
    int readApplicationData();
    int cardDataAuthentication();
    String setTlvData(String tagValues);
    int startTransaction(final long amountAuthorised, final long amountOther, final int pinKeyIdx, final int pinEntryTimeout);
    String completeTransaction(int onlineStatus, String issuerScript);
    int selectApplication(int appNo);
}
