package com.wallee.vsd.common.installer;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.wallee.vsd.common.installer.data.AppReport;
import com.wallee.vsd.common.installer.data.DataUtils;
import com.wallee.vsd.common.installer.data.UrlApplication;
import com.wallee.vsd.common.installer.data.UrlResponse;
import com.wallee.vsd.common.integration.ApiHandler;
import com.wallee.vsd.common.models.vsd.UpdtIface;
import com.wallee.vsd.common.vsd.Misc;
import com.wallee.vsd.common.vsd.Updt;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class AppsInstaller {
    private static final String TAG = "AppsInstaller";

    private final Context context;
    private final NetClient client;
    private final UpdtIface updater;
    private final AtomicInteger appsToInstall;
    private AtomicBoolean appInstalled = new AtomicBoolean(false);

    private String reportUrl; // general for every app

    public AppsInstaller(Context context, NetClient netClient, UpdtIface updtIface) {
        this.context = context;
        this.client = netClient;
        this.updater = updtIface;
        this.appsToInstall = new AtomicInteger(0);
    }

    // sample URL: https://dev.customweb.com/hunziker/android-download/index.php?c=blenny
    public void check(String url) {
        Log.d(TAG, "Check started for url: " + url);
        String ulrParameters = "&platform=" + Misc.getInstance().hwGetHardwarePlatform().toLowerCase();
        client.get(
                url + ulrParameters,
                new UrlResponseCallback(
                        this::handleUrlResponse,
                        this::handleUrlResponseError
                )
        );
    }

    public void handleUrlResponse(UrlResponse urlResponse) {
        Log.d(TAG, "handleUrlResponse");
        List<UrlApplication> appsToDownload = new ArrayList<>();

        if (urlResponse.applications != null) {
            for (UrlApplication app : urlResponse.applications) {
                PackageInfo packageInfo = findPackageInfo(app.applicationId);
                if (packageInfo == null || !app.version.equals(packageInfo.versionName)) {
                    appsToDownload.add(app);
                }
            }
        } else {
            Log.e(TAG, "No applications in response");
        }

        if (!appsToDownload.isEmpty()) {
            reportUrl = urlResponse.reportUrl;
            appsToInstall.set(appsToDownload.size());
            for (UrlApplication app : appsToDownload) {
                client.get(
                        app.downloadUrl,
                        new DownloadFileCallback(
                                context,
                                DataUtils.getApkFileName(app.downloadUrl),
                                it -> handleDownloadedApk(it, app.applicationId),
                                it -> handleDownloadError(it, app.applicationId)
                        ));
            }
        }
    }

    public void handleUrlResponseError(Exception e) {
        Log.d(TAG, "handleUrlResponseError", e);
    }

    public void handleDownloadedApk(File apkFile, String applicationId) {
        Log.d(TAG, "Downloaded apk: " + apkFile.getAbsolutePath() + " for: " + applicationId);
        if (updater.updateFileToApp(apkFile.getAbsolutePath()) == 0) {
            Log.d(TAG, "Successfully installed apk");
            appInstalled.set(true);
            client.post(
                    reportUrl,
                    DataUtils.toJson(new AppReport("installed", DataUtils.getSerial(), applicationId)),
                    new LogResponseCallback()
            );
        } else {
            Log.d(TAG, "Failed to install apk");
        }

        if (appInstalled.get() && appsToInstall.decrementAndGet() == 0) {
            finishCheck();
        }
    }

    public void handleDownloadError(Exception e, String applicationId) {
        Log.d(TAG, "Failed to download apk for: " + applicationId, e);
    }

    public void finishCheck() {
       Log.d(TAG, "Check finished");
       ApiHandler.launchTillApplication(context);
    }

    public PackageInfo findPackageInfo(String applicationId) {
        try {
            return context.getPackageManager().getPackageInfo(applicationId, PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }
}
