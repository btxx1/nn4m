package com.wallee.vsd.common.ui;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wallee.vsd.common.R;

import java.util.ArrayList;

public class InfoScreenAdapter extends BaseAdapter {
    private static final String TAG = "InfoScreenAdapter";
    private ArrayList<String> items;
    private Context context;
    LayoutInflater inflter;
    private View[] views;
    public static final String BOLD_TAG = "#?";
    private float fontSize = 12f;
    String fontColor = "#000000";
    String supportColor = "#ffffff";

    public InfoScreenAdapter(Context context, ArrayList<String> items, float fontSize, String fontColor, String supportColor) {
        this.context = context;
        this.items = items;
        this.views = new View[items.size()];
        this.fontSize = fontSize;
        this.fontColor = fontColor;
        this.supportColor = supportColor;
        inflter = (LayoutInflater.from(context));
        //showContent();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = inflter.inflate(R.layout.infoscreen_item, null);
        String itemdef = items.get(position);
        if (views[position] == null) {
            TextView tv = (TextView) v.findViewById(R.id.textItem);
            tv.setVisibility(View.VISIBLE);
            tv.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
            tv.setTextSize(fontSize);
            tv.setTextColor(Color.parseColor(fontColor));
            String header = checkIfHeader(itemdef);
            if (header != null) {
                itemdef = header;
                tv.setTypeface(null, Typeface.BOLD);
                tv.setBackgroundColor(Color.parseColor(supportColor));
            }
            tv.setText(itemdef);
            views[position] = tv;
        }
        return views[position];
    }

    private String checkIfHeader(String itemdef) {
        if (itemdef.isEmpty() || itemdef.length() < BOLD_TAG.length())
            return null;
        if (itemdef.substring(0, BOLD_TAG.length()).indexOf(BOLD_TAG) != 0)
            return null;
        return itemdef.substring(BOLD_TAG.length());
    }

    public void showContent() {
        Log.d(TAG, "MenuAdapter content:");
        for (int i = 0; i < items.size(); ++i) {
            Log.d(TAG, "        . " + items.get(i));
        }
    }
}
