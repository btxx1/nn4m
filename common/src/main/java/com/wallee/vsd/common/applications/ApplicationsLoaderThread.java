package com.wallee.vsd.common.applications;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;

import androidx.core.content.ContextCompat;

import com.wallee.vsd.common.applications.data.LauncherEntry;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

class ApplicationsLoaderThread extends Thread {
    private final WeakReference<ApplicationsActivity> weakActivity;
    private final Context context;
    private final PackageManager packageManager;

    ApplicationsLoaderThread(ApplicationsActivity activity) {
        this.weakActivity = new WeakReference<>(activity);
        this.context = activity.getApplicationContext();
        this.packageManager = activity.getPackageManager();
    }

    @Override
    public void run() {
        List<LauncherEntry> launcherEntries = new ArrayList<>();

        Intent main = new Intent(Intent.ACTION_MAIN, null);
        main.addCategory(Intent.CATEGORY_LAUNCHER);

        for (ResolveInfo activity : packageManager.queryIntentActivities(main, 0)) {
            ActivityInfo activityInfo = activity.activityInfo;

            if (!activityInfo.packageName.equals(context.getPackageName())) {
                String applicationName = activityInfo.loadLabel(packageManager).toString();
                ComponentName componentName = new ComponentName(activityInfo.packageName, activityInfo.name);
                Drawable applicationIcon = getApplicationIcon(activityInfo.packageName);

                launcherEntries.add(new LauncherEntry(
                        applicationName,
                        applicationIcon,
                        componentName
                ));
            }
        }

        if (!launcherEntries.isEmpty()) {
            ApplicationsActivity activity = weakActivity.get();
            if (activity != null) {
                activity.runOnUiThread(() -> activity.renderApplications(launcherEntries));
            }
        }
    }

    private Drawable getApplicationIcon(String packageName) {
        try {
            return packageManager.getApplicationIcon(packageName);
        } catch (PackageManager.NameNotFoundException e) {
            return ContextCompat.getDrawable(context, android.R.drawable.sym_def_app_icon);
        }
    }
}