package com.btavares.nn4m.app.presentation.extension

import android.os.SystemClock
import android.view.View

fun View.setOnDebouncedClickListener(action: () -> Unit){
    val actionDebouncer = ActionDebouncer(action)

    setOnClickListener{
        actionDebouncer.notifyAction()
    }
}


fun Double.format(digits: Int) = "%.${digits}f".format(this)

fun View.removeOnDebouncedClickListener(){
    setOnClickListener(null)
    isClickable = false
}



private class ActionDebouncer(private val action: () -> Unit) {

    companion object{
        const val DEBOUNCE_INTERVAL_MILLISECONDS = 600L
    }

    private var lastActionTime = 0L

    fun notifyAction(){
        val now = SystemClock.elapsedRealtime()

        val millisecondsPassed = now - lastActionTime
        val actionAllowed = millisecondsPassed > DEBOUNCE_INTERVAL_MILLISECONDS
        lastActionTime = now

        if (actionAllowed){
            action.invoke()
        }
    }

}