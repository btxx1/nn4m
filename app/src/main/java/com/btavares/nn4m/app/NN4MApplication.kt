package com.btavares.nn4m.app

import android.app.Application
import android.content.Context
import com.btavares.nn4m.BuildConfig
import com.btavares.nn4m.app.kodein.FragmentArgs
import com.btavares.nn4m.appModule
import com.btavares.nn4m.main.data.dataModule
import com.btavares.nn4m.main.domain.domainModule
import com.btavares.nn4m.main.presentation.presentationModule
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import timber.log.Timber

class NN4MApplication: Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@NN4MApplication))
        import(appModule)
        import(presentationModule)
        import(domainModule)
        import(dataModule)

        externalSources.add(FragmentArgs())
    }


    private lateinit var context: Context


    override fun onCreate() {
        super.onCreate()
        context = this

        initTimber()
    }


    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}