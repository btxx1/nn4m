package com.btavares.nn4m.app.presentation

import android.os.Bundle
import androidx.navigation.fragment.findNavController
import com.btavares.nn4m.R
import com.btavares.nn4m.app.presentation.base.BaseActivity
import com.btavares.nn4m.app.presentation.navigation.NavManager
import kotlinx.android.synthetic.main.activity_main.*
import org.kodein.di.generic.instance

class MainActivity : BaseActivity(R.layout.activity_main) {

    private val navigationController get() = navigationFragment.findNavController()

    private  val navigationManager: NavManager by instance()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }


    private fun init(){
        navigationManager.setOnNavEvent {
            navigationController.navigate(it)
        }

    }
}