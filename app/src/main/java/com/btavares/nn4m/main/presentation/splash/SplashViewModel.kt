package com.btavares.nn4m.main.presentation.splash

import com.btavares.nn4m.app.presentation.navigation.NavManager
import com.btavares.nn4m.app.presentation.viewmodel.BaseAction
import com.btavares.nn4m.app.presentation.viewmodel.BaseViewModel
import com.btavares.nn4m.app.presentation.viewmodel.BaseViewState

internal class SplashViewModel (
    private val navManager: NavManager
) : BaseViewModel<SplashViewModel.ViewState, SplashViewModel.Action>(ViewState()) {


    internal data class ViewState(
        val isLoading: Boolean = true,
        val isError: Boolean = false
    ): BaseViewState


    internal sealed class Action : BaseAction {

    }

    override fun onReduceState(viewAction: Action): ViewState {
        TODO("Not yet implemented")
    }


    fun navigateToHomeFragment() {
        val navDirections = SplashFragmentDirections.actionSplashFragmentToHomeFragment()
        navManager.navigate(navDirections)
    }
}