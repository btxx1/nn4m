package com.btavares.nn4m.main.data

import com.btavares.nn4m.main.data.repository.HomeRepositoryImpl
import com.btavares.nn4m.main.data.retrofit.service.NN4MService
import com.btavares.nn4m.main.domain.repository.HomeRepository
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit

internal const val MODULE_NAME = "Data"

val dataModule = Kodein.Module("${MODULE_NAME}Module") {

    bind<HomeRepository>() with singleton { HomeRepositoryImpl(instance()) }

    bind() from singleton { instance<Retrofit>().create(NN4MService::class.java) }
}