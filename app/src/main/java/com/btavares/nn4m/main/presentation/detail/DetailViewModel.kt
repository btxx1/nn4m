package com.btavares.nn4m.main.presentation.detail

import androidx.lifecycle.viewModelScope
import com.btavares.nn4m.app.presentation.navigation.NavManager
import com.btavares.nn4m.app.presentation.viewmodel.BaseAction
import com.btavares.nn4m.app.presentation.viewmodel.BaseViewModel
import com.btavares.nn4m.app.presentation.viewmodel.BaseViewState
import com.btavares.nn4m.main.domain.model.ProductDomainModel
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.models.SlideModel
import kotlinx.coroutines.launch


internal class DetailViewModel(
    private val navManager: NavManager,
    private val args: DetailFragmentArgs
) : BaseViewModel<DetailViewModel.ViewState, DetailViewModel.Action>(ViewState()) {




    override fun onLoadData()   {
        super.onLoadData()
        init()

    }

    private fun init() = viewModelScope.launch {
        if (!args.productDomainModel.name.isNullOrEmpty()) {
            sendAction(Action.ProductLoadingSuccess(args.productDomainModel))
            val arrayList = arrayListOf<SlideModel>()
            args.productDomainModel.allImages?.forEach {
                arrayList.add(SlideModel(it, ScaleTypes.FIT))
            }
            if (!arrayList.isNullOrEmpty())
              sendAction(Action.ProductImagesLoadingSuccess(arrayList))
            else
                sendAction(Action.ImagesLoadingFailure)
        } else {
            sendAction(Action.ProductLoadingFailure)
        }

    }

    internal sealed class Action : BaseAction {
        class ProductLoadingSuccess(val product: ProductDomainModel) : Action()
        class ProductImagesLoadingSuccess(val images: List<SlideModel>) : Action()
        object ProductLoadingFailure : Action()
        object ImagesLoadingFailure : Action()

    }

    override fun onReduceState(viewAction: Action) = when (viewAction) {
        is Action.ProductLoadingSuccess -> state.copy(
            isLoading = false,
            isProductLoadingError = false,
            product = viewAction.product
        )

        is Action.ProductImagesLoadingSuccess -> state.copy(
            isLoading = false,
            isImagesLoadingError = false,
            images = viewAction.images
        )

        is Action.ProductLoadingFailure -> state.copy(
            isLoading = false,
            isProductLoadingError = true,
            product = null
        )

        is Action.ImagesLoadingFailure -> state.copy(
            isLoading = false,
            isImagesLoadingError = true,
            images = listOf()
        )
    }

    internal data class ViewState(
        val isLoading: Boolean = true,
        val isProductLoadingError: Boolean = false,
        val isImagesLoadingError: Boolean = false,
        val product: ProductDomainModel? = null,
        val images: List<SlideModel> = listOf()
    ): BaseViewState



    fun navigateBackToHomeFragment(){
        val navDirections = DetailFragmentDirections.actionDetailFragmentGoBackToHomeFragment()
        navManager.navigate(navDirections)
    }





}