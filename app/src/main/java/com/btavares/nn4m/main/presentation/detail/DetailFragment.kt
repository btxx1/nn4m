package com.btavares.nn4m.main.presentation.detail

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.btavares.nn4m.R
import com.btavares.nn4m.app.presentation.extension.format
import com.btavares.nn4m.app.presentation.extension.observe
import com.btavares.nn4m.app.presentation.extension.setOnDebouncedClickListener
import com.btavares.nn4m.app.presentation.fragment.InjectionFragment
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.fragment_home.*
import org.kodein.di.generic.instance

class DetailFragment: InjectionFragment(R.layout.fragment_detail) {


    private val viewModel: DetailViewModel by instance()


    private val stateObserver = Observer<DetailViewModel.ViewState> {
        detailProgressBar.isVisible = it.isLoading
        tvDetailErrorMessage.isVisible = it.isProductLoadingError
        tvErrorLoadingImagesMessage.isVisible = it.isImagesLoadingError
        tvProductName.text = it.product?.name
        tvProductPrice.text = it.product?.cost?.format(2)
        imageSlider.setImageList(it.images)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        detailBackArrow.setOnDebouncedClickListener {
            viewModel.navigateBackToHomeFragment()
        }

        observe(viewModel.stateLiveData, stateObserver)
        viewModel.loadData()

    }



}