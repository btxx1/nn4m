package com.btavares.nn4m.main.data.model

import com.btavares.nn4m.main.domain.model.ProductDomainModel
import com.squareup.moshi.Json


internal data class ProductDataModel(
    @field:Json(name = "name") val name : String,
    @field:Json(name = "cost") val cost : Double,
    @field:Json(name = "wascost") val wascost : String,
    @field:Json(name = "costEUR") val costEUR : Double,
    @field:Json(name = "wascostEUR") val wascostEUR : String,
    @field:Json(name = "costWER") val costWER : Double,
    @field:Json(name = "wascostWER") val wascostWER : String,
    @field:Json(name = "costUSD") val costUSD : Double,
    @field:Json(name = "wascostUSD") val wascostUSD : String,
    @field:Json(name = "costAUD") val costAUD : Double,
    @field:Json(name = "wascostAUD") val wascostAUD : String,
    @field:Json(name = "costSEK") val costSEK : Double,
    @field:Json(name = "wascostSEK") val wascostSEK : String,
    @field:Json(name = "costWEK") val costWEK : Double,
    @field:Json(name = "wascostWEK") val wascostWEK : String,
    @field:Json(name = "prodid") val prodid : Int,
    @field:Json(name = "promotionImage") val promotionImage : String,
    @field:Json(name = "mediaIcon") val mediaIcon : String,
    @field:Json(name = "colour") val colour : String,
    @field:Json(name = "sizes") val sizes : String,
    @field:Json(name = "altImage") val altImage : String,
    @field:Json(name = "dateSort") val dateSort : Int,
    @field:Json(name = "allImages") val allImages : List<String>?,
    @field:Json(name = "isNewArrival") val isNewArrival : Boolean?,
    @field:Json(name = "isTrending") val isTrending : Boolean?,
    @field:Json(name = "category") val category : String?,
    @field:Json(name = "fit") val fit : String?,
    @field:Json(name = "design") val design : String?
)


internal fun ProductDataModel.toDomainModel() : ProductDomainModel {
    val imageBaseUrl = "https://riverisland.scene7.com/is/image/RiverIsland/${this.prodid}_main"

    return ProductDomainModel(
        name = this.name,
        cost = this.cost,
        wascost = this.wascost,
        costEUR = this.costEUR,
        wascostEUR = this.wascostEUR,
        costWER = this.costWER,
        wascostWER = this.wascostWER,
        costUSD = this.costUSD,
        wascostUSD = this.wascostUSD,
        costAUD = this.costAUD,
        wascostAUD = this.wascostAUD,
        costSEK = this.costSEK,
        wascostSEK = this.wascostSEK,
        costWEK = this.costWEK,
        wascostWEK = this.wascostWEK,
        prodid = this.prodid,
        promotionImage = this.promotionImage,
        mediaIcon = this.mediaIcon,
        colour = this.colour,
        sizes = this.sizes,
        altImage = this.altImage,
        dateSort = this.dateSort,
        allImages = this.allImages,
        isNewArrival = this.isNewArrival,
        isTrending = this.isTrending,
        category = this.category,
        fit = this.fit,
        design = this.design,
        imageUrl = imageBaseUrl
    )
}