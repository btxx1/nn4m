package com.btavares.nn4m.main.domain.repository

import com.btavares.nn4m.main.domain.model.ProductDomainModel

internal interface HomeRepository {
    suspend fun getProductsAsync() : List<ProductDomainModel>?
}