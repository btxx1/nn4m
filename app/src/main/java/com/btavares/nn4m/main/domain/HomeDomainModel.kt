package com.btavares.nn4m.main.domain

import com.btavares.nn4m.main.domain.usecase.GetProductsListUseCase
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

internal const val MODULE_NAME = "Domain"

val domainModule = Kodein.Module("${MODULE_NAME}Module") {

    bind() from singleton { GetProductsListUseCase(instance()) }
}