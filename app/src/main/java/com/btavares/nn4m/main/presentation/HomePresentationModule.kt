package com.btavares.nn4m.main.presentation

import androidx.fragment.app.Fragment
import com.btavares.nn4m.app.di.KotlinViewModelProvider
import com.btavares.nn4m.main.presentation.detail.DetailViewModel
import com.btavares.nn4m.main.presentation.home.HomeViewModel
import com.btavares.nn4m.main.presentation.home.recyclerview.ProductsAdapter
import com.btavares.nn4m.main.presentation.splash.SplashViewModel
import org.kodein.di.Kodein
import org.kodein.di.android.x.AndroidLifecycleScope
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.scoped
import org.kodein.di.generic.singleton

internal const val MODULE_NAME = "Presentation"

val presentationModule = Kodein.Module("${MODULE_NAME}Module") {

    bind<SplashViewModel>() with scoped<Fragment>(AndroidLifecycleScope).singleton {
        KotlinViewModelProvider.of(context){SplashViewModel(instance())}
    }

    bind<HomeViewModel>() with scoped<Fragment>(AndroidLifecycleScope).singleton {
        KotlinViewModelProvider.of(context){HomeViewModel(instance(), instance())}
    }

    bind<DetailViewModel>() with scoped<Fragment>(AndroidLifecycleScope).singleton {
        KotlinViewModelProvider.of(context){DetailViewModel(instance(), instance())}
    }


    bind() from singleton { ProductsAdapter() }
}