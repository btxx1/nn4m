package com.btavares.nn4m.main.domain.usecase

import com.btavares.nn4m.main.domain.model.ProductDomainModel
import com.btavares.nn4m.main.domain.repository.HomeRepository
import com.btavares.nn4m.main.presentation.home.HomeFragment
import java.io.IOException
import java.lang.Exception
import java.lang.RuntimeException

internal class GetProductsListUseCase (
    private val homeRepository: HomeRepository
) {
    sealed class Result {
        data class Success(val data: List<ProductDomainModel>) : Result()
        data class Error(val e: Throwable) : Result()
    }

    suspend fun execute():  Result = try {
        homeRepository.getProductsAsync()?.let {
            Result.Success(it)
        } ?: Result.Error(RuntimeException("Error"))
    } catch (e: Exception){
        Result.Error(e)
    }


}