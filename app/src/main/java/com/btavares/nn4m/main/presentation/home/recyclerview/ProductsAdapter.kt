package com.btavares.nn4m.main.presentation.home.recyclerview


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.btavares.nn4m.R
import com.btavares.nn4m.app.delegate.observer
import com.btavares.nn4m.app.presentation.extension.format
import com.btavares.nn4m.app.presentation.extension.setOnDebouncedClickListener
import com.btavares.nn4m.main.domain.model.ProductDomainModel
import kotlinx.android.synthetic.main.product_item.view.*

internal class ProductsAdapter : RecyclerView.Adapter<ProductsAdapter.ViewHolder>() {


    var mProduts: List<ProductDomainModel> by observer(listOf()) {
        notifyDataSetChanged()
    }

    private var mOnDebouncedClickListener: ((news : ProductDomainModel) -> Unit)? = null

    internal inner class ViewHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(productDomainModel: ProductDomainModel) {
            itemView.tvProductName.text = productDomainModel.name
            itemView.ivProductImage.load(productDomainModel.imageUrl)
            val productPrice = "${itemView.context.getString(R.string.pound_symbol)}${productDomainModel.cost.format(2)}"
            itemView.tvProductPrice.text = productPrice
            itemView.setOnDebouncedClickListener { mOnDebouncedClickListener?.invoke(productDomainModel) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.product_item, parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = mProduts.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
      holder.bind(mProduts[position])
    }

    fun setOnDebouncedClickListener(listener: (products : ProductDomainModel) -> Unit) {
        this.mOnDebouncedClickListener = listener
    }

}