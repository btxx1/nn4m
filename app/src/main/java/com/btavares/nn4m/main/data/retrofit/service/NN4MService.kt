package com.btavares.nn4m.main.data.retrofit.service

import com.btavares.nn4m.main.data.retrofit.response.GetProductsResponse
import retrofit2.http.GET
import retrofit2.http.Query

internal interface NN4MService {

    @GET("plp/en_gb/2506/products.json")
    suspend fun  getProducts(
    ): GetProductsResponse?
}