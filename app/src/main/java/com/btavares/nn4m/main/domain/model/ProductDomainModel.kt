package com.btavares.nn4m.main.domain.model

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.Json
import java.util.ArrayList

internal data class ProductDomainModel(
    @field:Json(name = "name") val name: String,
    @field:Json(name = "cost") val cost: Double,
    @field:Json(name = "wascost") val wascost: String,
    @field:Json(name = "costEUR") val costEUR: Double,
    @field:Json(name = "wascostEUR") val wascostEUR: String,
    @field:Json(name = "costWER") val costWER: Double,
    @field:Json(name = "wascostWER") val wascostWER: String,
    @field:Json(name = "costUSD") val costUSD: Double,
    @field:Json(name = "wascostUSD") val wascostUSD: String,
    @field:Json(name = "costAUD") val costAUD: Double,
    @field:Json(name = "wascostAUD") val wascostAUD: String,
    @field:Json(name = "costSEK") val costSEK: Double,
    @field:Json(name = "wascostSEK") val wascostSEK: String,
    @field:Json(name = "costWEK") val costWEK: Double,
    @field:Json(name = "wascostWEK") val wascostWEK: String,
    @field:Json(name = "prodid") val prodid: Int,
    @field:Json(name = "promotionImage") val promotionImage: String,
    @field:Json(name = "mediaIcon") val mediaIcon: String,
    @field:Json(name = "colour") val colour: String,
    @field:Json(name = "sizes") val sizes: String,
    @field:Json(name = "altImage") val altImage: String,
    @field:Json(name = "dateSort") val dateSort: Int,
    @field:Json(name = "allImages") val allImages: List<String>?,
    @field:Json(name = "isNewArrival") val isNewArrival: Boolean?,
    @field:Json(name = "isTrending") val isTrending: Boolean?,
    @field:Json(name = "category") val category: String?,
    @field:Json(name = "fit") val fit: String?,
    @field:Json(name = "design") val design: String?,
    val imageUrl: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString().toString(),
        parcel.readDouble(),
        parcel.readString().toString(),
        parcel.readDouble(),
        parcel.readString().toString(),
        parcel.readDouble(),
        parcel.readString().toString(),
        parcel.readDouble(),
        parcel.readString().toString(),
        parcel.readDouble(),
        parcel.readString().toString(),
        parcel.readDouble(),
        parcel.readString().toString(),
        parcel.readDouble(),
        parcel.readString().toString(),
        parcel.readInt(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readInt(),
        parcel.createStringArrayList(),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString().toString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeDouble(cost)
        parcel.writeString(wascost)
        parcel.writeDouble(costEUR)
        parcel.writeString(wascostEUR)
        parcel.writeDouble(costWER)
        parcel.writeString(wascostWER)
        parcel.writeDouble(costUSD)
        parcel.writeString(wascostUSD)
        parcel.writeDouble(costAUD)
        parcel.writeString(wascostAUD)
        parcel.writeDouble(costSEK)
        parcel.writeString(wascostSEK)
        parcel.writeDouble(costWEK)
        parcel.writeString(wascostWEK)
        parcel.writeInt(prodid)
        parcel.writeString(promotionImage)
        parcel.writeString(mediaIcon)
        parcel.writeString(colour)
        parcel.writeString(sizes)
        parcel.writeString(altImage)
        parcel.writeInt(dateSort)
        parcel.writeStringList(allImages)
        parcel.writeValue(isNewArrival)
        parcel.writeValue(isTrending)
        parcel.writeString(category)
        parcel.writeString(fit)
        parcel.writeString(design)
        parcel.writeString(imageUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProductDomainModel> {
        override fun createFromParcel(parcel: Parcel): ProductDomainModel {
            return ProductDomainModel(parcel)
        }

        override fun newArray(size: Int): Array<ProductDomainModel?> {
            return arrayOfNulls(size)
        }
    }
}
