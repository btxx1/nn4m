package com.btavares.nn4m.main.data.retrofit.response

import com.btavares.nn4m.main.data.model.ProductDataModel
import com.squareup.moshi.Json

internal data class GetProductsResponse (
    @field:Json(name = "Products") val products : List<ProductDataModel>
    )