package com.btavares.nn4m.main.presentation.home

import androidx.lifecycle.viewModelScope
import com.btavares.nn4m.app.presentation.navigation.NavManager
import com.btavares.nn4m.app.presentation.viewmodel.BaseAction
import com.btavares.nn4m.app.presentation.viewmodel.BaseViewModel
import com.btavares.nn4m.app.presentation.viewmodel.BaseViewState
import com.btavares.nn4m.main.domain.model.ProductDomainModel
import com.btavares.nn4m.main.domain.usecase.GetProductsListUseCase
import kotlinx.coroutines.launch

internal class HomeViewModel (
    private val navManager: NavManager,
    private val getProductsListUseCase: GetProductsListUseCase
) : BaseViewModel<HomeViewModel.ViewState, HomeViewModel.Action>(ViewState()) {

    var products: MutableList<ProductDomainModel> = mutableListOf()

     fun getProductsList() = viewModelScope.launch {

         getProductsListUseCase.execute().also { result ->
             val action = when(result) {
                 is GetProductsListUseCase.Result.Success ->
                     if (result.data.isEmpty()){
                      Action.ProductsListLoadingFailure
                     } else {
                         products = result.data.toMutableList()
                        Action.ProductsListLoadingSuccess(result.data)
                     }

                 is GetProductsListUseCase.Result.Error -> Action.ProductsListLoadingFailure

             }
              sendAction(action)
         }


     }


    fun sortProductsHighToLow(){
        products.sortByDescending { it.cost }
        sendAction(Action.ProductsListLoadingSuccess(products))
    }

    fun sortProductsLowToHigh(){
        products.sortBy { it.cost }
        sendAction(Action.ProductsListLoadingSuccess(products))
    }

    fun navigateDetailFragment(domainModel: ProductDomainModel) {
        val navDirection = HomeFragmentDirections.actionHomeFragmentToDetailFragment(domainModel)
        navManager.navigate(navDirection)
    }


    internal data class ViewState(
        val isLoading: Boolean = true,
        val isError: Boolean = false,
        val products: List<ProductDomainModel> = listOf()
    ): BaseViewState


    internal sealed class Action : BaseAction {
        class ProductsListLoadingSuccess(val products: List<ProductDomainModel>) : Action()
        object ProductsListLoadingFailure : Action()

    }

    override fun onReduceState(viewAction: Action) = when (viewAction) {
        is Action.ProductsListLoadingSuccess -> state.copy(
            isLoading = false,
            isError = false,
            products = viewAction.products

        )

        is Action.ProductsListLoadingFailure -> state.copy(
            isLoading = false,
            isError = true,
            products = listOf()
        )

    }
}