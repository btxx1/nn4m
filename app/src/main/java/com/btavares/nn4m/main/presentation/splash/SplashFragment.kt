package com.btavares.nn4m.main.presentation.splash

import android.os.Bundle
import android.view.View
import com.btavares.nn4m.R
import com.btavares.nn4m.app.presentation.fragment.InjectionFragment
import org.kodein.di.generic.instance
import java.util.*
import kotlin.concurrent.timerTask

internal class SplashFragment : InjectionFragment(R.layout.fragment_splash) {

    private  val viewModel: SplashViewModel by instance()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Timer().schedule(timerTask {
                viewModel.navigateToHomeFragment()
        }, 2000)

    }

}