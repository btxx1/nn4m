package com.btavares.nn4m.main.presentation.home

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.PopupMenu
import androidx.core.view.isVisible
import com.btavares.nn4m.R
import com.btavares.nn4m.app.presentation.fragment.InjectionFragment
import com.btavares.nn4m.main.presentation.home.recyclerview.ProductsAdapter
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.btavares.nn4m.app.presentation.extension.observe
import com.btavares.nn4m.app.presentation.extension.setOnDebouncedClickListener
import kotlinx.android.synthetic.main.fragment_home.*
import org.kodein.di.generic.instance


internal class HomeFragment : InjectionFragment(R.layout.fragment_home) {


    private  val viewModel: HomeViewModel by instance()

    private val productsAdapter : ProductsAdapter by instance()

    private val stateObserver = Observer<HomeViewModel.ViewState> {
        progressBar.isVisible = it.isLoading
        tvErrorMessage.isVisible = it.isError
        productsAdapter.mProduts = it.products


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val  context = requireContext()

        productsRecyclerView.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(context, 2)
            adapter = productsAdapter
        }

        btnSearch.setOnDebouncedClickListener {
            btnSearch.isVisible = false
            progressBar.isVisible = true
            tvSort.isVisible = true
            viewModel.getProductsList()
        }

        productsAdapter.setOnDebouncedClickListener {
            viewModel.navigateDetailFragment(it)
        }

        tvSort.setOnDebouncedClickListener {
            showPopUpMenu(context)
        }



        observe(viewModel.stateLiveData, stateObserver)

    }


    private fun showPopUpMenu(context : Context){
        val popupMenu = PopupMenu(context, tvSort)
        popupMenu.inflate(R.menu.product_list_menu)

        popupMenu.setOnMenuItemClickListener {
            when(it.itemId) {
                R.id.itemLowToHigh -> {
                viewModel.sortProductsLowToHigh()
                }
                R.id.itemHighToLow -> {
                    viewModel.sortProductsHighToLow()
                }
            }

            true
        }
        popupMenu.show()

    }

    override fun onResume() {
        super.onResume()
        if(productsAdapter.mProduts.isNotEmpty()){
            btnSearch.isVisible = false
            tvSort.isVisible = true
        }


    }

}