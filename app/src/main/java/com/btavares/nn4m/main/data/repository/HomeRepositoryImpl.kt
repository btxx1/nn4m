package com.btavares.nn4m.main.data.repository

import com.btavares.nn4m.main.data.model.toDomainModel
import com.btavares.nn4m.main.data.retrofit.service.NN4MService
import com.btavares.nn4m.main.domain.repository.HomeRepository

internal class HomeRepositoryImpl(
    private val nN4MService: NN4MService
) : HomeRepository {

    override suspend fun getProductsAsync()
        = nN4MService.getProducts()?.products?.map { it.toDomainModel() }
}