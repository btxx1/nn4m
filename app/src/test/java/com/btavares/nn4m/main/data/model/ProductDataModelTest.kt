package com.btavares.nn4m.main.data.model

import com.btavares.nn4m.main.data.DataFixtures
import com.btavares.nn4m.main.domain.model.ProductDomainModel
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Assert.*
import org.junit.Test

class ProductDataModelTest {


    @Test
    fun `convert productDataModel to Product Domain Model`() {
        //given
        val dataModel = DataFixtures.getProduct()


        //when
        val domainModel = dataModel.toDomainModel()


        // then
        domainModel shouldBeEqualTo ProductDomainModel(
          dataModel.name,
          dataModel.cost,
          dataModel.wascost,
          dataModel.costEUR,
          dataModel.wascostEUR,
          dataModel.costWER,
          dataModel.wascostWER,
          dataModel.costUSD,
          dataModel.wascostUSD,
          dataModel.costAUD,
          dataModel.wascostAUD,
          dataModel.costSEK,
          dataModel.wascostSEK,
          dataModel.costWEK,
          dataModel.wascostWEK,
          dataModel.prodid,
          dataModel.promotionImage,
          dataModel.mediaIcon,
          dataModel.colour,
          dataModel.sizes,
          dataModel.altImage,
          dataModel.dateSort,
          dataModel.allImages,
          dataModel.isNewArrival,
          dataModel.isTrending,
          dataModel.category,
          dataModel.fit,
          dataModel.design,
          "https://riverisland.scene7.com/is/image/RiverIsland/${dataModel.prodid}_main"



        )



    }




}