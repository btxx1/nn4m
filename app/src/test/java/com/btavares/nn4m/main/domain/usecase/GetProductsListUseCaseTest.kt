package com.btavares.nn4m.main.domain.usecase

import com.btavares.nn4m.main.domain.DomainFixtures
import com.btavares.nn4m.main.domain.repository.HomeRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Before
import org.junit.Test
import java.net.UnknownHostException

class GetProductsListUseCaseTest {

    @MockK
    internal lateinit var homeRepository: HomeRepository


    private lateinit var getProductsListUseCase: GetProductsListUseCase


    @Before
    fun setUp(){
        MockKAnnotations.init(this)

        getProductsListUseCase = GetProductsListUseCase(homeRepository)
    }


    @Test
    fun `return list of products ` () {
        //given
        val productsList = listOf(DomainFixtures.getProduct(),DomainFixtures.getProduct())
        coEvery { homeRepository.getProductsAsync() } returns productsList


        //when
        val result = runBlocking { getProductsListUseCase.execute() }


        //then
        result shouldBeEqualTo GetProductsListUseCase.Result.Success(productsList)

    }

    @Test
    fun `return error when throws an exception`() {
        // given
        val exception = UnknownHostException()
        coEvery { homeRepository.getProductsAsync() } throws exception

        //when
        val result = runBlocking { getProductsListUseCase.execute() }

        //then
        result shouldBeEqualTo GetProductsListUseCase.Result.Error(exception)


    }





}