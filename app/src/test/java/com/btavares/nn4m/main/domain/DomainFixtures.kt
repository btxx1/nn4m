package com.btavares.nn4m.main.domain

import com.btavares.nn4m.main.data.model.ProductDataModel
import com.btavares.nn4m.main.domain.model.ProductDomainModel

object DomainFixtures {

    internal fun getProduct(
        name: String = "Pink - Light SS RI Monogram Jacquard Polo",
        cost: Double = 28.00,
        wascost: String="",
        costEUR: Double =37.0,
        wascostEUR: String="",
        costWER: Double =37.0,
        wascostWER: String="",
        costUSD: Double =56.0,
        wascostUSD: String="",
        costAUD : Double =56.0,
        wascostAUD: String="",
        costSEK: Double =399.0,
        wascostSEK: String="",
        costWEK: Double =399.0,
        wascostWEK: String="",
        prodid: Int=786003,
        promotionImage: String="",
        mediaIcon: String="",
        colour: String="Pink",
        sizes: String="6,8,10,12,14,16,18",
        altImage: String="",
        dateSort: Int=0,
        allImages : List<String> = listOf("https://images.riverisland.com/is/image/RiverIsland/pink---light-ss-ri-monogram-jacquard-polo_786003_main","https://images.riverisland.com/is/image/RiverIsland/pink---light-ss-ri-monogram-jacquard-polo_786003_rollover"),
        isNewArrival: Boolean= true,
        isTrending: Boolean = false,
        category: String="Tops",
        fit: String="Main Collection",
        design: String="Print",
        imageUrl: String = "https://riverisland.scene7.com/is/image/RiverIsland/786003_main"
    ) : ProductDomainModel = ProductDomainModel(name,cost,wascost,costEUR,wascostEUR, costWER, wascostWER, costUSD,
        wascostUSD, costAUD, wascostAUD, costSEK, wascostSEK, costWEK, wascostWEK, prodid, promotionImage, mediaIcon, colour, sizes,
        altImage, dateSort, allImages, isNewArrival, isTrending, category, fit, design, imageUrl)


}