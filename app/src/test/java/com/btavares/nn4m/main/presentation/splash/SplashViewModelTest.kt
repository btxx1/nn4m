package com.btavares.nn4m.main.presentation.splash

import com.btavares.nn4m.app.presentation.navigation.NavManager
import io.mockk.MockKAnnotations
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class SplashViewModelTest {

    @MockK(relaxed = true)
    internal lateinit var mockNavManager : NavManager

    private lateinit var  viewModel: SplashViewModel


    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        viewModel = SplashViewModel(mockNavManager)
    }



    @Test
    fun `navigate to home fragment` () {
        // given
        val navDirections = SplashFragmentDirections.actionSplashFragmentToHomeFragment()

        //when
        viewModel.navigateToHomeFragment()

        // then
        coVerify { mockNavManager.navigate(navDirections) }
    }

}