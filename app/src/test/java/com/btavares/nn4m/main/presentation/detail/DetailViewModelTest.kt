package com.btavares.nn4m.main.presentation.detail

import com.btavares.nn4m.app.presentation.navigation.NavManager
import io.mockk.MockKAnnotations
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import org.junit.Before
import org.junit.Test

class DetailViewModelTest {

    @MockK(relaxed = true)
    internal lateinit var mockNavManager : NavManager

    @MockK
    internal lateinit var  mockDetailFragmentArgs: DetailFragmentArgs

    private lateinit var  viewModel: DetailViewModel


    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        viewModel = DetailViewModel(mockNavManager, mockDetailFragmentArgs)
    }


    @Test
    fun `navigate to detail fragment` () {
        // given
        val navDirections = DetailFragmentDirections.actionDetailFragmentGoBackToHomeFragment()

        //when
        viewModel.navigateBackToHomeFragment()

        // then
        coVerify { mockNavManager.navigate(navDirections) }
    }

}