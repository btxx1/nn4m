package com.btavares.nn4m.main.data.repository
import com.btavares.nn4m.main.data.DataFixtures
import com.btavares.nn4m.main.data.model.toDomainModel
import com.btavares.nn4m.main.data.retrofit.response.GetProductsResponse
import com.btavares.nn4m.main.data.retrofit.service.NN4MService
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Before
import org.junit.Test



class HomeRepositoryImplTest {

    @MockK
    internal lateinit var mNN4MService: NN4MService

    private lateinit var mHomeRepositoryImpl: HomeRepositoryImpl


    @Before
    fun setUp(){
       MockKAnnotations.init(this)
        mHomeRepositoryImpl = HomeRepositoryImpl(mNN4MService)
    }

    @Test
    fun `get products list`() {
        // given
        coEvery {
            mNN4MService.getProducts()
        } returns GetProductsResponse(listOf(DataFixtures.getProduct(),DataFixtures.getProduct()))

        //when
        val result = runBlocking { mHomeRepositoryImpl.getProductsAsync() }


        // then
        result shouldBeEqualTo listOf(DataFixtures.getProduct().toDomainModel(), DataFixtures.getProduct().toDomainModel())

    }


   @Test
    fun `get products returns null value`() {
        // given
        coEvery {
            mNN4MService.getProducts()
        } returns null

        //when
        val result = runBlocking { mHomeRepositoryImpl.getProductsAsync() }


        // then
        result shouldBeEqualTo null
    }


    @Test
    fun `fix test pipeline`() {
        // given

        coEvery {
            mNN4MService.getProducts()
        } returns GetProductsResponse(listOf(DataFixtures.getProduct()))

        //when
        val result = runBlocking { mHomeRepositoryImpl.getProductsAsync() }


        // then
        result shouldBeEqualTo listOf(DataFixtures.getProduct().toDomainModel())
    }

}