package com.btavares.nn4m.main.presentation.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.btavares.nn4m.app.presentation.navigation.NavManager
import com.btavares.nn4m.main.CoroutineRule
import com.btavares.nn4m.main.domain.DomainFixtures
import com.btavares.nn4m.main.domain.usecase.GetProductsListUseCase
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.net.UnknownHostException

class HomeViewModelTest {

    @ExperimentalCoroutinesApi
    @get:Rule
    var coroutinesTestRule = CoroutineRule()

    @get:Rule
    var rule = InstantTaskExecutorRule()

    @MockK(relaxed = true)
    internal lateinit var mockNavManager : NavManager

    @MockK
    internal lateinit var mockGetProductsListUseCase: GetProductsListUseCase


    private lateinit var  viewModel: HomeViewModel


    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        viewModel = HomeViewModel(mockNavManager, mockGetProductsListUseCase)
    }


    @Test
    fun `navigate to detail fragment` () {
        // given
        val product = DomainFixtures.getProduct()
        val navDirections = HomeFragmentDirections.actionHomeFragmentToDetailFragment(product)

        //when
        viewModel.navigateDetailFragment(product)

        // then
        coVerify { mockNavManager.navigate(navDirections) }
    }


    @Test
    fun `verify state when GetProductsListUseCase return successfully call` () {
        // given
        val products = listOf(DomainFixtures.getProduct(), DomainFixtures.getProduct())
        coEvery { mockGetProductsListUseCase.execute() } returns GetProductsListUseCase.Result.Success(products)

        //when
        viewModel.getProductsList()


        //then
        viewModel.stateLiveData.value shouldBeEqualTo HomeViewModel.ViewState(
            isLoading = false,
            isError = false,
            products = products
        )
    }

    @Test
    fun `verify state when GetProductsListUseCase return error` () {
        // given
        val exception = UnknownHostException()
        coEvery { mockGetProductsListUseCase.execute() } returns GetProductsListUseCase.Result.Error(exception)

        //when
        viewModel.getProductsList()


        //then
        viewModel.stateLiveData.value shouldBeEqualTo HomeViewModel.ViewState(
            isLoading = false,
            isError = true,
            products = listOf()
        )
    }




}